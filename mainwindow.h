﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QFileDialog>
#include <QMessageBox>
#include <QInputDialog>
#include <QFileInfo>
#include <QAction>
#include <QSettings>

#include <Apprentissage/apprentissage.h>
#include <Fonctions/fonctionactivation.h>
#include <Chronometre/chronometre.h>

#include "Donnees/charger.h"
#include "Donnees/conversion.h"
#include "EdVision/detectionplaque.h"

#include "Dialogs/visualiserpoidsdialog.h"
#include "Dialogs/visualiserplaquesdialog.h"

#define LOG_NORMAL_LEVEL "black"    // Normal
#define LOG_WARNING_LEVEL "orange"  // Pour les warnings quelconques
#define LOG_ERROR_LEVEL "red"       // Pour les erreurs
#define LOG_USER_LEVEL "green"      // Pour les textes ajoutés par l'utilisateur

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

  void setDernierDossierVisite(QString fichier);
  void LireParametres();
  void ClearTrainData();
  void ClearTestData();

  void AppendExtension(string &s, string extension=".csv");
  void AppendExtension(QString &s, string extension=".csv");

  void NormaliserSorties(ReseauNeurone &rdn, double newMax=1, double newMin=0);
  void NormaliserSorties(vector<Neurone> &neurones, double newMax=1, double newMin=0);

  void AnalyserExemple(ReseauNeurone &rdn, vector<double> &exemple, vector<double> oneHotEncodedVector, size_t *indexePrediction, size_t *indexeReponse);
  void AnalyserExemple(vector<Neurone> &neurones, vector<double> &exemple, vector<double> oneHotEncodedVector, size_t *indexePrediction, size_t *indexeReponse);

  size_t TrouverMaximum(vector<Neurone> &neurones);
  size_t TrouverMaximum(vector<double> &vec);

private slots:
  // Dans le GUI
  void on_pushButtonEntrainer_clicked();
  void on_pushButtonChoisirFichierApprentissage_clicked();
  void on_pushButtonChoisirFichierTest_clicked();
  void on_pushButtonChoisirFichierLabel_clicked();
  void on_pushButtonTestJeuDonnees_clicked();
  void on_pushButtonTestElement_clicked();
  void on_comboBoxDescenteGradient_currentTextChanged(const QString &arg1);

  // Menu Convertir
  void on_actionFichier_IDX_UBYTE_en_CSV_triggered(bool checked=false);
  void on_actionPGMToCSV_triggered(bool checked = false);
  void on_actionRemplacerValeurs_triggered(bool checked = false);
  void on_actionCentrerReduire_triggered(bool checked = false);
  void on_actionRetiretDiviser128_triggered(bool checked = false);
  void on_actionCentrerReduireParam_triggered(bool checked = false);

  // Menu Fichier
  void on_actionEnregistrerNeurones_triggered(bool checked = false);
  void on_actionChargerNeurones_triggered(bool checked = false);
  void on_actionEnregistrerReseauNeurones_triggered(bool checked = false);
  void on_actionChargerReseauNeurones_triggered(bool checked = false);

  // Menu Paramètres
  void on_actionChangerEcartTypesNeurones_triggered(bool checked = false);
  void on_actionChangerMoyenneNeurones_triggered(bool checked = false);

  // Menu Application
  void on_actionReconnaitreCaracteresPlaques_triggered(bool checked = false);
  void on_actionExtraireCaracPlaques_triggered(bool checked = false);


  void on_pushButtonVisualiserPoids_clicked();

  void on_pushButtonViderLog_clicked();

private:
  Ui::MainWindow *ui;

  QSettings *settings=NULL;

  QString nomFichierApprentissage, nomFichierTest, nomFichierLabel, descenteGradientString, fonctionActivationString, derniereErreur,
  dernierDossierVisite;

  Chronometre chrono;

  ptrfa_t fonctionActivationPtr, fonctionActivationDeriveePtr;

  size_t nbEntrees, nbCouches, nbNeuronesParCouches, nbNeuronesSortie, nbIterationsMax, tailleBatch;
  double tauxApprentissage, seuilTolerance, zoom;
  bool utiliserSoftmax = false, utiliserQuadratique=true, utiliserMLP=true;

  vector<double> moyennesApprentissage, ecarttypesApprentissage;
  vector<vector<double>> xTrain, yTrain, yTrainTranspose, xTest, yTest;
  vector<string> labels;

  // Données dans les fichiers CSV
  int quantiteApprentissage, rowApprentissage, colApprentissage, tailleYApprentissage, tailleXApprentissage, // apprentissage
  quantiteTest, rowTest, colTest, tailleYTest, tailleXTest, // test / validation
  quantiteLabel, rowLabel, colLabel, tailleYLabel, tailleXLabel; // labels

  vector<Neurone> neurones; // Perceptrons mono-couche
  ReseauNeurone rdn; // Perceptrons multi-couche

  VisualiserPoidsDialog *poidsDialog;
  VisualiserPlaquesDialog *plaquesDialog;

  double EntrainerReseauNeurone(ReseauNeurone &rdn, vector<vector<double>> &entrees, vector<vector<double>> &sortiesDesirees);
  double EntrainerNeurone(Neurone &n, vector<vector<double>> &entrees, vector<double> &sortiesDesirees);

  void InitUI();
  void Test();

  void LoadSettings();
  void SaveSettings();

  void closeEvent(QCloseEvent *event);

  void Log(QString text, string log_level=LOG_NORMAL_LEVEL);

};

#endif // MAINWINDOW_H
