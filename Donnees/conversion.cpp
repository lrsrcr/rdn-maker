﻿#include "conversion.h"

void ConversionIDXUbyteToCSV(const char *ficherSrcImages, const char *fichierSrcLabels, const char *fichierDest)
{
  FILE *fSrcImages = fopen(ficherSrcImages, "rb"),
      *fSrcLabels = fopen(fichierSrcLabels, "rb");

  unsigned int num, row, col, tailleX, tailleY=10;

  assert(fSrcImages);
  assert(fSrcLabels);

  SkipMagicNumber(fSrcImages);
  SkipMagicNumber(fSrcLabels);

  LireImagesLignesColonnes(fSrcImages, num, row, col);

  tailleX = col*row;
  cout << "Nombre d'images: " << num << '\t'
       << "Nombre de lignes: " << row << '\t'
       << "Nombre de colonnes: " << col << endl;

  // Créer fichier destination
  ofstream streamDst(fichierDest);
  // Ecrire la taille des données, le nombre d'entrées et de sorties
  streamDst << MAGIC_NUMBER_DATA << SEP << num << SEP << row << SEP << col << SEP << tailleY << endl;

  // Lire le nombre d'éléments, on le connait déjà: 60 000 !
  fread(&num, sizeof(unsigned int), 1, fSrcLabels);
  EchangerEndian(num);

  unsigned char tableauTmp[tailleX] = {0}, labelChar;
  double tableauImage[tailleX] = {0},
      tableauLabel[tailleY] = {0}; // tableauLabel: one hot encoded vector !

  for(size_t i=0; i<num; i++)
  {
    // Lire une image
    fread(tableauTmp, sizeof(unsigned char), tailleX, fSrcImages); // Fichier encodé en bytes
    ConversionTableauCharToDouble(tableauTmp, tableauImage, tailleX);

    // Lire son label
    fread(&labelChar, sizeof(unsigned char), 1, fSrcLabels);
    // Définir la case correspondante au label à 1
    tableauLabel[(size_t) labelChar] = 1;

    //Enregistrer au format CSV
    for(size_t j=0; j<tailleX; j++)
      streamDst << tableauImage[j] << SEP;

    for(size_t j=0; j<tailleY-1; j++)
      streamDst << tableauLabel[j] << SEP;

    streamDst << tableauLabel[tailleY-1] << endl; // Ecrire le dernier label à la fin
    streamDst.flush();

    tableauLabel[(size_t) labelChar] = 0; // Ré-initialiser le tableau des labels
  }

  // Fermer les fichiers
  fclose(fSrcImages);
  fclose(fSrcLabels);

  streamDst.close();
}

/*void ConversionTableauToMat(const unsigned char *tableau, cv::Mat &matImage, size_t row, size_t col)
{
  for(size_t i=0; i<row; i++)
    for(size_t j=0; j<col; j++)
      matImage.at<uchar>(i,j) = tableau[i*row + j];

  // cv::imshow("test", matImage);
  // cv::waitKey();
}*/

void ConversionTableauCharToDouble(const unsigned char *tableauSrc, double *tableauDst, size_t taille)
{
  for(size_t i=0; i<taille; i++)
    tableauDst[i] = (double) tableauSrc[i];
}

QPixmap ConversionVectorToPixmap(const vector<double> &src, size_t row, size_t col, double zoom)
{
  QImage imageTmp((int) col, (int) row, QImage::Format_RGB32);

  for(size_t i=0; i<col; i++)
    for(size_t j=0; j<row; j++)
      imageTmp.setPixel(i,j, (uint)src[j*row+i]);

  QImage scaledImageTmp = imageTmp.scaled((int) (col*zoom), (int) (row*zoom));

  return QPixmap::fromImage(scaledImageTmp);
}

bool ConversionPGMToCSV(const string &fichierSrc, const string &fichierDst)
{
  EdIMAGE *image = NULL;
  int nlig = 0, ncol = 0;
  unsigned char prof = 0;
  FILE *fichier = NULL;

  if(!(fichier = fopen(fichierSrc.c_str(),"rb")))
    return false; //ret=false;

  if(Reading_ImageHeader(fichier, &ncol, &nlig, &prof)>0
     || crea_IMAGE(image) == NULL // creation of Image Header
     || !Creation_Image (image, nlig, ncol, prof) // Image Data
     || !Reading_ImageData(fichier,image) // Image Pixel Data
     )
  {
    Free_Image(image);
    fclose(fichier);
    return false;
  }

  // Créer fichier destination
  ofstream streamDst;
  if(FichierExiste(fichierDst))
  {    
    int quantite=0, formatX1=0, formatX2=0, formatY=0; // Charger l'en-tête
    LireEnteteCSV(fichierDst, &quantite, &formatX1, &formatX2, &formatY, NULL);
    streamDst.open(fichierDst+".tmp");

    ifstream streamSrc(fichierDst);
    // Créer un nouvel en-tête et l'écrire
    streamDst << MAGIC_NUMBER_DATA << SEP << quantite+1 << SEP << nlig << SEP << ncol << SEP << formatY << endl;

    string dummy;
    streamSrc >> dummy; // Sauter premiere ligne: l'en-tête
    while(streamSrc >> dummy) // Recopier
      streamDst << dummy << endl;

    streamDst.flush(); // Fermer
    streamDst.close();

    // Renommer le fichier: enlever le ".tmp" à la fin
    rename((fichierDst+".tmp").c_str(), fichierDst.c_str());
  }
  else
  {
    // Ecrire en-tête
    streamDst.open(fichierDst, ios_base::app);
    streamDst << MAGIC_NUMBER_DATA << SEP << "1" << SEP << nlig << SEP << ncol << SEP << "0" << endl; // Ré-écrire l'en-tête modifié
    streamDst.flush(); // Fermer
    streamDst.close();
  }

  streamDst.open(fichierDst, ios_base::app);
  if(!streamDst.is_open()) // SI LE RENAME A ECHOUE -> ON LE SAIT ICI !!
  {
    Free_Image(image);
    return false;
  }

  // Ajouter le nouvel exemple à la fin
  for(size_t i=0; i<(size_t)nlig; i++)
    for(size_t j=0; j<(size_t)ncol; j++)
      streamDst << (double) image->ptdata.poctet[(i*nlig + j)*image->prof] << SEP;

  streamDst << fichierSrc << endl; // Ajouter le nom du fichier dans les Y, pour s'y retrouver au cas ou
  streamDst.flush();
  streamDst.close();

  fclose(fichier);

  if(Free_Image(image) == FALSE)
    return false;

  return true;
}

bool ConversionEdImageToVector(EdIMAGE *img, vector<double> &vecteur)
{
  // Image supposée en niveau de gris !
  vecteur=vector<double>(img->nlig * img->ncol);

  for(size_t i=0, h=0; i<(size_t) img->nlig; i++)
  {
    for(size_t j=0; j<(size_t) img->ncol; j++, h++)
    {
      vecteur[h] = (double) img->ptdata.poctet[i*img->nlig + j];
    }
  }

  return true;
}
