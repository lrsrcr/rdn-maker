﻿#ifndef LOAD_H
#define LOAD_H

#include <iostream>
#include <cstdio>
#include <cassert>
#include <fstream>
#include <sstream>

#include <Fichier/serialisation.h>
#include <Fichier/donnees.h>

#define MAGIC_NUMBER_DATA 0

using namespace std;

// DONNEES
void ChargerMNIST(string dossier);

// UTILS
void EchangerEndian(unsigned int &valeur);

void SkipMagicNumber(FILE *fichier);

#endif // LOAD_H
