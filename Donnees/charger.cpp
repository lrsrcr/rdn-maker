﻿#include "charger.h"

//Help of: https://gist.github.com/spaghetti-source/5620288
void ChargerMNIST(string dossier)
{
  // Magic Number; Nombre d'images dans le fichier, nombres de lignes et de colonnes d'une image
  unsigned int num, row, col;

  FILE *fimages = fopen((dossier + "train-images.idx3-ubyte").c_str(), "rb"), // DEBUG
      *flabels = fopen((dossier + "train-labels.idx1-ubyte").c_str(), "rb"); // DEBUG

  assert(fimages);
  assert(flabels);

  SkipMagicNumber(fimages);
  SkipMagicNumber(flabels);

  fread(&num, 4, 1, flabels); // dust

  LireImagesLignesColonnes(fimages, num, row, col);

  cout << "Nombre d'images: " << num << '\t'
       << "Nombre de lignes: " << row << '\t'
       << "Nombre de colonnes: " << col << endl;

  fclose(fimages);
  fclose(flabels);
}

void EchangerEndian(unsigned int &valeur)
{
  valeur = (valeur>>24)|((valeur<<8)&0x00FF0000)|((valeur>>8)&0x0000FF00)|(valeur<<24);
}

void SkipMagicNumber(FILE *fichier)
{
  unsigned int magic=0;
  fread(&magic, sizeof(unsigned int), 1, fichier);
}

inline void LireImagesLignesColonnes(FILE *fichier, unsigned int &nombre, unsigned int &lignes, unsigned int &colonnes)
{
  fread(&nombre, sizeof(unsigned int), 1, fichier);
  EchangerEndian(nombre);
  fread(&lignes, sizeof(unsigned int), 1, fichier);
  EchangerEndian(lignes);
  fread(&colonnes, sizeof(unsigned int), 1, fichier);
  EchangerEndian(colonnes);
}
