﻿#ifndef TRAITEMENT_H
#define TRAITEMENT_H

#include <fstream>
#include <regex>
#include <sstream>
#include <iostream>

#include "charger.h"

using namespace std;

void SeuillerVecteur(vector<double> &src, double seuil, double min, double max);
void RemplacerValeursCSV(string fichierSrc, string fichierDest, int threshold, int under, int above);

bool CentrerReduire(string fichierSrc, string fichierDst);
bool CentrerReduire(string fichierSrc, string fichierDst, vector<double> &moyennes, vector<double> &ecarttypes);
bool CentrerReduire(vector<double> &src, vector<double> &mu, vector<double> &sigma);

void CalculerMoyennesEcartTypes(vector<vector<double>> &src, vector<double> &moyennes, vector<double> &ecartstypes);

bool RetirerDiviserElements(string fichierSrc, string fichierDst, double mu, double sigma);

void NormaliserVecteur(vector<double> &tmp, double newMax, double newMin);

vector<vector<double>> TransposeMatrice(vector<vector<double>> &src);

#endif // TRAITEMENT_H
