﻿#include "traitement.h"

void SeuillerVecteur(vector<double> &src, double seuil, double min, double max)
{
  for(size_t i=0; i<src.size(); i++)
    src[i] = (src[i]>=seuil ? max : min);
}

void RemplacerValeursCSV(string fichierSrc, string fichierDest, int seuil, int under, int above)
{
  ifstream streamSrc(fichierSrc);
  ofstream streamDst(fichierDest);

  int num, row, col, tailleY, tailleX;
  LireEnteteCSV(&streamSrc, &num, &row, &col, &tailleY, &tailleX); // Lire l'en-tête
  EcrireEnteteCSV(&streamDst, num, row, col, tailleY); // Ecrire l'en-tête

  vector<double> tmp(tailleX+tailleY);
  // Remplacer ligne par ligne
  for(size_t i=0; i<(size_t) num; i++)
  {
    LireLigneCSV(tmp, streamSrc); // Lire ligne
    SeuillerVecteur(tmp, seuil, under, above); // Seuiller le vecteur
    EcrireLigneCSV(tmp, &streamDst);
  }

  streamDst.close();
}

vector<vector<double>> TransposeMatrice(vector<vector<double> > &src)
{
  // On suppose que chaque élément i de src[i] possède un nombre constant de double
  size_t col = src.size(), row=src[0].size();
  vector<vector<double>> dst(row);
  for(size_t i=0; i<row; i++)
  {
    dst[i] = vector<double>(col);
    for(size_t j=0; j<dst[i].size(); j++)
      dst[i][j] = src[j][i];
  }

  return dst;
}

bool CentrerReduire(string fichierSrc, string fichierDst)
{
  // Lire en-tête
  int num, row, col, tailleY, tailleX;

  if(!LireEnteteCSV(fichierSrc, &num, &row, &col, &tailleY, &tailleX))
    return false;

  vector<vector<double>> exemples(num), sorties(num); // Charger le dataset
  vector<double> moyennes(tailleX), ecarttypes(tailleX);

  if(!ChargerCSV(fichierSrc, exemples, sorties, num, moyennes, ecarttypes))
    return false;

  CalculerMoyennesEcartTypes(exemples, moyennes, ecarttypes);

  for(size_t i=0; i<(size_t) num; i++)  // Centrer et réduire chaque exemple
    for(size_t j=0; j<(size_t) tailleX; j++) // Pour chaque entrées, d'un exemple
      exemples[i][j] = (exemples[i][j]-moyennes[j])/ecarttypes[j];

  // Enregistrer dans le fichier destination
  if(!EnregistrerCSV(fichierDst, num, row, col, tailleY, exemples, sorties, &moyennes, &ecarttypes))
    return false;

  return true;
}

bool CentrerReduire(string fichierSrc, string fichierDst, vector<double> &moyennes, vector<double> &ecarttypes)
{
  // Lire en-tête
  int num, row, col, tailleY, tailleX;

  if(!LireEnteteCSV(fichierSrc, &num, &row, &col, &tailleY, &tailleX))
    return false;

  vector<vector<double>> exemples(num), sorties(num); // Charger le dataset

  if(!ChargerCSV(fichierSrc, exemples, sorties, num, moyennes, ecarttypes))
    return false;

  for(size_t i=0; i<(size_t) num; i++)  // Centrer et réduire chaque exemple
    for(size_t j=0; j<(size_t) tailleX; j++) // Pour chaque entrées, d'un exemple
      exemples[i][j] = (exemples[i][j]-moyennes[j])/ecarttypes[j];

  // Enregistrer dans le fichier destination
  if(!EnregistrerCSV(fichierDst, num, row, col, tailleY, exemples, sorties, &moyennes, &ecarttypes))
    return false;

  return true;
}

void CalculerMoyennesEcartTypes(vector<vector<double>> &src, vector<double> &moyennes, vector<double> &ecartstypes)
{
  double quantite = (double) src.size();
  // Calculer Moyennes
  vector<double> acc(moyennes.size());

  for(size_t i=0; i<src.size(); i++)
    for(size_t j=0; j<src[i].size(); j++)
      acc[j] += src[i][j];

  for(size_t i=0; i<moyennes.size(); i++)
    moyennes[i] = acc[i] / quantite;

// Calculer Ecarts-Types
  acc=vector<double>(ecartstypes.size());

  for(size_t i=0; i<src.size(); i++)
    for(size_t j=0; j<src[i].size(); j++)
      acc[j] += pow(src[i][j]-moyennes[j], 2);

  for(size_t i=0; i<ecartstypes.size(); i++)
  {
    ecartstypes[i] = sqrt(acc[i]/quantite);
    if(ecartstypes[i]==0)
      ecartstypes[i]=1E-4;
  }
}

bool CentrerReduire(vector<double> &src, vector<double> &mu, vector<double> &sigma)
{
  if(src.size() != mu.size() || src.size() != sigma.size())
    return false;

  // Calculer la moyenne
  for(size_t i=0; i<src.size(); i++)
    src[i] = (src[i]-mu[i])/sigma[i];

  return true;
}

bool RetirerDiviserElements(string fichierSrc, string fichierDst, double mu, double sigma)
{
  int num, row, col, tailleY, tailleX;
  LireEnteteCSV(fichierSrc, &num, &row, &col, &tailleY, &tailleX);
  vector<vector<double>> exemples(num), sorties(num); // Charger le dataset
  vector<double> moyennes(tailleX), ecarttypes(tailleX);


  if(!ChargerCSV(fichierSrc, exemples, sorties, num, moyennes, ecarttypes))
    return false;

  for(size_t i=0; i<(size_t) exemples.size(); i++)  // Centrer et réduire chaque exemple
    for(size_t j=0; j<(size_t) tailleX; j++)
      exemples[i][j] = (exemples[i][j]-mu)/sigma; // Pour chaque entrées, d'un exemple

  if(!EnregistrerCSV(fichierDst, num, row, col, tailleY, exemples, sorties))
    return false;

  return true;
}

void NormaliserVecteur(vector<double> &tmp, double newMax, double newMin)
{
  double min = INFINITY, max = -INFINITY;

  // Récupérer le maximum et le minimum
  for(double &d : tmp)
  {
    if(d<min)
      min = d;
    if(d>max)
      max = d;;
  }

  // Normaliser
  double oldIntervalle = max-min, newIntervalle = newMax-newMin;
  for(double &d : tmp)
    d = ((d-min) * newIntervalle/oldIntervalle)+newMin;
}
