﻿#ifndef CONVERSION_H
#define CONVERSION_H

#include "charger.h"
#include "EdVision/EdStructures.h"
#include "EdVision/EdUtilities.h"

#include <stdio.h>
#include <fstream>
//#include <opencv2/highgui.hpp>
#include <QPixmap>

void ConversionIDXUbyteToCSV(const char *ficherSrcImages, const char *fichierSrcLabels, const char *fichierDest);

//void ConversionTableauToMat(const unsigned char *tableau, cv::Mat &matImage, size_t row, size_t col);

void ConversionTableauCharToDouble(const unsigned char *tableauSrc, double *tableauDst, size_t taille);

QPixmap ConversionVectorToPixmap(const vector<double> &src, size_t row, size_t col, double zoom = 1.0);

bool ConversionPGMToCSV(const string &fichierSrc, const string &fichierDst);

bool ConversionEdImageToVector(EdIMAGE *img, vector<double> &vecteur);

#endif // CONVERSION_H
