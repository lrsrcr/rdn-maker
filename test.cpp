﻿#include <iostream>
#include <fstream>
#include <sstream>      // std::ostringstream

#include <Apprentissage/apprentissage.h>

#include <Donnees/conversion.h>
#include <Donnees/traitement.h>

#include "EdVision/detectionplaque.h"

using namespace std;

#define WORKING_DIR "/home/julien/Documents/MNIST/"
#define WORKING_DIR2 "/home/julien/Documents/JeuPlaques1/"


void ExtraireCaracteresPlaques(string &fichierSrc, string &fichierDst, string &extension);
vector<cv::Point> TrouverCoins(vector<cv::Point>pts);
vector<int> CaculerDimensions(vector<cv::Point> &coins);

/* Sources:
 * http://yann.lecun.com/exdb/mnist/
 * https://mmlind.github.io/Simple_1-Layer_Neural_Network_for_MNIST_Handwriting_Recognition/
 *
*/
int main1(/*int argc, char *argv[]*/)
{
  //size_t quantiteTrain=60000, quantiteTest=1;
  // 20000 exemples
  //double tauxApprentissage = 0.0005; // Poids Gaussien moyenne = 0; EC=1: top = 380
  //double tauxApprentissage = 0.0001; // Poids Gaussien moyenne = 0; EC=0.001: TA=0.0001, top = 285 mais erreur dimininue de +- 500 à 300, converge très lentement

  // 60000 exemples
  //double tauxApprentissage = 0.00005; // Poids Gaussien moyenne = 0; EC=0.01: TA=0.0007, top = 890 (50 itérations, 25 itérations 896)
  /*vector<vector<double>> xTrain(quantiteTrain), xTest(quantiteTest),
      yTrain(quantiteTrain), yTest(quantiteTest), yTrainTranspose;*/

  // FICHIER TRAIN
  ConversionIDXUbyteToCSV(WORKING_DIR"train-images.idx3-ubyte", WORKING_DIR"train-labels.idx1-ubyte", WORKING_DIR"mnist_train.csv"); // Convertir IDX-UBYTE en CSV
  //RemplacerValeursCSV(WORKING_DIR"mnist_train.csv", WORKING_DIR"mnist_train_adaline.csv", (int)1, (int)-1, (int)1); // Seuiller les valeurs des entrées dans un fichier CSV
  //ChargerMNISTCSV(WORKING_DIR"mnist_train_01.csv", xTrain, yTrain, quantiteTrain);
  //ChargerMNISTCSV(WORKING_DIR"mnist_train_adaline.csv", xTrain, yTrain, quantiteTrain);

  // FICHIER TESTS
  //ConversionIDXUbyteToCSV(WORKING_DIR"t10k-images.idx3-ubyte", WORKING_DIR"t10k-labels.idx1-ubyte", WORKING_DIR"mnist_test.csv"); // Convertir IDX-UBYTE en CSV
  //RemplacerValeursCSV(WORKING_DIR"mnist_test.csv", WORKING_DIR"mnist_test_adaline.csv", (int)1, (int)-1, (int)1); // Seuiller les valeurs des entrées dans un fichier CSV
  //ChargerMNISTCSV(WORKING_DIR"mnist_test_01.csv", xTest, yTest, quantiteTest);
  //ChargerMNISTCSV(WORKING_DIR"mnist_test_adaline.csv", xTrain, yTrain, quantiteTrain);

  // yTrain.size() = nombre d'exemples, yTrain[0].size() = nombre de sorties possible = nombre de neurones!!
  /*vector<Neurone> neurones(yTrain[0].size());

  yTrainTranspose = TransposeMatrice(yTrain);

  //initialiser les neurone & apprendre
  for(size_t i=0; i<neurones.size(); i++)
  //for(size_t i=0; i< 5; i++) // Entrainer que le premier neurone
  {
    //neurones[i]=Neurone(xTrain[0].size(), Logistique, LogistiqueDerivee);
    neurones[i]=Neurone(xTrain[0].size(), Signe);
    size_t nbErreurs = DescenteGradientWidrowHoff(neurones[i], xTrain, yTrainTranspose[i], 10, tauxApprentissage);
    cout << "Nombre d'erreur pour le neurone: " << i << " = " << nbErreurs << endl;
  }*/

  /*cout << "Evaluation" << endl;
  double erreurMoyenne = 0.0f,
      tmp = 0.0f;
  for(size_t i=0; i<10; i++)
  //for(size_t i=0; i<xTrain.size(); i++)
  {
    vector<double> &exemple = xTrain[i];

    for(size_t j=0; j<neurones.size(); j++)
    {
      double sortie = neurones[j].Evaluer(exemple),
	  t= (sortie>0.0 ? 1.0f : -1.0f),
	  reponse = yTrainTranspose[j][i];

      tmp += pow((reponse - t),2);

      if(t!=reponse)
	cout << i << '\t' << j << " : " << "Une erreur a ete trouvee" << endl;

      cout << '[' << j << "] La classe prédite est: " << sortie
	   << "\tLa classe réelle est : " << yTrainTranspose[j][i]
	      << endl;
    }
    cout << endl;
  }
  erreurMoyenne = (double) (tmp / (double)xTrain.size());
  cout << "Erreur Moyenne = " << erreurMoyenne << endl;*/

  //ConversionPGMToCSV(WORKING_DIR2"Chiffre_4_Th.pgm", WORKING_DIR2"chiffre4.csv");

  /*vector<double> w={0.1, 0.8};
  Neurone n(w, LineaireDerivee, TangenteHyperbolique);
  n.biais=44;
  vector<Neurone> neurones(1);
  neurones[0]=n;
  SauvegarderNeurones(neurones, WORKING_DIR"neurones1.csv");

  neurones[0].poids={0,0};
  neurones[0].biais=-1000;
  neurones[0].fonctionActivation = NULL;
  neurones[0].fonctionActivationDerivee = NULL;

  ChargerNeurones(neurones, WORKING_DIR"neurones1.csv");*/

  /*ReseauNeurone rdn(2, TangenteHyperboliqueDerivee, ReLU, 2, 1, 1);
  rdn.isSoftmax=true;
  // Définir biais
  rdn.couches[0][0].biais=8.35; rdn.couches[0][1].biais=-0.52;
  rdn.couches[1][0].biais=-2.27;

  // Définir poids
  // {{-5.98, -6.12}, {-1.11, -1.14}}, {{19.35, -59.51}}
  rdn.couches[0][0].poids={-5.98, -6.12}; rdn.couches[0][1].poids={-1.11, -1.14};
  rdn.couches[1][0].poids={19.35, -59.51};
  EnregistrerReseauNeurones(rdn, WORKING_DIR"rdn.csv");

  ReseauNeurone rdn2(1, Logistique, LogistiqueDerivee);
  ChargerReseauNeurones(rdn2, WORKING_DIR"rdn.csv");*/

 /* vector<string> caracteres = {"/home/julien/Documents/PlaquesNG/Train/L_Train_N.pgm", "/home/julien/Documents/PlaquesNG/Train/J_Train_N.pgm",
                  "/home/julien/Documents/PlaquesNG/Test1/Plaque1/D_Plaque1_N.pgm", "/home/julien/Documents/PlaquesNG/Test1/Plaque1/P_Plaque1_N.pgm"};*/

  /*ofstream tmp("/home/julien/Documents/PlaquesNG/test.csv");
  tmp << ",";
  for(size_t i=0; i<7; i++)
    tmp << "hu" << i+1 << ',';

  tmp << endl;*/

    /*for(const string &caractere : caracteres)
    {
        //tmp << caractere << ',';
        double surface = 0.0f, perimetre=0.0f, MomentsHu[7];
        int nbTrous=0, Xg=0, Yg=0;
        CalculerMomentsCaractere(caractere, MomentsHu, &perimetre, &surface, &nbTrous, &Xg, &Yg);

        for(size_t i=0; i<7; i++)
            cout << "Hu" << i+1 << "  : " << MomentsHu[i] << ",";

    //        double surface = 0.0f, perimetre=0.0f;
    //        int nbTrous=0, Xg=0, Yg=0;
    //        ExtraireCaracteristiques(caracteres[2], &surface, &perimetre, &nbTrous, &Xg, &Yg);
    //        cout << "Caracteristiques autres: " << surface << '\t' << perimetre << '\t'
    //          << nbTrous << '\t' << Xg << '\t' << Yg << endl;

        //tmp << endl;
        cout << endl;
    }*/

  //ExtraireCaracteristiques("/home/julien/Documents/PlaquesNG/Train/download_modified.pgm", "/home/julien/Documents/test.pgm", 28, 28);

    /*string fichierSrc="/home/julien/Documents/PlaquesNG/Test2/PlaquesRecoupees/Plaque3.pgm",
            fichierDst="/home/julien/Documents/PlaquesNG/Test2/CaracteresExtraits/plaque3_caractere",
            extension=".pgm";*/
    string fichierSrc="/home/julien/Documents/PlaquesNG/Train/download_modified.pgm",
            fichierDst="/home/julien/Documents/PlaquesNG/Test2/CaractereParfaits/train",
            extension=".pgm";
    ExtraireCaracteresPlaques(fichierSrc, fichierDst, extension);

    return 0;
}

void ExtraireCaracteresPlaques(string &fichierSrc, string &fichierDst, string &extension)
{
    cv::Mat fichier = cv::imread(fichierSrc, cv::IMREAD_GRAYSCALE),
            fichierSeuille = cv::Mat::zeros(fichier.rows, fichier.cols, CV_8UC1),
            fichierInverse = cv::Mat::zeros(fichier.rows, fichier.cols, CV_8UC1);
    int newH=28, newW=28;

    //cv::imshow("Image test", fichier);

    cv::threshold(fichier, fichierSeuille, 0, 255, cv::THRESH_OTSU | cv::THRESH_BINARY);
    //cv::imshow("Image test seuillée", fichierSeuille);

    cv::bitwise_not(fichierSeuille, fichierInverse);

    cv::Mat element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size( 3, 3));
    cv::morphologyEx(fichierInverse, fichierInverse, cv::MORPH_CLOSE, element);
    //cv::imshow("Image test seuillée inversée", fichierInverse);

    vector<vector<cv::Point>> contours, contoursCaracteres, coinsCaracteres;
    vector<cv::Vec4i> hierarchy;

    cv::findContours(fichierInverse, contours, hierarchy, cv::RETR_CCOMP, cv::CHAIN_APPROX_SIMPLE); // Détection des contours des caractères

    cv::Mat drawing=cv::Mat::zeros(fichierInverse.rows, fichierInverse.cols, CV_8UC3);
    for(size_t i=0; i<contours.size(); i++) // Filtrer les contours
    {
        // Diviser par le périmètre pour normaliser par rapport aux dimensions
        double aire = cv::contourArea(contours[i])/(fichierInverse.rows * fichierInverse.cols),
            perimetre = cv::arcLength(contours[i], FALSE)/(fichierInverse.rows * fichierInverse.cols);
        cout << aire << " , " << perimetre << endl;

        if((aire > 0.03 || perimetre>0.0017)/* && perimetre<0.0045*/)
        {
            contoursCaracteres.push_back(contours[i]);
            cv::drawContours(drawing, contours, i, cv::Scalar(255, 0, 0), 2, 8, hierarchy, 0, cv::Point());
            //cout << aire << " , " << perimetre << endl;
        }
    }

    // Les 4 coins de chacun des contours
    for(vector<cv::Point> &contour : contoursCaracteres)
        coinsCaracteres.push_back(TrouverCoins(contour));

    // Extraire les caractères + transformatio n
    cv::Point2f orderedCorners[4], dstCorners[4];

    cv::Mat transform_matrix;
    /*cv::Scalar couleurs[4] = {cv::Scalar(255,0,0),
        cv::Scalar(0,255,0),cv::Scalar(0,0,255),
        cv::Scalar(255,255,255),
    };*/
    int i=0;
    for(vector<cv::Point> &coins : coinsCaracteres)
    {
        for(size_t j=0; j<coins.size(); j++) // conversion vector en tableau
        {
            orderedCorners[j].x = coins[j].x;
            orderedCorners[j].y = coins[j].y;
            //cv::circle(drawing, orderedCorners[j], 1, couleurs[j], 2);
        }

        vector<int> dimensions = CaculerDimensions(coins); // [0] = Hauteur, [1] = largeur
        dstCorners[0]=cv::Point2f(0.0f, 0.0f);
        dstCorners[1]=cv::Point2f(dimensions[1]-1, 0.0f);
        dstCorners[2]=cv::Point2f(dimensions[1]-1, dimensions[0]-1);
        dstCorners[3]=cv::Point2f(0.0f, dimensions[0]-1);

        cv::Mat caractereExtrait = cv::Mat(dimensions[0], dimensions[1], CV_8UC1, cv::Scalar(255));
        transform_matrix= cv::getPerspectiveTransform(orderedCorners, dstCorners);
        cv::warpPerspective(fichier, caractereExtrait, transform_matrix, caractereExtrait.size());

        cv::Mat caractereRetaille=cv::Mat(newH, newW, CV_8UC1, cv::Scalar(255));;
        cv::resize(caractereExtrait, caractereRetaille, caractereRetaille.size());
        //cv::imshow("Lettre "+i, caractereExtrait); cv::waitKey();

        std::ostringstream stringStream;
        stringStream << fichierDst << "_" << i << extension;
        cv::imwrite(stringStream.str(), caractereRetaille);
        i++;
    }

    cv::imshow("contours", drawing); cv::waitKey();
}

vector<int> CaculerDimensions(vector<cv::Point> &coins)
{
    vector<int> res(2); // Hauteur et Largeur
    int largeurH, largeurB, hauteurH, hauteurB;

    largeurH = coins[1].x - coins[0].x, // largeur en haut
    largeurB = coins[3].x - coins[2].x;// largeur en bas
    res[1] = max(largeurH, largeurB);

    hauteurH = coins[3].y - coins[0].y, // hauteur à gauche
    hauteurB = coins[2].y - coins[1].y;// hauteur à droite
    res[0] = max(hauteurH, hauteurB);

    return res;
}

vector<cv::Point> TrouverCoins(vector<cv::Point>pts)
{
    vector<cv::Point> res(4);

    int xmax=pts[0].x, xmin=pts[0].x,
        ymax=pts[0].y, ymin=pts[0].y;
    res[0].x = res[3].x = xmin; res[1].x = res[2].x = xmax;
    res[0].y = res[1].y = ymin; res[2].y = res[3].y = ymax;

    for(size_t i=1; i<pts.size(); i++)
    {
        xmin = min(xmin, pts[i].x);
        xmax = max(xmax, pts[i].x);
        ymin = min(ymin, pts[i].y);
        ymax = max(ymax, pts[i].y);
    }

    res[0].x = xmin; res[0].y = ymin;
    res[1].x = xmax; res[1].y = ymin;
    res[2].x = xmax; res[2].y = ymax;
    res[3].x = xmin; res[3].y = ymax;
    return res;
}
