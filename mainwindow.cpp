﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    InitUI();
    LoadSettings();

    // init variables membres
    chrono = Chronometre();

    //Test();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setDernierDossierVisite(QString fichier)
{
    if(fichier.length()==0)
        return;

    QFileInfo info(fichier);
    dernierDossierVisite = info.absoluteDir().absolutePath();
}

void MainWindow::LireParametres()
{
    nomFichierApprentissage = ui->lineEditFichierApprentissage->text();
    nomFichierTest = ui->lineEditFichierTest->text();

    nbEntrees = (size_t) ui->spinBoxNbEntrees->value();
    nbCouches = (size_t) ui->spinBoxNbCouches->value();
    nbNeuronesParCouches = (size_t) ui->spinBoxNbNeuroneCache->value();
    nbNeuronesSortie = (size_t) ui->spinBoxNbNeuronesSortie->value();
    nbIterationsMax = (size_t) ui->spinBoxNbIterations->value();

    tauxApprentissage = ui->doubleSpinBoxTauxApprentissage->value();
    seuilTolerance = ui->doubleSpinBoxSeuilTolerance->value();
    tailleBatch = ui->spinBoxTailleBatch->value();

    if(seuilTolerance<0) // Si l'utilisateur choisit un seuil < 0 -> signifie qu'il faut le négliger
        seuilTolerance = INFINITY;

    zoom = ui->doubleSpinBoxZoom->value();

    descenteGradientString = ui->comboBoxDescenteGradient->currentText().toUpper();
    fonctionActivationString = ui->comboBoxFonctionActivation->currentText().toUpper();

    utiliserSoftmax = ui->checkBoxSoftmax->isChecked();
    utiliserQuadratique = ui->checkBoxQuadratique->isChecked();

    fonctionActivationPtr = NULL;

    if(fonctionActivationString == "TANGENTE HYPERBOLIQUE")
    {
        fonctionActivationPtr = TangenteHyperbolique;
        fonctionActivationDeriveePtr = TangenteHyperboliqueDerivee;
        utiliserMLP=true;
    }
    else if (fonctionActivationString == "LOGISTIQUE")
    {
        fonctionActivationPtr = Logistique;
        fonctionActivationDeriveePtr = LogistiqueDerivee;
        utiliserMLP=true;
    }
    else if (fonctionActivationString == "HEAVISIDE")
    {
        fonctionActivationPtr = Heaviside;
        utiliserMLP=false;
    }
    else if (fonctionActivationString == "SIGNE")
    {
        fonctionActivationPtr = Signe;
        utiliserMLP=false;
    }
    else if(fonctionActivationString == "LINEAIRE")
    {
        fonctionActivationPtr = Lineaire;
        utiliserMLP=false;
    }
    else
        QMessageBox::information(this, this->windowTitle(), tr("Fonction d'activation inconnue ?") );

    setDernierDossierVisite(nomFichierTest);
}

void MainWindow::ClearTrainData()
{
    moyennesApprentissage.clear();
    ecarttypesApprentissage.clear();
    xTrain.clear();
    yTrain.clear();
    yTrainTranspose.clear();
}

void MainWindow::ClearTestData()
{
    xTest.clear();
    yTest.clear();
}

void MainWindow::AppendExtension(string &s, string extension)
{
    // si s ne se termine pas par l'extension, l'ajouter
    if(s.compare(s.length()-extension.length(), extension.length(), extension)!=0)
        s+=extension;
}

void MainWindow::AppendExtension(QString &s, string extension)
{
    if(!s.endsWith(QString::fromStdString(extension)))
        s += QString::fromStdString(extension);
}

void MainWindow::NormaliserSorties(ReseauNeurone &rdn, double newMax, double newMin)
{
    double min = INFINITY, max = -INFINITY;

    // Récupérer le maximum et le minimum
    for(Neurone &n : rdn.getCoucheSortie())
    {
        if(n.y<min)
            min = n.y;
        if(n.y>max)
            max = n.y;

        // Affecter au potentiel la sortie initialement obtenue
        n.potentiel = n.y;
    }

    // Normaliser
    double intervalle = max-min, newIntervalle = newMax-newMin;
    for(Neurone &n : rdn.getCoucheSortie())
        n.y = ((n.y-min) * (newIntervalle)/intervalle)+newMin;
}

void MainWindow::NormaliserSorties(vector<Neurone> &neurones, double newMax, double newMin)
{
    double min = INFINITY, max = -INFINITY;

    // Récupérer le maximum et le minimum
    for(Neurone &n : neurones)
    {
        if(n.y<min)
            min = n.y;
        if(n.y>max)
            max = n.y;

        // Affecter au potentiel la sortie initialement obtenue
        n.potentiel = n.y;
    }

    // Normaliser
    double intervalle = max-min, newIntervalle = newMax-newMin;
    for(Neurone &n : neurones)
        n.y = ((n.y-min) * (newIntervalle)/intervalle)+newMin;
}

void MainWindow::AnalyserExemple(ReseauNeurone &rdn, vector<double> &exemple, vector<double> oneHotEncodedVector, size_t *indexePrediction, size_t *indexeReponse)
{
    *indexeReponse=-1, *indexePrediction=-1;

    // Analyser l'exemple & retrouver le bon label
    rdn.Evaluer(exemple);

    // Normaliser Sorties
    if(!utiliserSoftmax)
        NormaliserSorties(rdn);

    // Affecter le label correspondant à la sortie prédite
    *indexePrediction = TrouverMaximum(rdn.getCoucheSortie());
    *indexeReponse = TrouverMaximum(oneHotEncodedVector);
}

void MainWindow::AnalyserExemple(vector<Neurone> &neurones, vector<double> &exemple, vector<double> oneHotEncodedVector, size_t *indexePrediction, size_t *indexeReponse)
{
    *indexeReponse=-1, *indexePrediction=-1;

    // Normaliser Sorties
    NormaliserSorties(neurones);

    // Affecter le label correspondant à la sortie prédite
    *indexePrediction = TrouverMaximum(neurones);
    *indexeReponse = TrouverMaximum(oneHotEncodedVector);
}

size_t MainWindow::TrouverMaximum(vector<Neurone> &neurones)
{
    double max = -INFINITY;
    size_t indexe = 0;

    for(size_t j=0; j<neurones.size(); j++)
    {
        if(neurones[j].y>max)
        {
            max=neurones[j].y;
            indexe= j;
        }
    }

    return indexe;
}

size_t MainWindow::TrouverMaximum(vector<double> &vec)
{
    double max = -INFINITY;
    size_t indexe = 0;

    for(size_t j=0; j<vec.size(); j++)
    {
        if(vec[j]>max)
        {
            max=vec[j];
            indexe= j;
        }
    }

    return indexe;
}

double MainWindow::EntrainerReseauNeurone(ReseauNeurone &rdn, vector<vector<double>> &entrees, vector<vector<double>> &sortiesDesirees)
{
    double erreurMoyenne = 0.0;

    if(descenteGradientString.toUpper() == "FULL BATCH")
        erreurMoyenne = RetropropagationGradient(rdn, entrees, sortiesDesirees, nbIterationsMax, tauxApprentissage, seuilTolerance, utiliserQuadratique);
    else if(descenteGradientString.toUpper() == "STOCHASTIQUE")
        erreurMoyenne = DescenteGradientStochastique(rdn, entrees, sortiesDesirees, nbIterationsMax, tauxApprentissage, seuilTolerance, utiliserQuadratique);
    else if(descenteGradientString.toUpper() == "MINI-BATCH")
        erreurMoyenne = DescenteGradientStochastiqueMB(rdn, entrees, sortiesDesirees, nbIterationsMax, tailleBatch, tauxApprentissage, seuilTolerance, utiliserQuadratique);
    else
    {
        derniereErreur = QString("(MLP) Algorithme d'apprentissage inconnu: %1\n").arg(descenteGradientString) ;
        erreurMoyenne = -1; // -1 indique une erreur !
    }

    if(erreurMoyenne==INFINITY)
    {
        derniereErreur = QString("(MLP) Algorithme d'apprentissage inconnu: %1\n").arg(descenteGradientString) ;
        erreurMoyenne = -1; // -1 indique une erreur !
    }

    return erreurMoyenne;
}

double MainWindow::EntrainerNeurone(Neurone &n, vector<vector<double>> &entrees, vector<double> &sortiesDesirees)
{
    double nbErreurs=0;

    if(descenteGradientString == "DESCENTE DE GRADIENT (PERCEPTRON)")
    {
        nbErreurs = (double) DescenteGradient(n, entrees, sortiesDesirees, nbIterationsMax, tauxApprentissage);
    }
    else if(descenteGradientString == "ADALINE (PERCEPTRON)")
    {
        nbErreurs = DescenteGradientWidrowHoff(n, entrees, sortiesDesirees, nbIterationsMax, tauxApprentissage, seuilTolerance);
    }
    else
    {
        derniereErreur = QString("(Perceptron Mono-Couche) Algorithme d'apprentissage inconnu: %1\n").arg(descenteGradientString) ;
        nbErreurs = -1; // -1 indique une erreur !
    }

    return nbErreurs;
}

void MainWindow::InitUI()
{
    ui->spinBoxNbEntrees->setRange(0, 1000000);
    ui->spinBoxIndexeAfficherDonnee->setRange(0, 0);

    setWindowTitle("RDN Maker");
    QCoreApplication::setOrganizationName("Lrsrcr");
    QCoreApplication::setOrganizationDomain("https://gitlab.com/lrsrcr");
    QCoreApplication::setApplicationName("RDN Maker");

    poidsDialog=new VisualiserPoidsDialog(this);
    plaquesDialog=new VisualiserPlaquesDialog(this);
}

void MainWindow::Test()
{
    ui->comboBoxDescenteGradient->setCurrentIndex(4);
    ui->comboBoxFonctionActivation->setCurrentIndex(3);
    ui->spinBoxNbNeuronesSortie->setValue(10);
    ui->lineEditFichierApprentissage->setText("/home/julien/Documents/MNIST/mnist_train_adaline.csv");
    ui->lineEditFichierTest->setText("/home/julien/Documents/MNIST/mnist_test_adaline.csv");
    ui->spinBoxNbEntrees->setValue(784);
    ui->lineEditQuantiteTests->setText(QString("%1").arg(10000));
}

void MainWindow::LoadSettings()
{
    if(settings==NULL)
        settings = new QSettings(QCoreApplication::organizationName(), QCoreApplication::applicationName());

    // Charger les paramètres
    nomFichierApprentissage = settings->value("nomFichierApprentissage", "").value<QString>();
    nomFichierTest = settings->value("nomFichierTest", "").value<QString>();
    ui->comboBoxDescenteGradient->setCurrentIndex(settings->value("indexDescenteDeGradient", 0).value<int>());
    ui->comboBoxFonctionActivation->setCurrentIndex(settings->value("fonctionActivationIndex", 0).value<int>());
    nbCouches = settings->value("nbCouches", 0).value<int>();
    nbNeuronesParCouches = settings->value("nbNeuronesParCouches", 0).value<int>();
    nbNeuronesSortie = settings->value("nbNeuronesSortie", 0).value<int>();
    nbIterationsMax = settings->value("nbIterationsMax", 0).value<int>();
    tailleBatch = settings->value("tailleBatch", 0).value<int>();
    tauxApprentissage = settings->value("tauxApprentissage", 0.001).value<double>();
    seuilTolerance = settings->value("seuilTolerance", 0.001).value<double>();
    zoom = settings->value("zoom", 1.0).value<double>();
    utiliserSoftmax = settings->value("utiliserSoftmax", false).value<bool>();
    utiliserQuadratique = settings->value("utiliserQuadratique", true).value<bool>();
    dernierDossierVisite = settings->value("dernierDossierVisite", QDir::homePath()).value<QString>();
    setDernierDossierVisite(dernierDossierVisite);

    // Définir dans le GUI
    ui->lineEditFichierApprentissage->setText(nomFichierApprentissage);
    ui->lineEditFichierTest->setText(nomFichierTest);
    ui->spinBoxNbCouches->setValue(nbCouches);
    ui->spinBoxNbNeuroneCache->setValue(nbNeuronesParCouches);
    ui->spinBoxNbNeuronesSortie->setValue(nbNeuronesSortie);
    ui->spinBoxNbIterations->setValue(nbIterationsMax);
    ui->spinBoxTailleBatch->setValue(tailleBatch);
    ui->doubleSpinBoxTauxApprentissage->setValue(tauxApprentissage);
    ui->doubleSpinBoxSeuilTolerance->setValue(seuilTolerance);
    ui->doubleSpinBoxZoom->setValue(zoom);
    ui->checkBoxSoftmax->setChecked(utiliserSoftmax);
    ui->checkBoxQuadratique->setChecked(utiliserQuadratique);
}

void MainWindow::SaveSettings()
{
    LireParametres();

    if(settings==NULL)
        settings = new QSettings(QCoreApplication::organizationName(), QCoreApplication::applicationName());

    settings->setValue("nomFichierApprentissage", nomFichierApprentissage);
    settings->setValue("nomFichierTest", nomFichierTest);
    settings->setValue("indexDescenteDeGradient", ui->comboBoxDescenteGradient->currentIndex());
    settings->setValue("fonctionActivationIndex", ui->comboBoxFonctionActivation->currentIndex());
    settings->setValue("nbCouches", (int) nbCouches);
    settings->setValue("nbNeuronesParCouches", (int) nbNeuronesParCouches);
    settings->setValue("nbNeuronesSortie", (int) nbNeuronesSortie);
    settings->setValue("nbIterationsMax", (int) nbIterationsMax);
    settings->setValue("tailleBatch", (int) tailleBatch);
    settings->setValue("tauxApprentissage", tauxApprentissage);
    settings->setValue("seuilTolerance", seuilTolerance);
    settings->setValue("zoom", zoom);
    settings->setValue("utiliserSoftmax", utiliserSoftmax);
    settings->setValue("utiliserQuadratique", utiliserQuadratique);
    settings->setValue("dernierDossierVisite", dernierDossierVisite);

    settings->sync();
}

void MainWindow::closeEvent(QCloseEvent *)
{
    SaveSettings();
}

void MainWindow::Log(QString text, string log_level)
{
    QString formattedText = QString("<span  style='color: %2' >%1</span>").arg(text)
            .arg(QString::fromStdString(log_level));
    ui->textBrowserLog->append(formattedText);
    ui->textBrowserLog->update();
}

// Dans le GUI
void MainWindow::on_pushButtonEntrainer_clicked()
{
    LireParametres();

    if(nbEntrees==0 || xTrain.size()==0 || yTrain.size()==0)
    {
        QMessageBox::information(this, tr("Erreur Entrainer"), tr("Aucune entrée dans le réseau..."));
        return;
    }

    // Vérifier si le fichier choisis existe
    QFileInfo fichierSelectionne(nomFichierApprentissage);
    if (!fichierSelectionne.exists() || !fichierSelectionne.isFile())
    {
        QMessageBox::information(this, this->windowTitle(), tr("Le fichier n'existe pas ou n'est pas un dossier!"));
        return;
    }

    // Vérifier le ptr de la fonction d'activation
    if(fonctionActivationPtr==NULL)
    {
        QMessageBox::information(this, this->windowTitle(), tr("La fonction d'activation choisie est inconnue!"));
        return;
    }

    double acc = 0.0f, erreurPourcent=0.0f;

    if(utiliserMLP) // Utiliser RDN
    {
        rdn=ReseauNeurone(nbEntrees, fonctionActivationPtr, fonctionActivationDeriveePtr, nbNeuronesParCouches, nbCouches, nbNeuronesSortie);
        rdn.isSoftmax=utiliserSoftmax;

        chrono.Start();
        erreurPourcent = EntrainerReseauNeurone(rdn, xTrain, yTrain);
        chrono.Stop();

        if(erreurPourcent==-1)
        {
            QMessageBox::information(this, this->windowTitle(), tr(qPrintable(derniereErreur)));
            return;
        }
        Log(QString("Temps mis pour entrainer: %1\nErreur Moyenne : %2")
            .arg(QString::fromStdString(chrono.Formatter())).arg(erreurPourcent));
    }
    else // Utiliser Neurone (perceptron mono-couche)
    {
        neurones=vector<Neurone>(nbNeuronesSortie);
        for(size_t i=0; i<neurones.size(); i++)
        {
            neurones[i]=Neurone(nbEntrees, fonctionActivationPtr);

            chrono.Start();
            double nbErreurs = EntrainerNeurone(neurones[i], xTrain, yTrainTranspose[i]);
            chrono.Stop();

            if(nbErreurs==-1)
            {
                QMessageBox::information(this, this->windowTitle(), tr(qPrintable(derniereErreur)));
                return;
            }

            acc += nbErreurs;
            Log(QString("Temps mis pour entrainer: %1\nNombre d'erreur neurone: %2 = %3\n")
                .arg(QString::fromStdString(chrono.Formatter())).arg(i).arg(nbErreurs));
        }

        erreurPourcent= 100 * (double) (acc/(double)(xTrain.size())); // erreur moyenne en % sur la prédiction
    }

    ui->lineEditNbErreursRestantes->setText(QString("%1").arg(acc));
    ui->lineEditErreuMoyenne->setText(QString("%1").arg(erreurPourcent));
}

void MainWindow::on_pushButtonChoisirFichierApprentissage_clicked()
{
    // Vider les jeux de données


    nomFichierApprentissage = QFileDialog::getOpenFileName(this, "Choisir un fichier de données pour l'apprentissage", dernierDossierVisite, "CSV (*.csv)");
    if(nomFichierApprentissage.length()>0)
    {
        this->ui->lineEditFichierApprentissage->setText(nomFichierApprentissage);
        LireEnteteCSV(nomFichierApprentissage.toStdString(), &quantiteApprentissage, &rowApprentissage, &colApprentissage,
                      &tailleYApprentissage, &tailleXApprentissage);

        LireMoyennesEcarttypes(nomFichierApprentissage.toStdString(), moyennesApprentissage, ecarttypesApprentissage);
        if(moyennesApprentissage.size()==0)
            Log(QString("Le fichier %1 ne contient pas de ligne avec les moyennes").arg(nomFichierApprentissage), LOG_WARNING_LEVEL);

        if(ecarttypesApprentissage.size()==0)
            Log(QString("Le fichier %1 ne contient pas de ligne avec les écarts-types").arg(nomFichierApprentissage), LOG_WARNING_LEVEL);

        ui->spinBoxNbEntrees->setValue(tailleXApprentissage);
        ui->spinBoxNbNeuronesSortie->setValue(tailleYApprentissage);
        ui->spinBoxTailleBatch->setMaximum(quantiteApprentissage);

        setDernierDossierVisite(nomFichierApprentissage);

        // Charger les données
        chrono.Start();
        xTrain=vector<vector<double>>(quantiteApprentissage);
        yTrain=vector<vector<double>>(quantiteApprentissage);

        if(!ChargerCSV(nomFichierApprentissage.toStdString(), xTrain, yTrain, quantiteApprentissage, moyennesApprentissage, ecarttypesApprentissage))
        {
            QMessageBox::information(this, tr("Erreur lors du chargement des données dans %1").arg(nomFichierApprentissage),
                                     tr("Le fichier %1 n'a pas pu être chargé correctement !").arg(nomFichierApprentissage));
            Log(QString("Erreur lors du chargement de fichier %1").arg(nomFichierApprentissage), LOG_ERROR_LEVEL);
            return;
        }
        chrono.Stop();

        Log(QString("Chargement du fichier %1 terminé en %2")
            .arg(nomFichierApprentissage).arg(QString::fromStdString(chrono.Formatter())));

        yTrainTranspose = TransposeMatrice(yTrain);
    }
}

void MainWindow::on_pushButtonChoisirFichierTest_clicked()
{
    // Vider les données
    ClearTestData();

    nomFichierTest = QFileDialog::getOpenFileName(this, "Choisir un fichier de données pour tester", dernierDossierVisite, "CSV (*.csv)");
    if(nomFichierTest.length()>0)
    {
        this->ui->lineEditFichierTest->setText(nomFichierTest);
        LireEnteteCSV(nomFichierTest.toStdString(), &quantiteTest, &rowTest, &colTest, &tailleYTest, &tailleXTest);
        ui->lineEditQuantiteTests->setText(QString("%1").arg(quantiteTest));
        ui->spinBoxIndexeAfficherDonnee->setRange(0, quantiteTest-1);

        ui->spinBoxNbEntrees->setValue(tailleXTest);
        ui->spinBoxNbNeuronesSortie->setValue(tailleYTest);

        setDernierDossierVisite(nomFichierTest);

        // Charger les données
        chrono.Start();
        xTest=vector<vector<double>>(quantiteTest);
        yTest=vector<vector<double>>(quantiteTest);

        if(!ChargerCSV(nomFichierTest.toStdString(), xTest, yTest, quantiteTest, moyennesApprentissage, ecarttypesApprentissage))
        {
            QMessageBox::information(this, tr("Erreur lors du chargement des données dans %1")
                                     .arg(nomFichierTest), tr("Le fichier %1 n'a pas pu être chargé correctement !"));
            Log(QString("Erreur lors du chargement de fichier %1").arg(nomFichierTest), LOG_ERROR_LEVEL);
            return;
        }
        chrono.Stop();

        Log(QString("Chargement du fichier %1 terminé en %2")
            .arg(nomFichierTest).arg(QString::fromStdString(chrono.Formatter())));
    }
}

void MainWindow::on_pushButtonChoisirFichierLabel_clicked()
{
    nomFichierLabel = QFileDialog::getOpenFileName(this, "Choisir un fichier contenant les labels des données", dernierDossierVisite, "CSV (*.csv)");
    if(nomFichierLabel.length()>0)
    {
        this->ui->lineEditFichierLabel->setText(nomFichierLabel);
        LireEnteteCSV(nomFichierLabel.toStdString(), &quantiteLabel, &rowLabel, &colLabel, &tailleYLabel, &tailleXLabel);

        setDernierDossierVisite(nomFichierLabel);

        // Charger les données
        chrono.Start();
        labels=vector<string>(quantiteLabel);

        if(!ChargerCSV(nomFichierLabel.toStdString(), quantiteLabel, labels))
        {
            QMessageBox::information(this, tr("Erreur lors du chargement des labels dans %1")
                                     .arg(nomFichierLabel), tr("Le fichier %1 n'a pas pu être chargé correctement !"));
            Log(QString("Erreur lors du chargement de fichier %1").arg(nomFichierTest), LOG_ERROR_LEVEL);
            return;
        }
        chrono.Stop();

        Log(QString("Chargement du fichier %1 terminé en %2")
            .arg(nomFichierLabel).arg(QString::fromStdString(chrono.Formatter())));
    }
}

void MainWindow::on_pushButtonTestJeuDonnees_clicked()
{
    if(neurones.size()== 0 && rdn.couches.size()==0)
    {
        derniereErreur = "Veuillez d'abord entraîner un perceptron ou un réseau de neurones";
        QMessageBox::information(this, this->windowTitle(), tr(qPrintable(derniereErreur)));
        return;
    }

    if(xTest.size()==0 || yTest.size()==0)
    {
        derniereErreur = "Veuillez d'abord sélectionner des données!";
        QMessageBox::information(this, this->windowTitle(), tr(qPrintable(derniereErreur)));
        return;
    }

    int nbErreurs=0;
    double erreurMoyenne=0.0f;

    chrono.Start();
    if(utiliserMLP) // Utiliser RDN
    {
        for(size_t i=0; i<xTest.size(); i++)
        {
            size_t indexeReponse=-1, indexePrediction=-1;
            AnalyserExemple(rdn, xTest[i], yTest[i], &indexePrediction, &indexeReponse);

            if(indexePrediction!=indexeReponse)
                nbErreurs++;
        }
    }
    else // Utiliser Neurone (perceptron)
    {
        for(size_t i=0; i<xTest.size(); i++)
        {
            size_t indexeReponse=-1, indexePrediction=-1;
            AnalyserExemple(neurones, xTest[i], yTest[i], &indexePrediction, &indexeReponse);

            if(indexePrediction!=indexeReponse)
                nbErreurs++;
        }
    }
    chrono.Stop();

    erreurMoyenne = 100 * (double) ((double)nbErreurs/(double)xTest.size()); // erreur moyenne en % sur la prédiction
    Log(QString("Test du jeu exécuté en %3\nErreur : %1 (%2 %)\n")
        .arg(nbErreurs).arg(erreurMoyenne).arg(QString::fromStdString(chrono.Formatter())));

    ui->lineEditNbErreursRestantes->setText(QString("%1").arg(nbErreurs));
    ui->lineEditErreuMoyenne->setText(QString("%1 %").arg(erreurMoyenne));
}

void MainWindow::on_pushButtonTestElement_clicked()
{
    LireParametres();

    if(neurones.size()== 0 && rdn.couches.size()==0)
    {
        derniereErreur = "Veuillez d'abord entraîner un perceptron ou un réseau de neurones";
        QMessageBox::information(this, this->windowTitle(), tr(qPrintable(derniereErreur)));
        return;
    }

    int indexe = ui->spinBoxIndexeAfficherDonnee->value();

    // Charger les données
    LireEnteteCSV(nomFichierTest.toStdString(), &quantiteTest, &rowTest, &colTest, &tailleYTest, &tailleXTest);

    vector<double> exemple(tailleXTest), oneHotEncodedVector(tailleYTest);
    LireEntreeCSV(nomFichierTest.toStdString(), indexe, exemple, oneHotEncodedVector);

    // Afficher l'image
    vector<double> exempleSeuille = exemple;
    NormaliserVecteur(exempleSeuille, 0, 255);
    QPixmap tmp=ConversionVectorToPixmap(exempleSeuille, rowTest, colTest, zoom);

    ui->labelImageTest = new QLabel();
    ui->labelImageTest->setPixmap(tmp);
    ui->scrollAreaImageTest->setWidget(ui->labelImageTest);

    size_t indexeReponse=-1, indexePrediction=-1;

    chrono.Start();
    if(utiliserMLP) // Utiliser RDN
        AnalyserExemple(rdn, exemple, oneHotEncodedVector, &indexePrediction, &indexeReponse);
    else // Utiliser Neurone (perceptron)
        AnalyserExemple(neurones, exemple, oneHotEncodedVector, &indexePrediction, &indexeReponse);

    chrono.Stop();

    if(labels.size()==0) // Si pas de labels présents
    {
        ui ->lineEditPredictionDonneeTest->setText(QString("%1").arg(indexePrediction));
        ui->lineEditEtiquetteDonneeTest->setText(QString("%1").arg(indexeReponse));
    }
    else // Si labels présents
    {
        ui ->lineEditPredictionDonneeTest->setText(QString("%1 ( %2 )")
                                                   .arg(indexePrediction)
                                                   .arg(QString::fromStdString(labels[indexePrediction])));
        ui->lineEditEtiquetteDonneeTest->setText(QString("%1 ( %2 )")
                                                 .arg(indexeReponse)
                                                 .arg(QString::fromStdString(labels[indexeReponse])));
    }

    if(indexe==quantiteTest-1) // Si Indexe atteint dernier élément -> remettre à 0
        ui->spinBoxIndexeAfficherDonnee->setValue(0);
    else
        ui->spinBoxIndexeAfficherDonnee->setValue(indexe+1);

    this->update();
}

void MainWindow::on_comboBoxDescenteGradient_currentTextChanged(const QString &arg1)
{
    if(arg1.toUpper()==QString("MINI-BATCH"))
    {
        ui->spinBoxTailleBatch->setEnabled(true);
        ui->spinBoxTailleBatch->setMaximum(quantiteApprentissage);
    }
    else
        ui->spinBoxTailleBatch->setEnabled(false);
}

void MainWindow::on_actionFichier_IDX_UBYTE_en_CSV_triggered(bool )
{
    QString fichierSrcImage = QFileDialog::getOpenFileName(this, tr("Choisir un fichier d'images à manipuler"),
                                                           dernierDossierVisite, "Fichier IDX-UBYTE(*.idx3-ubyte)");
    if(fichierSrcImage.length()==0) // si aucun fichier sélectionné, arreter là
        return;

    QString fichierSrcLabel = QFileDialog::getOpenFileName(this, tr("Choisir le fichier de labels correspondant"),
                                                           dernierDossierVisite, "Fichier IDX-UBYTE(*.idx1-ubyte)");
    if(fichierSrcLabel.length()==0) // si aucun fichier sélectionné, arreter là
        return;

    bool ok=true;
    QString fichierDst = QFileDialog::getSaveFileName(this,  tr("Fichier destination"), tr("Dans quel fichier CSV souhaitez-vous "
                                                                                           "enregistrer le résultat"), "(*.csv)");

    AppendExtension(fichierDst); // Ajouter automatiquement l'extension

    if(!ok)
        return;

    ConversionIDXUbyteToCSV(fichierSrcImage.toStdString().c_str(), fichierSrcLabel.toStdString().c_str(),
                            fichierDst.toStdString().c_str());
}

// Menu Convertir
void MainWindow::on_actionPGMToCSV_triggered(bool )
{
    QStringList fichiersSrc = QFileDialog::getOpenFileNames(this, tr("Choisir un fichier de données à convertir"), dernierDossierVisite, "Images PGM Normalisées(*_N.pgm)");

    if(fichiersSrc.size()==0) // si aucun fichier sélectionné, arreter là
        return;

    // Dans quel fichier les enregistrer
    QString directory = QFileInfo(fichiersSrc[0]).absolutePath();
    QString fichierDst = QFileDialog::getSaveFileName(this, tr("Dans quel fichier CSV souhaitez-vous enregistrer le résultat"), directory);

    if(fichierDst.length()==0)
        return;

    AppendExtension(fichierDst); // Ajouter automatiquement l'extension

    for(QString fichierSrc : fichiersSrc)
    {
        ConversionPGMToCSV(fichierSrc.toStdString(), fichierDst.toStdString());
        Log(QString("Insertion du fichier %1 dans %2").arg(fichierSrc).arg(fichierDst));
    }

    setDernierDossierVisite(fichierDst);
}

void MainWindow::on_actionRemplacerValeurs_triggered(bool )
{
    QString fichierSrc = QFileDialog::getOpenFileName(this, tr("Choisir un fichier de données à manipuler"),
                                                      dernierDossierVisite, "Fichier CSV(*.csv)");
    if(fichierSrc.length()==0) // si aucun fichier sélectionné, arreter là
        return;

    bool ok=true;
    QString directory = QFileInfo(fichierSrc).absolutePath(),
            fichierDst = QFileDialog::getSaveFileName(this, tr("Fichier destination"), directory, "Fichier CSV(*.csv)");

    AppendExtension(fichierDst);

    if(!ok)
        return;

    // Encoder les valeurs seuil, max, min
    double seuil=QInputDialog::getDouble(this, tr("Valeur de seuil"), tr("Entrez la valeur de seuil"), 1, -INFINITY, INFINITY, 8, &ok);
    if(!ok)
        return;

    double max=QInputDialog::getDouble(this, tr("Valeur supérieur au seuil"), tr("Entrez la valeur de remplacement pour une entrée supérieure au seuil"), 0, -INFINITY, INFINITY, 8, &ok);
    if(!ok)
        return;

    double min=QInputDialog::getDouble(this, tr("Valeur de seuil"), tr("Entrez la valeur de remplacement pour une entrée inféieure au seuil"), 0, -INFINITY, INFINITY, 8, &ok);
    if(!ok)
        return;

    Log(QString("Seuiller le fichier %1 et résultat enregistré dans %2\nSeuil: %3\tMax: %4\tMin: %5").arg(fichierSrc, fichierDst).arg(seuil).arg(max).arg(min));
    RemplacerValeursCSV(fichierSrc.toStdString(), fichierDst.toStdString(), seuil, min, max);
    setDernierDossierVisite(fichierDst);
}

void MainWindow::on_actionCentrerReduire_triggered(bool)
{
    QString fichierSrc = QFileDialog::getOpenFileName(this, tr("Fichier à traiter"), dernierDossierVisite);
    if(fichierSrc.length()==0)
        return;

    QString directory = QFileInfo(fichierSrc).absolutePath(),
            fichierDst = QFileDialog::getSaveFileName(this, tr("Fichier destination"), directory);
    if(fichierDst.length()==0)
        return;

    AppendExtension(fichierDst); // Ajouter automatiquement l'extension

    Log(QString("Normaliser le fichier %1 et résultat enregistré dans %2").arg(fichierSrc).arg(fichierDst));
    if(!CentrerReduire(fichierSrc.toStdString(), fichierDst.toStdString()))
    {
        QMessageBox::information(this, tr("Erreur lors de l'action Centrer-Réduire"),
                                 tr("L'action a échoué !"));
        return;
    }
    setDernierDossierVisite(fichierDst);
}

void MainWindow::on_actionRetiretDiviser128_triggered(bool )
{
    QString fichierSrc = QFileDialog::getOpenFileName(this, tr("Fichier à traiter"), dernierDossierVisite);
    if(fichierSrc.length()==0)
        return;

    double mu = 255, sigma = 255;
    bool ok=true;

    mu = QInputDialog::getDouble(this, tr("Choisir une moyenne pour centrer"), tr("Moyenne"),
                                 mu, -INFINITY, +INFINITY, 2, &ok);
    if(!ok)
        return;

    sigma = QInputDialog::getDouble(this, tr("Choisir un écart-type pour réduire"), tr("Ecart-type"),
                                 sigma, -INFINITY, +INFINITY, 2, &ok);
    if(!ok)
        return;

    QString directory = QFileInfo(fichierSrc).absolutePath(),
            fichierDst = QFileDialog::getSaveFileName(this, tr("Fichier destination"), directory);
    if(fichierDst.length()==0)
        return;

    AppendExtension(fichierDst); // Ajouter automatiquement l'extension

    Log(QString("Normaliser le fichier %1 en retirant %3 et en divisant par %4 et résultat enregistré dans %2")
        .arg(fichierSrc).arg(fichierDst).arg(mu).arg(sigma));
    RetirerDiviserElements(fichierSrc.toStdString(), fichierDst.toStdString(), mu, 128);
    setDernierDossierVisite(fichierDst);
}

void MainWindow::on_actionCentrerReduireParam_triggered(bool )
{
    if(moyennesApprentissage.size()==0 || ecarttypesApprentissage.size()==0)
    {
        Log("Veuiller d'abord charger un fichier ayant des moyennes et des écarts-types!", LOG_ERROR_LEVEL);
        return;
    }

    QString fichierSrc = QFileDialog::getOpenFileName(this, tr("Fichier à traiter"), dernierDossierVisite);
    if(fichierSrc.length()==0)
        return;

    QString directory = QFileInfo(fichierSrc).absolutePath(),
            fichierDst = QFileDialog::getSaveFileName(this, tr("Enregistrer le résultat dans un fichier"), directory);
    if(fichierDst.length()==0)
        return;

    AppendExtension(fichierDst); // Ajouter automatiquement l'extension

    Log(QString("Normaliser le %1 et résultat enregistré dans %2").arg(fichierSrc).arg(fichierDst));
    if(!CentrerReduire(fichierSrc.toStdString(), fichierDst.toStdString(), moyennesApprentissage, ecarttypesApprentissage))
    {
        QMessageBox::information(this, tr("Erreur lors de l'action Centrer-Réduire"), tr("L'action a échoué !"));
        return;
    }
    setDernierDossierVisite(fichierDst);
}

// Menu Fichier
void MainWindow::on_actionEnregistrerNeurones_triggered(bool )
{
    if(neurones.size()==0)
    {
        QMessageBox::information(this, tr("Erreur"), tr("Veuillez d'abord créer un perceptron mono-couche!"));
        return;
    }

    QString fichier = QFileDialog::getSaveFileName(this, tr("Fichier destination"), dernierDossierVisite);

    AppendExtension(fichier); // Ajouter automatiquement l'extension

    if(!EnregistrerNeurones(neurones, fichier.toStdString()))
        Log(QString("Sauvegarde du perceptron mono-couche dans %1 a échoué").arg(fichier), LOG_ERROR_LEVEL);
    else
        Log(QString("Sauvegarde du perceptron mono-couche dans %1 a réussi").arg(fichier));

    setDernierDossierVisite(fichier);
}

void MainWindow::on_actionChargerNeurones_triggered(bool )
{
    QString fichier = QFileDialog::getOpenFileName(this, tr("Fichier à charger"), dernierDossierVisite, "CSV (*.csv)");

    if(fichier.length()==0) // si aucun dossier sélectionner
        return;

    if(!ChargerNeurones(neurones, fichier.toStdString()))
    {
        QMessageBox::information(this, tr("Erreur"), tr("Impossible de charger le fichier %1: vérifier s'il existe bien, s'il est bien formatté.").arg(fichier));
        Log(QString("Impossible de charger le fichier %1: vérifier s'il existe bien, s'il est bien formatté.").arg(fichier), LOG_ERROR_LEVEL);
        return;
    }

    // Afficher les informations dans le GUI
    ui->spinBoxNbNeuronesSortie->setValue(neurones.size());
    ui->spinBoxNbEntrees->setValue(neurones[0].poids.size());

    int comboboxIndex = ui->comboBoxFonctionActivation->findText(QString::fromStdString(FonctionToString(neurones[0].fonctionActivation)).toUpper());
    ui->comboBoxFonctionActivation->setCurrentIndex(comboboxIndex);

    Log(QString("Chargement du perceptron mono-couche à partir de %1 a réussi").arg(fichier));

    setDernierDossierVisite(fichier);
}

void MainWindow::on_actionEnregistrerReseauNeurones_triggered(bool )
{
    if(rdn.couches.size()==0)
    {
        QMessageBox::information(this, tr("Erreur"), tr("Veuillez d'abord créer un réseau de neurones!"));
        return;
    }

    QString fichier = QFileDialog::getSaveFileName(this, tr("Fichier destination"), dernierDossierVisite);

    AppendExtension(fichier); // Ajouter automatiquement l'extension

    if(!EnregistrerReseauNeurones(rdn, fichier.toStdString()))
        Log(QString("Sauvegarde du perceptron multi-couches dans %1 a échoué").arg(fichier), LOG_ERROR_LEVEL);
    else
        Log(QString("Sauvegarde du perceptron multi-couches dans %1 a réussi").arg(fichier));

    setDernierDossierVisite(fichier);
}

void MainWindow::on_actionChargerReseauNeurones_triggered(bool )
{
    QString fichier = QFileDialog::getOpenFileName(this, tr("Fichier à charger"), dernierDossierVisite, "CSV (*.csv)");

    if(fichier.length()==0) // si aucun dossier sélectionner
        return;

    if(!ChargerReseauNeurones(rdn, fichier.toStdString()) || rdn.couches.size()==0)
    {
        QMessageBox::information(this, tr("Erreur"), tr("Impossible de charger le fichier %1: vérifier s'il existe bien, s'il est bien formatté.").arg(fichier));
        Log(QString("Impossible de charger le fichier %1: vérifier s'il existe bien, s'il est bien formatté.").arg(fichier), LOG_ERROR_LEVEL);
        return;
    }

    // Afficher les informations dans le GUI
    ui->spinBoxNbNeuronesSortie->setValue(rdn.getCoucheSortie().size());
    ui->spinBoxNbCouches->setValue(rdn.couches.size()-1);
    ui->spinBoxNbNeuroneCache->setValue(rdn.couches[0].size());
    ui->spinBoxNbEntrees->setValue(rdn.couches[0][0].poids.size());

    int comboboxIndex = ui->comboBoxFonctionActivation->findText(QString::fromStdString(FonctionToString(rdn.couches[0][0].fonctionActivation)).toUpper());
    ui->comboBoxFonctionActivation->setCurrentIndex(comboboxIndex);

    ui->checkBoxSoftmax->setChecked(rdn.isSoftmax);

    Log(QString("Chargement du perceptron multi-couches à partir de %1 a réussi").arg(fichier));

    setDernierDossierVisite(fichier);
}

// Menu Paramètres
void MainWindow::on_actionChangerEcartTypesNeurones_triggered(bool )
{
    bool ok=true;
    double ecarttype = QInputDialog::getDouble(this, tr("Nouvel écart-type pour les neurones"), tr("Entrez le nouvel écart-type pour les neurones"),
                                               Neurone::sigma, -INFINITY, INFINITY, 8, &ok);
    if(ok)
    {
        Neurone::sigma = ecarttype;
        Log(QString("Écart-type des Neurones changée à %1").arg(Neurone::sigma));
    }
}

void MainWindow::on_actionChangerMoyenneNeurones_triggered(bool )
{
    bool ok=true;
    double moyenne= QInputDialog::getDouble(this, tr("Nouvelle moyenne pour les neurones"), tr("Entrez la nouvelle moyenne pour les neurones"),
                                            Neurone::mu, -INFINITY, INFINITY, 8, &ok);
    if(ok)
    {
        Neurone::mu = moyenne;
        Log(QString("Moyenne des Neurones changée à %1").arg(Neurone::mu));
    }
}

void MainWindow::on_actionReconnaitreCaracteresPlaques_triggered(bool )
{
    if(neurones.size()== 0 && rdn.couches.size()==0)
    {
        QMessageBox::information(this, tr("Reconnaître Caractères Plaques"), tr("Veuillez d'abord entraîner un perceptron ou un réseau de neurones"));
        return;
    }
    else if(moyennesApprentissage.size()==0 || ecarttypesApprentissage.size()==0)
    {
        QMessageBox::information(this, tr("Reconnaître Caractères Plaques"), tr("Veuillez charger un fichier CSV avec les moyennes et les écarts-types"));
        return;
    }

    QString fichier = QFileDialog::getOpenFileName(this, tr("Fichier à charger"), dernierDossierVisite, "PGM (*.pgm)");
    vector<vector<double>> caracteresPlaques;
    rowApprentissage=28; colApprentissage=28; // DEBUG

    // Extraire les caractères et les stocker dans caracteresPlaques
    DetectionCaracteresPlaque(fichier.toStdString(), rowApprentissage, colApprentissage, caracteresPlaques);

    vector<size_t> predictions(caracteresPlaques.size());
    for(size_t i=0; i<caracteresPlaques.size(); i++)
    {
        vector<double> tmpCaractere = caracteresPlaques[i]; // Créer et modifier une copie du caractère (le caractère est destiné à être affiché!)

        // Pré-traiter les données:
        CentrerReduire(tmpCaractere, moyennesApprentissage, ecarttypesApprentissage);

        // Analyser les sorties
        if(utiliserMLP)
        {
            rdn.Evaluer(tmpCaractere);
            predictions[i] = TrouverMaximum(rdn.getCoucheSortie());
        }
        else
        {
            // Analyser dans un perceptron mono-couche
            for(Neurone &n : neurones)
                n.Evaluer(tmpCaractere);

            predictions[i] = TrouverMaximum(neurones);
        }
    }

    setDernierDossierVisite(fichier);

    if(plaquesDialog!=NULL)
        delete plaquesDialog;

    plaquesDialog=new VisualiserPlaquesDialog(this);
    plaquesDialog->AfficherPlaque(fichier, caracteresPlaques, predictions, labels, rowApprentissage, colApprentissage);
}

void MainWindow::on_actionExtraireCaracPlaques_triggered(bool )
{
    // Récupérer les noms fichiers
    QStringList fichiersCaracteres = QFileDialog::getOpenFileNames(this, tr("Choisir les fichiers dont les caractéristiques seront extraites"),
                                                                   dernierDossierVisite, "PGM normalisés (*_N.pgm)");

    if(fichiersCaracteres.size()==0)
        return;

    QString fichierCSV = QFileDialog::getSaveFileName(this, tr("Où enregistrer les fichiers contenant les caractéristiques"),
                                                      dernierDossierVisite, "CSV (*.csv)");
    if(fichierCSV.length()==0)
        return;

    AppendExtension(fichierCSV); // Ajouter automatiquement l'extension

    ofstream fichierResultat(fichierCSV.toStdString());

    // Créer en-tête fichier
    fichierResultat << ",Surface, Perimetre, Trous, XG, YG,";
    for(size_t i=0; i<7; i++)
        fichierResultat << "Hu" << i+1 << SEP;
    fichierResultat << endl;

    for(const QString &fichier : fichiersCaracteres) // Pour chaque fichier
    {
        // Déclaration
        double surface = 0.0f, perimetre=0.0f, MomentsHu[7];
        int nbTrous=0, Xg=0, Yg=0;
        string string_fichier = fichier.toStdString();

        // Extraire les caractéristiques
        CalculerMomentsCaractere(string_fichier, MomentsHu, &perimetre, &surface, &nbTrous, &Xg, &Yg);

        // Les ecrire dans le fichier
        fichierResultat << string_fichier << SEP << surface << SEP << perimetre
                        << SEP << nbTrous << SEP << Xg << SEP << Yg;
        for(size_t i=0; i<7; i++)
            fichierResultat << SEP << MomentsHu[i];

        fichierResultat << endl;
        fichierResultat.flush(); // Vider le buffeur
    }

    fichierResultat.close(); // Fermer le fichier

    Log(QString("Les caractéristiques de %1 fichiers ont été extraites et enregistrées dans le fichier %2")
        .arg(fichiersCaracteres.size()).arg(fichierCSV)); // LOG
}

void MainWindow::on_pushButtonVisualiserPoids_clicked()
{
    if(neurones.size()==0 && rdn.couches.size()==0)
    {
        QMessageBox::information(this, tr("Erreur"), tr("Veuillez d'abord entrainer un réseau de neurone ou un perceptron mono-couche!"));
        return;
    }

    if(utiliserMLP)
    {
        bool ok= true;
        int indexe = QInputDialog::getInt(this, tr("Afficher une couche de poids"), tr("De quelle couche souhaitez-vous prendre les poids ?"), 0, 0, rdn.couches.size()-1,1, &ok );
        if(!ok)
            return;

        this->poidsDialog->AfficherPoids(rdn.couches[indexe], rowApprentissage, colApprentissage);
    }
    else
        this->poidsDialog->AfficherPoids(neurones, rowApprentissage, colApprentissage);
}

void MainWindow::on_pushButtonViderLog_clicked()
{
    ui->textBrowserLog->clear();
}
