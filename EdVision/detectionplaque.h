﻿#ifndef DETECTIONPLAQUE_H
#define DETECTIONPLAQUE_H

#include <string>
#include <iostream>
#include <vector>

// EdVision classique
#include "EdStructures.h"
#include "EdUtilities.h"
#include "EdLibThreshold.h"

// Composantes Connexes (Blob detection)
#include "CompatibiliteStructures.h"
#include "ClLibDecCompConnexesDF.h"
#include "DSCNormExtract.h"
#include "DSCUtilsVisu.h"

// Conversion EdIMAGE en vector<double>
#include "Donnees/conversion.h"

// Includes OpenCV
#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

using namespace std;

bool DetectionCaracteresPlaque(string nomFichier, int row, int col, vector<vector<double>> &caracteresPlaques);

bool ExtraireCaractere(EdIMAGE *imageBW, EdIMAGE *imageNE, PT_ClREGION TabClRegion, int indexe);

// Les images doivent être normalisées et en niveau de gris!
void CalculerMomentsCaractere(string image, double momentsHu[7], double *perimetre, double *surface, int *nbTrous, int *Xg, int *Yg);
void CalculerCaracteristiqueCaractere(string image);

// Extrait caractéristique hors d'UNE SEULE lettre contenue dans un fichier
bool ExtraireCaracteristiques(string nomFichier, double *surface, double *perimetre, int *nbTrous, int *Xg, int *Yg);
//bool ExtraireCaracteristiques(string nomFichierSrc, string nomFichierDest, int row, int col);

#endif // DETECTIONPLAQUE_H
