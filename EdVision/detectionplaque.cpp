﻿#include "detectionplaque.h"

bool DetectionCaracteresPlaque(string nomFichier, int row, int col, vector<vector<double>> &caracteresPlaques)
{
  // Variables images classiques
  EdIMAGE *image = NULL,*imres = NULL, *imEtq = NULL, *imNE = NULL, *imVisu = NULL;

  int nlig = 0, ncol = 0 , nligNE = row, ncolNE = col;
  unsigned char	prof = 0, profNE = 0;
  FILE *fichier = NULL;

  // Variables composantes connexes
  PT_ClREGION TabClRegion;
  int  nbcc = 0, nbetqinit = 0, surfmin = 200, taillemin = 15, Th = 128; // variables paramètres

// INITIALISATION DES STRUCTURES
  /* --- Allocation of Data Structures --- */
  TabClRegion = (PT_ClREGION)malloc((NBClREGIONMAX + 1)*sizeof(ClREGION));

  /* --- Source Image --- */
  if(!(fichier = fopen(nomFichier.c_str(), "rb")) || Reading_ImageHeader(fichier, &ncol, &nlig, &prof))
  {
    cerr << "Source Image " << nomFichier << " Pb of Opening" << endl;
    return false;
  }

  profNE = prof;
  cerr << "Size of the Image : " << nlig << "  lines x " << ncol << " colums : Type : "<< (int) prof << endl;

  /* --- Creation of Images Header and Data --- */
  if (crea_IMAGE(image) == NULL || crea_IMAGE(imNE) == NULL ||  /* creation of Image Headers  */
      crea_IMAGE(imres) == NULL || crea_IMAGE(imEtq) == NULL ||
      crea_IMAGE(imVisu) == NULL)
  {
    cerr << "Error of Memory Allocation" << endl;
    return false;
  }

  if (!Creation_Image(image, nlig, ncol, prof) || !Creation_Image(imres, nlig, ncol, prof) || // Image Data
      !Creation_Image (imNE, nligNE, ncolNE, profNE) || !Creation_Image (imEtq, nlig, ncol, /*prof*/2) ||
      !Creation_Image (imVisu, nlig, ncol, /*prof*/3))
  {
    cerr << "Error of Memory Allocation of Pixels" << endl;
    return false;
  }

  /* --- Reading of Umage Data from file to Memory --- */
  if (!Reading_ImageData(fichier,image))  // Image Pixel Data
  {
    cerr << "Problem of Reading in " << nomFichier << endl;
    return false;
  }

  fclose(fichier); // Plus besoin du fichier!

// PROCESSING IMAGE
  if(ClassicThreshold(image, imres, Th)!=0 ||
     !DecComposanteConnexeDF(imres, imEtq, &nbcc, &nbetqinit, NBClREGIONMAX, TabClRegion))
  {
    cerr << "Erreur pendant le processing" << endl;
    return false;
  }

  /* --- Filtrage sur la taille des Composantes connexes --- */
  FiltrageTailleCompConnexe(imEtq, TabClRegion, &nbcc, (long)surfmin, taillemin);
  cerr << "Nombre de Composantes apres Filtrage : " << nbcc << endl;

  /* --- Vizualisation --- */
  Gray_To_Color(image, imVisu);

   /* --- Letters and Figures Detection --- */
  LetterAndFigureDetection(imEtq, imVisu, nbcc, TabClRegion);

  /* --- Normalized Letters / Figures Extraction --- */
  caracteresPlaques=vector<vector<double>>(0);
  for(size_t i=0; i<(size_t) nbcc; i++)
  {
    if(!ExtraireCaractere(image, imNE, TabClRegion, i))
      continue;

    vector<double> tmp;
    if (TabClRegion[i].fRec != FALSE)
    {
      ConversionEdImageToVector(imNE, tmp);
      if(tmp.size()>0)
        caracteresPlaques.push_back(tmp);
    }
  }

// LIBERATION DE LA MEMOIRE
  /* --- Free of Images --- */
  if(!Free_Image(image) || !Free_Image(imNE) || !Free_Image(imEtq) || !Free_Image(imVisu))
  {
    cerr << "Problem of Free the Memory" << endl;
    return false;
  }

  free(TabClRegion);

  return true;
}

bool ExtraireCaractere(EdIMAGE *imBW, EdIMAGE *imNE, PT_ClREGION TabClRegion, int indexe)
{
  if (TabClRegion[indexe].fRec == FALSE) // reconnaître un caractère d'une plaque d'immatriculation
  {
    cout << "Element à l'indexe " << indexe << " ignoré" << endl;
    return true;
  }

  //cerr << "Extraction Composante " << indexe << endl;

  if(NormalizedExtractionBW(imBW, imNE, &(TabClRegion[indexe].box))>0) // >0 -> erreur
  {
    cerr << "Extraire Lettre [" << indexe << " ]: erreur lors de l'action NormalizedExtractionBW" << endl;
    return false;
  }

  return true;
}


void CalculerMomentsCaractere(string image, double momentsHu[], double *perimetre, double *surface, int *nbTrous, int *Xg, int *Yg)
{
  cv::Mat caractere = cv::imread(image, CV_8UC1),
      caractereSeuille, caractereInverse;
  vector<vector<cv::Point>> contours;
  vector<cv::Vec4i> hierarchy;

  cv::threshold(caractere, caractereSeuille, 128, 255, cv::THRESH_OTSU | cv::THRESH_BINARY); // Seuillage
  cv::bitwise_not(caractereSeuille, caractereInverse); // Nécessaire avant la détection des contours (contours = pixels > 0, or les caractères sont en noir dans notre cas)
  cv::findContours(caractereInverse, contours, hierarchy, cv::RETR_CCOMP, cv::CHAIN_APPROX_SIMPLE); // Détection des contours des caractères

  cv::Moments moments = cv::moments(contours[0]);
  cv::HuMoments(moments, momentsHu);

  // Calculer Centre de Gravité
  *Xg = int(moments.m10/moments.m00);
  *Yg = int(moments.m01/moments.m00);

  // Calculer périmètre de la lettre
  *perimetre = cv::arcLength(contours[0], false);

  // Calculer surface de la lettre
  *surface = cv::contourArea(contours[0]);

  // Trouver le nombre de trous
  *nbTrous=0;
  int indexe = hierarchy[0].val[2];
  //vector<vector<cv::Point>> contoursTrous;// DEBUG
  if(indexe!=-1) // Vérifier si first_child est != -1
  { // Si oui -> il y a des trous et il faut les parcourir
    while(indexe!=-1)
    {
      //contoursTrous.push_back(contours[indexe]); // DEBUG
      indexe = hierarchy[indexe].val[0]; // indice du trou suivant (next), s'il existe
      *nbTrous += 1;
    }
  }

  // DEBUG : afficher les contours, les trous et le CG
  // Dessiner contours extérieurs en bleu
  /*cv::Mat drawing=cv::Mat::zeros(caractere.size(), CV_8UC3);
  cv::RNG rng(12345);
  cv::drawContours(drawing, contours, 0, cv::Scalar(255, 0, 0), 2, 8, hierarchy, 0, cv::Point() );

  // Dessiner trous en verts
  for(size_t i=0; i<contoursTrous.size(); i++)
    cv::drawContours(drawing, contoursTrous, i, cv::Scalar(0, 255, 0), 2, 8, hierarchy, 0, cv::Point() );

  // Dessiner CG en rouge
  cv::circle(drawing, cv::Point(*Xg, *Yg), 1, cv::Scalar(0, 0, 255));

  cv::imshow(image, drawing);
  cv::waitKey();*/

  // Afficher les caractéristiques
  /*cout << "Caracteristiques autres: Surface: " << surface << "\tPerimetre: " << perimetre << "\tNbTrous: "
       << nbTrous << "\t Centre de gravité (X,Y) : " << Xg << ',' << Yg << endl;*/
}

bool ExtraireCaracteristiques(string nomFichier, double *surface, double *perimetre, int *nbTrous, int *Xg, int *Yg)
{
  // Variables images classiques
  EdIMAGE *image = NULL,*imres = NULL, *imEtq = NULL, *imNE = NULL, *imVisu = NULL;

  int nlig = 0, ncol = 0 , nligNE = 0, ncolNE = 0;
  unsigned char	prof = 0, profNE = 0;
  FILE *fichier = NULL,
       *fichVisu = NULL; // DEBUG

  // Variables composantes connexes
  PT_ClREGION TabClRegion;
  int  nbcc = 0, nbetqinit = 0, surfmin = 200, taillemin = 15, Th = 128; // variables paramètres

// INITIALISATION DES STRUCTURES
  /* --- Allocation of Data Structures --- */
  TabClRegion = (PT_ClREGION)malloc((NBClREGIONMAX + 1)*sizeof(ClREGION));

  /* --- Source Image --- */
  if(!(fichier = fopen(nomFichier.c_str(), "rb")) || Reading_ImageHeader(fichier, &ncol, &nlig, &prof))
  {
    cerr << "Source Image " << nomFichier << " Pb of Opening" << endl;
    return false;
  }

  profNE = prof;
  nligNE=nlig; // Taille Normalized Extracted Image = Taille Image (28 x 28 par exemple)
  ncolNE = ncol;
  cerr << "Size of the Image : " << nlig << "  lines x " << ncol << " colums : Type : "<< (int) prof << endl;

  /* --- Creation of Images Header and Data --- */
  if (crea_IMAGE(image) == NULL || crea_IMAGE(imNE) == NULL ||  /* creation of Image Headers  */
      crea_IMAGE(imres) == NULL || crea_IMAGE(imEtq) == NULL ||
      crea_IMAGE(imVisu) == NULL)
  {
    cerr << "Error of Memory Allocation" << endl;
    return false;
  }

  if (!Creation_Image(image, nlig, ncol, prof) || !Creation_Image(imres, nligNE, ncolNE, prof) || // Image Data
      !Creation_Image (imNE, nligNE, ncolNE, profNE) || !Creation_Image (imEtq, nligNE, ncolNE, /*prof*/2) ||
      !Creation_Image (imVisu, nligNE, ncolNE, /*prof*/3))
  {
    cerr << "Error of Memory Allocation of Pixels" << endl;
    return false;
  }

  if(!(fichVisu = fopen("/home/julien/Documents/Lettre_X.pgm","wb"))) // DEBUG
  {
    cerr << "Result Visualization Image " << "/home/julien/Documents/Lettre_X.pgm" << " Pb of Opening" << endl;
    return false;
  }

  /* --- Reading of Umage Data from file to Memory --- */
  if (!Reading_ImageData(fichier,image))  // Image Pixel Data
  {
    cerr << "Problem of Reading in " << nomFichier << endl;
    return false;
  }

// PROCESSING IMAGE
  if(ClassicThreshold(image, imres, Th)!=0 ||
     !DecComposanteConnexeDF(imres, imEtq, &nbcc, &nbetqinit, NBClREGIONMAX, TabClRegion))
  {
    cerr << "Erreur pendant le processing" << endl;
    return false;
  }

  //Filtrage sur la taille des Composantes connexes
  FiltrageTailleCompConnexe(imEtq, TabClRegion, &nbcc, (long)surfmin, taillemin);
  cerr << '[' << nomFichier << "] Nombre de Composantes apres Filtrage : " << nbcc << endl;

  // Vizualisation
  Gray_To_Color(image, imVisu);

   // Letters and Figures Detection
  LetterAndFigureDetection(imEtq, imVisu, nbcc, TabClRegion);

  for(size_t i=0; i<(size_t) nbcc; i++)
  {
    // Enregistrer les caractéristiques
    if(TabClRegion[i].fRec==FALSE)
      continue;

    *surface = (double) TabClRegion[i].surf;
    *perimetre = (double) TabClRegion[i].perim;
    *nbTrous = TabClRegion[i].nbHole;
    *Xg = TabClRegion[i].ptg.x;
    *Yg = TabClRegion[i].ptg.y;
  }

  /*if(!ExtraireCaractere(image, imNE, TabClRegion, 0))
    return false;*/

  // DEBUG
  fprintf(fichVisu,"P6\n#creating by EdEnviTI\n%d %d\n255\n",(int)ncol, (int)nlig); // Header
  if(!Writing_ImageData(fichVisu, imVisu)) // Image Pixel Data
  {
    cerr << "Problem of Writing fichVisu" << endl;
    return false;
  }

// LIBERATION DE LA MEMOIRE
  fclose(fichier);
  fclose(fichVisu); // DEBUG

  /* --- Free of Images --- */
  if(!Free_Image(image) || !Free_Image(imNE) || !Free_Image(imEtq) || !Free_Image(imVisu))
  {
    cerr << "Problem of Free the Memory" << endl;
    return false;
  }

  free(TabClRegion);

  return true;
}

//bool ExtraireCaracteristiques(string nomFichierSrc, string nomFichierDest, int row, int col)
//{
//  // Variables images classiques
//  EdIMAGE *image = NULL,*imres = NULL, *imEtq = NULL, *imNE = NULL, *imVisu = NULL;

//  int nlig = 0, ncol = 0 , nligNE = row, ncolNE = col;
//  unsigned char	prof = 0, profNE = 0;
//  FILE *fichier = NULL,
//       *fichVisu = NULL; // DEBUG

//  // Variables composantes connexes
//  PT_ClREGION TabClRegion;
//  int  nbcc = 0, nbetqinit = 0, surfmin = 200, taillemin = 15, Th = 128; // variables paramètres

//  // paramètres
//  /*double surface = 0.0f, perimetre=0.0f;
//  int nbTrous=0, Xg=0, Yg=0;*/

//// INITIALISATION DES STRUCTURES
//  /* --- Allocation of Data Structures --- */
//  TabClRegion = (PT_ClREGION)malloc((NBClREGIONMAX + 1)*sizeof(ClREGION));

//  /* --- Source Image --- */
//  if(!(fichier = fopen(nomFichierSrc.c_str(), "rb")) ||
//     Reading_ImageHeader(fichier, &ncol, &nlig, &prof))
//  {
//    cerr << "Source Image " << nomFichierSrc << " Pb of Opening" << endl;
//    return false;
//  }

//  profNE = prof;
//  cerr << "Size of the Image : " << nlig << "  lines x " << ncol << " colums : Type : "<< (int) prof << endl;

//  /* --- Creation of Images Header and Data --- */
//  if (crea_IMAGE(image) == NULL || crea_IMAGE(imNE) == NULL ||  /* creation of Image Headers  */
//      crea_IMAGE(imres) == NULL || crea_IMAGE(imEtq) == NULL ||
//      crea_IMAGE(imVisu) == NULL)
//  {
//    cerr << "Error of Memory Allocation" << endl;
//    return false;
//  }

//  if (!Creation_Image(image, nlig, ncol, prof) || !Creation_Image(imres, nligNE, ncolNE, prof) || // Image Data
//      !Creation_Image (imNE, nligNE, ncolNE, profNE) || !Creation_Image (imEtq, nligNE, ncolNE, /*prof*/2) ||
//      !Creation_Image (imVisu, nligNE, ncolNE, /*prof*/3))
//  {
//    cerr << "Error of Memory Allocation of Pixels" << endl;
//    return false;
//  }

//  if(!(fichVisu = fopen("/home/julien/Documents/Lettre_X.pgm","wb"))) // DEBUG
//  {
//    cerr << "Result Visualization Image " << "/home/julien/Documents/Lettre_X.pgm" << " Pb of Opening" << endl;
//    return false;
//  }

//  /* --- Reading of Umage Data from file to Memory --- */
//  if (!Reading_ImageData(fichier,image))  // Image Pixel Data
//  {
//    cerr << "Problem of Reading in " << nomFichierSrc << endl;
//    return false;
//  }

//// PROCESSING IMAGE
//  if(ClassicThreshold(image, imres, Th)!=0 ||
//     !DecComposanteConnexeDF(imres, imEtq, &nbcc, &nbetqinit, NBClREGIONMAX, TabClRegion))
//  {
//    cerr << "Erreur pendant le processing" << endl;
//    return false;
//  }

//  //Filtrage sur la taille des Composantes connexes
//  FiltrageTailleCompConnexe(imEtq, TabClRegion, &nbcc, (long)surfmin, taillemin);
//  cerr << '[' << nomFichierSrc << "] Nombre de Composantes apres Filtrage : " << nbcc << endl;

//  // Vizualisation
//  Gray_To_Color(image, imVisu);

//   // Letters and Figures Detection
//  LetterAndFigureDetection(imEtq, imVisu, nbcc, TabClRegion);

//  // DEBUG
//  //fprintf(fichVisu,"P6\n#creating by EdEnviTI\n%d %d\n255\n",(int)ncol, (int)nlig); // Header
//  /*if(!Writing_ImageData(fichVisu, imVisu)) // Image Pixel Data
//  {
//    cerr << "Problem of Writing fichVisu" << endl;
//    return false;
//  }*/

//// LIBERATION DE LA MEMOIRE
//  fclose(fichier);
//  fclose(fichVisu); // DEBUG

//  /* --- Free of Images --- */
//  if(!Free_Image(image) || !Free_Image(imNE) || !Free_Image(imEtq) || !Free_Image(imVisu))
//  {
//    cerr << "Problem of Free the Memory" << endl;
//    return false;
//  }

//  free(TabClRegion);

//  return true;
//}

void CalculerCaracteristiqueCaractere(string image)
{
  cv::Mat caractere = cv::imread(image, CV_8UC1),
      caractereSeuille, caractereInverse;
}
