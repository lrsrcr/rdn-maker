// DSCNormExtract.c
// Extraction normalisée
#include <string.h>
#include "EdStructures.h"
#include "CompatibiliteStructures.h"
#include "EdUtilities.h"

int NormalizedExtractionBW (EdIMAGE *imBW, EdIMAGE *imNE, EdBOX *box)
{
   EdPOINT   *point, *pointn;
   int dim_x, dim_y;

   if(crea_POINT(point) == NULL)
   {
     fprintf(stderr,"Memory Allocation Error : Label_Convertion \n");
     return 1;
   }
   if(crea_POINT(pointn) == NULL)
   {
     fprintf(stderr,"Memory Allocation Error : Label_Convertion \n");
     return 1;
   }    
   
   dim_x = BOX_XMAX(box) - BOX_XMIN(box) + 1;
   dim_y = BOX_YMAX(box) - BOX_YMIN(box) + 1;

   if(dim_x<0 || dim_y<0)
       return 1;
   
   // fprintf(stderr,"Taille de l'Image Extraite : %d x %d \n",dim_x, dim_y);
   
   for (POINT_Y(pointn) = 0; POINT_Y(pointn) < NLIG(imNE);
        POINT_Y(pointn)++)
   for (POINT_X(pointn) = 0; POINT_X(pointn) < NCOL(imNE);
        POINT_X(pointn)++)
   {
      POINT_X(point) = BOX_XMIN(box) + (int)((double)POINT_X(pointn) * (double)dim_x
                                             / (double)NCOL(imNE) + 0.5);
      POINT_Y(point) = BOX_YMIN(box) + (int)((double)POINT_Y(pointn) * (double)dim_y
                                             / (double)NCOL(imNE) + 0.5);
      PIXEL(imNE, pointn) = PIXEL(imBW, point);
      
   } /* end of Normalized Extracted Image Computation */ 
          
   
   free((void *)point);
   free((void *)pointn);

   return 0;
}

/* --------------------------------------------------------------- */
int ExtractNormalizedImages (EdIMAGE *imBW, EdIMAGE *imNE,  
                            PT_ClREGION TabClRegion, int nbcc, char *nom)
{
   FILE *fp;
   int ncc;
   int ret;
   int nb_lf = 0;
   char NomImNE[500];
   int nlig = NLIG(imNE);
   int ncol = NCOL(imNE);
   //unsigned char prof = PROF(imNE);
   
   
  for (ncc = 1; ncc <= nbcc; ncc++)
  {
     if (TabClRegion[ncc].fRec == FALSE)
        continue;
     fprintf(stderr,"Extraction Composante %d\n",ncc);
     ret = NormalizedExtractionBW (imBW, imNE, &(TabClRegion[ncc].box));
     sprintf(NomImNE,"%s_%d_N.pgm",nom,nb_lf);
     fprintf(stderr,"Extraction Image %s\n",NomImNE);
     nb_lf++;
     
     if(!(fp = fopen(NomImNE,"wb")))
     {
       fprintf(stderr,"Extracted Image %s Pb of Opening\n",NomImNE);
       //system ("PAUSE"); // Windows Only
       exit(0);
     }
     
     /* --- Writing of the Image Result in File --- */
     fprintf(fp,"P5\n#creating by EdEnviTI\n%d %d\n255\n",(int)ncol, (int)nlig); // Header 
     ret = Writing_ImageData(fp, imNE); // Image Pixel Data
     
     if (!ret)
     {
       fprintf(stderr,"Problem of Writing \n");
       //system ("PAUSE"); // Windows Only
       exit(0);
     }
     
     fclose (fp); 
  }
  return 1;
}

