#include "EdStructures.h"
typedef EdIMAGE IMAGE;
typedef EdPOINT POINT;
typedef EdBOX BOX;

/* --- Structure ClRegion --- */
#define COMPCONNEXE 1
#define REGCCNB 1
#define REGCCCOUL 1
#define REGNEW 1
#define REGNEWCOUL 1
#define LICENCEPLATE 1

struct ClRegion
{
  
  int         etiqeq; /* etiquette equivalente */
  int         activ;  /* pour la fusion */
  
  /* --- Proprietes Topologiques --- */
  POINT         ptg;    /* Centre de Gravite */
  long          surf;   /* surface */
  BOX           box;    /* boite englobante */
  
  /* --- Element Cumulatifs --- */
  long          som_x;  /* somme des x */
  long          som_y;  /* somme des y */

#if COMPCONNEXE
  /* --- Composante Connexe --- */
  unsigned char niv;    /* niveau */
  
#endif

#if REGCCNB 
  /* --- Regions en N&B --- */
  unsigned char moy_lum; /* luminance moyenne */
  long          som_lum; /* somme des luminances */
#endif

#if REGCCCOUL 
  /* --- Regions en imagerie Couleur : DE et DS --- */
  unsigned char moy_lumPl1; /* luminance moyenne sur le Plan 1 */
  unsigned char moy_lumPl2; /* luminance moyenne sur le Plan 2 */
  unsigned char moy_lumPl3; /* luminance moyenne sur le Plan 3 */

  long          som_lumPl1; /* somme des luminances du Plan 1 */
  long          som_lumPl2; /* somme des luminances du Plan 2 */
  long          som_lumPl3; /* somme des luminances du Plan 3 */
#endif

#if REGNEW
  unsigned char moy_lum_init;
  int           etiqeqcpr;

  unsigned char conf1; /* regroupement des pixels voisins */
  unsigned char conf2; /* regroupement des regions 3x3 voisines */
  unsigned char conf3; /* regroupement des regions 9x9 voisines */
  unsigned char etat; 
  
  // 0 : Non etablie ;                 1 : Region non regroupee 3x3
  // 2 : Region non regroupee 9x9 ;    3 : Region non regroupee 27 x 27
  // 4 : Region Fusionnee              
  // 5 : Region en Croissance

  unsigned char rgp; /* region regroupee lors du 4eme regroupement */

#endif

#if REGNEWCOUL
  unsigned char moy_lum_initPl1;
  unsigned char moy_lum_initPl2;
  unsigned char moy_lum_initPl3;
#endif


#if LICENCEPLATE
  unsigned char fRec;
  int nbHole;
  int perim;
#endif 



};

typedef struct ClRegion ClREGION;
typedef ClREGION* PT_ClREGION; /* pointeur sur la structure Clregion */
