// DSCNormExtract.h
// Extraction normalisée

int NormalizedExtractionBW (EdIMAGE *imBW, EdIMAGE *imNE, EdBOX *box);
int ExtractNormalizedImages (EdIMAGE *imBW, EdIMAGE *imNE,  
                            PT_ClREGION TabClRegion, int nbcc, char *nom);
