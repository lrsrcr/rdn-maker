﻿// ------------------------------------------------------------------
/**
 * @file 	ClLibDecCompConnexesDF.c
 *
 * @brief 	Fonctions de Decomposition en Composantes Connexes,
 * selon l'Algorithme de Rosenfield et Pflatz
 * et Adaptation a une Croissance de Regions Simple
 * en imagerie en niveaux de gris et couleur a donnees entrelacees
 * et separees
 * 
 * @author 	Patrick J. Bonnin
 * @email  	bonnin@iutv.univ-paris13.fr
 * @date 2004.05.28 : creation.
 * @date 2004.05.28 : last modification.
 */
// ------------------------------------------------------------------
/* COPYRIGHT (C)	2004, P. Bonnin <bonnin@iutv.univ-paris13.fr>
 *
 * This  library  is  a free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as  published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This  Library  is  distributed in the hope that it will be useful,
 * but  WITHOUT  ANY  WARRANTY;  without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULIAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You  should  have received a copy of the GNU Lesser General Public 
 * License  along  with  this  library;  if  not,  write  to the Free 
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
// ------------------------------------------------------------------
/* Modifications :
 * 2004.05.28 : creation
 */
// ------------------------------------------------------------------
// !!!!!!!!!!! ATTENTION: ATTENTION: ATTENTION: !!!!!!!!!!!!!!!!
// POSITIONNER les INDICATEURS COMPCONNEXE REGCCNB REGCCCOUL 
// dans ClStructures.h 
//
// Fonction : void MiseAJourTablEquivDF( int etq, 
//          : int anetq, int nvetq, 
//          : PT_ClREGION TabClRegion)
// Role     : gere les probleme de transitivite lors de la detection
//          : d'etiquettes equivalentes
//
// Fonction : int CompactageDF (int etq, PT_ClREGION TabClRegion)
// Role     : reetiquette le tableau d'equivalences sans trou et en le 
//          : compactant en effectuant la fusion et la remise a jour des 
//          : parametres cumulatifs puis du centre de gravite
//
// Fonction : void DecComposanteConnexeDF (unsigned char *pdtsrc,
//	    : int *pdtetq, int nlig, int ncol,  int *pnbcc,
//          : int *pnetqinit, int netqmax, PT_ClREGION TabClRegion)
// Role     : Decomposition en composantes connexes (procedure principale)
//          : incluant les deux fonctions precedentes,selon l'algorithme
//          : de A.Rosenfeld & JL Pflaz
//
// Fonction : void DecComposanteConnexeSaufZeroDF (unsigned char *pdtsrc,
//	    : int *pdtetq, int nlig, int ncol, int *pnbcc,
//          : int *pnetqinit, int netqmax, PT_ClREGION TabClRegion)
// Role     : idem fonction precedente, mais ne considere pas  
//          : niveau zero (fond a ne pas etiqueter!). Interet : Rapidite 
//
// Fonction : void FiltrageTailleCompConnexe(IMAGE *imetq, 
//          : PT_ClREGION TabClRegion, int *pnbcc, long surfmin, 
//          : int taillemin)
// Role     : Elimine les Composantes Connexes de taille
//	    : inferieure a taillemin, avec une taille minimale de 
//          : la boite englobante
//
// Fonction : void FusionCompConnexe(IMAGE *imetq, PT_ClREGION TabClRegion
//          : int *pnbcc, int dmax)
// Role     : Fusionne deux composantes connexes de meme niveau
//          : dont les boites englobantes sont proches.
//          : Attention la composante obtenue n'est plus connexe
//
// --- Utilitaires de Test ---
// Fonction : void EnregistreComposantesConnexes(FILE *fp,
//          : PT_ClREGION TabClRegion, int nbccf)
// Role     : Enregistrement dans un fichier les composantes et de leurs
//          : parametres
//
// Fonction : void TraceComposantesConnexes (IMAGE *imcetq, 
//          : PT_ClREGION TabClRegion, int nbcc)
// Role     : Trace le centre de gravite et la boite englobante. Permet
//          : notamment de visualiser les points de contact.
//
// --- Adaptation a la Croissance de Regions
//
// Fonction : void RegionNBFromComposanteConnexeDF (unsigned char *pdtsrc,
//	    : int *pdtetq, int nlig, int ncol,  int *pnbcc,
//          : int *pnetqinit, int netqmax, PT_ClREGION TabClRegion,
//          : int difmax)
// Role     : Extraction des Regions en Imagerie Noire et Blanc (niveaux 
//          : de gris a partir de l'adaptation de l'algorithme de
//          : de A.Rosenfeld & JL Pflaz
//
// Fonction : void RegionCoulDEFromComposanteConnexeDF (unsigned char *pdtsrc,
//	    : int *pdtetq, int nlig, int ncol,  int *pnbcc,
//          : int *pnetqinit, int netqmax, PT_ClREGION TabClRegion,
//          : int difmax1, int difmax2, int difmax3)
// Role     : Extraction des Regions en Imagerie Couleur a donnees 
//          : entrelacees a partir de l'adaptation de l'algorithme de
//          : de A.Rosenfeld & JL Pflaz


#include "CompatibiliteStructures.h"
#define INFO 0

// MiseAJourTablEquivDF ---------------------------------------------
/** 
 * @brief	Mise a Jour du Tableau des Equivalences.
 * 
 * Gere les problemes de transitivite lors de la detection 
 * d'etiquettes equivalentes.
 *		
 * @param	etq   : nombre de composantes connexes initiales +1 
 * @param	anetq : ancienne etiquette : etiquette disparaissant 
 * lors de la fusion
 * @param	nvetq : nouvelle etiquette : etiquette remplacant l'ancienne
 * @param	TabClRegion : Tableau de Structures
 *
 * @return None.
 */
// ------------------------------------------------------------------
void MiseAJourTablEquivDF(int etq, int anetq,
			  int nvetq, PT_ClREGION TabClRegion)
{
  int   k=0;  /* indice du tableau */
  
  if(anetq == nvetq)/* si les deux etiqettes equivalentes sont identiques */
    return;         /* pas besoin de remettre a jour le tableau */
  
  for(k = 1; k < etq; k++)
    if(TabClRegion[k].etiqeq == anetq)
      TabClRegion[k].etiqeq = nvetq;
  
} /* fin de la procedure de mise a jour */
/* --------------------------------------------------------- */

// CompactageDF -----------------------------------------------------
/** 
 * @brief  	Procedure de Compactage des Etiquettes.
 *
 * - reetiquette sans trous le tableau des equivalences et le compacte
 * - Effectue la Fusion des Composantes Connexes et la Mise a jour des 
 * cumulatifs parametres
 *		
 * @param	etq : nombre de regions crees + 1
 * @param 	TabClRegion : Tableau de structures
 *
 * @return	entier
 */
// ------------------------------------------------------------------
int CompactageDF (int etq, PT_ClREGION TabClRegion)
{
  int       k=0, l=0;             /* indices du tableau : 2 balayages */
  int       nbetq = 1;       /* nombre d'etiquettes (+ 1!) */
  PT_ClREGION   local_pointeur_ClRegion, local_pointeur_ClRegion_sec;
  PT_ClREGION   local_pointeur_ClRegion_new;

  for(k = 1; k < etq; k++)
  {
    local_pointeur_ClRegion = &(TabClRegion[k]);
    if(local_pointeur_ClRegion->etiqeq == k)
    {

      local_pointeur_ClRegion->etiqeq = nbetq;

      local_pointeur_ClRegion_new = &(TabClRegion[nbetq]);
      
      /* --- Recopiage des info sauf l'etiquette equivalente --- */
#if COMPCONNEXE
      local_pointeur_ClRegion_new->niv = local_pointeur_ClRegion->niv;
#endif

      local_pointeur_ClRegion_new->som_x = local_pointeur_ClRegion->som_x;
      local_pointeur_ClRegion_new->som_y = local_pointeur_ClRegion->som_y;
      local_pointeur_ClRegion_new->surf = local_pointeur_ClRegion->surf;

#if REGCCNB
      local_pointeur_ClRegion_new->som_lum = local_pointeur_ClRegion->som_lum;
#endif
#if REGCCCOUL
      local_pointeur_ClRegion_new->som_lumPl1 = 
	local_pointeur_ClRegion->som_lumPl1;
      local_pointeur_ClRegion_new->som_lumPl2 = 
	local_pointeur_ClRegion->som_lumPl2;
      local_pointeur_ClRegion_new->som_lumPl3 = 
	local_pointeur_ClRegion->som_lumPl3;
#endif

      local_pointeur_ClRegion_new->box.box_xmin = 
	local_pointeur_ClRegion->box.box_xmin;
      local_pointeur_ClRegion_new->box.box_xmax = 
	local_pointeur_ClRegion->box.box_xmax;
      local_pointeur_ClRegion_new->box.box_ymin = 
	local_pointeur_ClRegion->box.box_ymin;
      local_pointeur_ClRegion_new->box.box_ymax = 
	local_pointeur_ClRegion->box.box_ymax;
 
      
	/* --- Reetiquetage de la fin du tableau --- */
      for (l = k + 1; l < etq; l++)
      {
	local_pointeur_ClRegion_sec=&(TabClRegion[l]);
	
	if(local_pointeur_ClRegion_sec->etiqeq == k ) 
	  /* mise a jour de la structure */
	{
	  
	  local_pointeur_ClRegion_new->som_x += 
	    local_pointeur_ClRegion_sec->som_x;
	  local_pointeur_ClRegion_new->som_y += 
	    local_pointeur_ClRegion_sec->som_y;

#if REGCCNB
	  local_pointeur_ClRegion_new->som_lum += 
	    local_pointeur_ClRegion_sec->som_lum;
#endif
#if REGCCCOUL
	  local_pointeur_ClRegion_new->som_lumPl1 += 
	    local_pointeur_ClRegion_sec->som_lumPl1;
	  local_pointeur_ClRegion_new->som_lumPl2 += 
	    local_pointeur_ClRegion_sec->som_lumPl2;
	  local_pointeur_ClRegion_new->som_lumPl3 += 
	    local_pointeur_ClRegion_sec->som_lumPl3;
#endif
	  local_pointeur_ClRegion_new->surf += 
	    local_pointeur_ClRegion_sec->surf;

	  if(local_pointeur_ClRegion_new->box.box_xmin > 
	     local_pointeur_ClRegion_sec->box.box_xmin)
	    local_pointeur_ClRegion_new->box.box_xmin = 
	      local_pointeur_ClRegion_sec->box.box_xmin;
	  
	  if(local_pointeur_ClRegion_new->box.box_xmax < 
	     local_pointeur_ClRegion_sec->box.box_xmax)
	    local_pointeur_ClRegion_new->box.box_xmax = 
	      local_pointeur_ClRegion_sec->box.box_xmax;

	  if(local_pointeur_ClRegion_new->box.box_ymin > 
	     local_pointeur_ClRegion_sec->box.box_ymin)
	    local_pointeur_ClRegion_new->box.box_ymin = 
	      local_pointeur_ClRegion_sec->box.box_ymin;
	  
	  if(local_pointeur_ClRegion_new->box.box_ymax < 
	     local_pointeur_ClRegion_sec->box.box_ymax)
	    local_pointeur_ClRegion_new->box.box_ymax = 
	      local_pointeur_ClRegion_sec->box.box_ymax;
	  
	  local_pointeur_ClRegion_sec->etiqeq = 
	    local_pointeur_ClRegion->etiqeq;

	} /* fin de si l'etiquette courante l est equivalente a nbetq */
      } /* fin de retiquetage de la fin du tableau */
      nbetq++; /* une etiquette principale de plus */
    } /* fin du test si l'etiquette est "principale" */

  }/* fin balayage global du tableau */
  
  /* --- Calcul Final du Centre de Gravite et autres ... --- */
  for(k = 1; k < nbetq; k++)
  {
    local_pointeur_ClRegion = &(TabClRegion[k]);

    local_pointeur_ClRegion->ptg.x = 
      (int)(local_pointeur_ClRegion->som_x / local_pointeur_ClRegion->surf);
    local_pointeur_ClRegion->ptg.y = 
      (int)(local_pointeur_ClRegion->som_y / local_pointeur_ClRegion->surf);

#if REGCCNB
    local_pointeur_ClRegion->moy_lum = 
      (unsigned char)(local_pointeur_ClRegion->som_lum 
		      / local_pointeur_ClRegion->surf);
#endif
#if REGCCCOUL
    local_pointeur_ClRegion->moy_lumPl1 = 
      (unsigned char)(local_pointeur_ClRegion->som_lumPl1 
		      / local_pointeur_ClRegion->surf);
    local_pointeur_ClRegion->moy_lumPl2 = 
      (unsigned char)(local_pointeur_ClRegion->som_lumPl2 
		      / local_pointeur_ClRegion->surf);
    local_pointeur_ClRegion->moy_lumPl3 = 
      (unsigned char)(local_pointeur_ClRegion->som_lumPl3 
		      / local_pointeur_ClRegion->surf);
#endif
  }

  return(nbetq);
  
} /* fin de la procedure de compactage */
/* ---------------------------------------------------------- */

/* ----------------------------------------------------------*/
// DecComposanteConnexeDF  -----------------------------------
/** 
 * @brief  	Operateur de Decomposition en Composantes Connexes 
 *
 * Decomposition en composantes connexes (procedure principale)
 * incluant les deux fonctions precedentes selon l'algorithme
 * d'A.Rosenfeld et JL Pflatz
 * 
 * @attention 	Implante en mode Flot de Donnees et releve toutes les
 *  informations necessaires dans une structure ClRegion		
 * 
 *         
 * @param	imsrc : image issue de la classification 
 * @param	imetq : image des etiquettes
 * @param	pnbcc : pointeur sur le nombre de composantes connexes
 * @param	pnetqinit : pointeur sur le nombre de composantes connexes 
 * initiales, c'est a dire avant la fusion
 * @param	netqmax : nombre de composantes maximale, correspond 
 * a la taille du tableau de composantes allouees
 * @param	TabClRegion : Tableau de Structures
 *
 * @return	entier TRUE si Correct, FALSE si netqmax trop petit
 */
// ------------------------------------------------------------------
#if COMPCONNEXE
// ------------------------------------------------------------------
int DecComposanteConnexeDF (IMAGE *imsrc, IMAGE *imetq,
			    int *pnbcc, int *pnetqinit,
			    int netqmax, PT_ClREGION TabClRegion)

{
  unsigned char   *pdtsrc = PIOCTET(imsrc);
  int             *pdtetq = PIETIQ(imetq);
  int             nlig = NLIG(imsrc);
  int             ncol = NCOL(imsrc);
  
  unsigned char   *ptcsrc; /* pointeur sur point courant image source */
  int             *ptcetq; /* pointeur sur point courant image des etiquettes */
  
  unsigned char   *ptvhsrc; /* pointeurs sur les points haut et gauche  */
  unsigned char   *ptvgsrc; /* du voisinage de l'image source */
  
  int             *ptvhetq; /* pointeurs sur les etiquettes des points voisins */
  int             *ptvgetq; /* gauche et haut */
  

  int             n=0, m=0;               /* indices pour balayer l'image */
  int             xpos=0, ypos=0;         /* position du point courant */
  int             etq = 1;                /* etiquette courante  */
  int             nfetq=0;                /* nombre d'etiquettes en final */
  int             anetq=0, nvetq=0;       /* etiquettes anciennes et nouvelles */
  
  int     flagh=0;
  int     flagg=0;

  PT_ClREGION local_pointeur_ClRegion;


  /* --- Premier Point : en haut et a gauche  --- */
  ptcsrc = pdtsrc; /* initialisation des points courants en debut */
  ptcetq = pdtetq; /* d'image */

  /* --- initialisation de la structure de donnees --- */
  local_pointeur_ClRegion=&(TabClRegion[etq]);
  
  local_pointeur_ClRegion->etiqeq = etq;
  local_pointeur_ClRegion->niv = *ptcsrc;
  local_pointeur_ClRegion->som_x = 0;
  local_pointeur_ClRegion->som_y = 0;
  local_pointeur_ClRegion->surf = 1;
  local_pointeur_ClRegion->box.box_xmin = 0;
  local_pointeur_ClRegion->box.box_xmax = 0;
  local_pointeur_ClRegion->box.box_ymin = 0;
  local_pointeur_ClRegion->box.box_ymax = 0;
  
  *ptcetq = etq++;   /* affectation de l'etiquette et incrementation */

  /* --- Etiquetage de la Premiere Ligne --- */
  ptvgsrc = ptcsrc;       /* point voisin de gauche image source */
  ptvgetq = ptcetq;       /* idem image des etiquettes */
  ptcsrc++;               /* second point de la premiere ligne */
  ptcetq++;

  for (n = 0; n < ncol - 1; n++)
  {
    xpos = n + 1;
        
    if(*ptcsrc == *ptvgsrc)
    {
      /* --- Mise a jour de la structure de donnees --- */
      TabClRegion[*ptvgetq].som_x += (long)xpos;
      TabClRegion[*ptvgetq].surf += 1;
      if(xpos > TabClRegion[*ptvgetq].box.box_xmax)
	TabClRegion[*ptvgetq].box.box_xmax = xpos;

      *ptcetq = *ptvgetq;
    } /* fin de mm etiquette que le voisin de gauche */

    else
    {
      /* --- initialisation de la structure de donnees --- */
      if(etq > netqmax)
	return(FALSE);

      local_pointeur_ClRegion=&(TabClRegion[etq]);
      local_pointeur_ClRegion->etiqeq = etq;
      local_pointeur_ClRegion->niv = *ptcsrc;
      local_pointeur_ClRegion->som_x = xpos;
      local_pointeur_ClRegion->som_y = 0;
      local_pointeur_ClRegion->surf = 1;
      local_pointeur_ClRegion->box.box_xmin = xpos;
      local_pointeur_ClRegion->box.box_xmax = xpos;
      local_pointeur_ClRegion->box.box_ymin = 0;
      local_pointeur_ClRegion->box.box_ymax = 0;
      *ptcetq = etq++;   /* affectation de l'etiquette et incrementation */
    }
    ptcsrc++;   /* points courants suivants source et etiquette */
    ptcetq++;
    ptvgsrc++;  /* voisin de gauche images source et etiquette */
    ptvgetq++;
  }

  /* --- Etiquetage de la Premiere Colonne  --- */
  ptcsrc = pdtsrc + ncol; /* initialisation des points courants */
  ptcetq = pdtetq + ncol;
  
  ptvhsrc = pdtsrc; /* initialisation des points voisins de dessus */
  ptvhetq = pdtetq;
  
  for (n = 0; n < nlig - 1; n++)
  {
    ypos = n + 1;

    if(*ptcsrc == *ptvhsrc)
    {
      /* --- Mise a jour de la structure de donnees --- */
      TabClRegion[*ptvhetq].som_y += (long)ypos;
      TabClRegion[*ptvhetq].surf += 1;
      if(ypos > TabClRegion[*ptvhetq].box.box_ymax)
	TabClRegion[*ptvhetq].box.box_ymax = ypos;

      *ptcetq = *ptvhetq;
    } /* fin de mm etiquette que le voisin du dessus */

    else
    {
      /* --- Initialisation de la structure de donnees --- */
      if(etq > netqmax)
	return(FALSE);

      local_pointeur_ClRegion=&(TabClRegion[etq]);
      local_pointeur_ClRegion->etiqeq = etq;
      local_pointeur_ClRegion->niv = *ptcsrc;
      local_pointeur_ClRegion->som_x = 0;
      local_pointeur_ClRegion->som_y = ypos;
      local_pointeur_ClRegion->surf = 1;
      local_pointeur_ClRegion->box.box_xmin = 0;
      local_pointeur_ClRegion->box.box_xmax = 0;
      local_pointeur_ClRegion->box.box_ymin = ypos;
      local_pointeur_ClRegion->box.box_ymax = ypos;
      *ptcetq = etq++;   /* affectation de l'etiquette et incrementation */
    }

    ptcsrc += ncol; /* points courants suivants image source et etiquette */
    ptcetq += ncol;
    ptvhsrc += ncol; /* idem points voisins de dessus */
    ptvhetq += ncol;
  }

  /* --- Etiquetage de l'Interieur de l'Image --- */
  /* --- Initialisation du premier point --- */
  ptcsrc = pdtsrc + ncol + 1; /* points courants */
  ptcetq = pdtetq + ncol + 1;

  ptvhsrc = pdtsrc + 1;        /* points voisins image source */
  ptvgsrc = pdtsrc + ncol;
  
  ptvhetq = pdtetq + 1;        /* idem image des etiquettes */
  ptvgetq = pdtetq + ncol;
  
  for (m = 0; m < nlig - 1; m++) /* boucle externe sur les lignes */
  {
    for (n = 0; n < ncol - 1; n++) /* boucle interne sur les colonnes */
    {
      xpos = n + 1;
      ypos = m + 1;

      
      flagh = FALSE;
      flagg = FALSE;
      
      if(*ptcsrc == *ptvhsrc)
	flagh = TRUE;
      
      if(*ptcsrc == *ptvgsrc)
	flagg = TRUE;

      if((!flagh) && (!flagg))
	/* --- pas de voisin identique : nouvelle etiquette --- */
      {
	/* --- Initialisation de la structure de donnees --- */
	if(etq > netqmax)
	  return(FALSE);

	local_pointeur_ClRegion=&(TabClRegion[etq]);
	local_pointeur_ClRegion->etiqeq = etq;
	local_pointeur_ClRegion->niv = *ptcsrc;
	local_pointeur_ClRegion->som_x = (long)xpos;
	local_pointeur_ClRegion->som_y = (long)ypos;
	local_pointeur_ClRegion->surf = 1;
	local_pointeur_ClRegion->box.box_xmin = xpos;
	local_pointeur_ClRegion->box.box_xmax = xpos;
	local_pointeur_ClRegion->box.box_ymin = ypos;
	local_pointeur_ClRegion->box.box_ymax = ypos;

	/* --- affectation de l'etiquette et incrementation --- */
	*ptcetq = etq++;
      }
      
      else if(!flagg)  /* voisin haut identique */
      {
	
	local_pointeur_ClRegion=&(TabClRegion[*ptvhetq]);
	
	/* --- Mise a jour de la structure de donnees --- */
	local_pointeur_ClRegion->som_x += (long)xpos;
	local_pointeur_ClRegion->som_y += (long)ypos;
	local_pointeur_ClRegion->surf += 1;
	
	if(xpos > local_pointeur_ClRegion->box.box_xmax)
	  local_pointeur_ClRegion->box.box_xmax = xpos;
	
	if(ypos > local_pointeur_ClRegion->box.box_ymax)
	  local_pointeur_ClRegion->box.box_ymax = ypos;
	
	*ptcetq = *ptvhetq;
      } /* fin voisin du haut identique */
      
      else if(!flagh)  /* voisin de gauche identique */
      {
	
	local_pointeur_ClRegion=&(TabClRegion[*ptvgetq]);
	/* --- Mise a jour de la structure de donnees --- */
	local_pointeur_ClRegion->som_x += (long)xpos;
	local_pointeur_ClRegion->som_y += (long)ypos;
	local_pointeur_ClRegion->surf += 1;
	
	if(xpos > local_pointeur_ClRegion->box.box_xmax)
	  local_pointeur_ClRegion->box.box_xmax = xpos;
	
	if(ypos > local_pointeur_ClRegion->box.box_ymax)
	  local_pointeur_ClRegion->box.box_ymax = ypos;
	
	*ptcetq = *ptvgetq;
      } /* fin de voisin de gauche identique */
      
      else if(*ptvgetq == *ptvhetq)
	/* --- deux voisins identiques, mais de meme etiquette --- */
      {
	local_pointeur_ClRegion=&(TabClRegion[*ptvgetq]);
	/* --- Mise a jour de la structure de donnees --- */
	local_pointeur_ClRegion->som_x += (long)xpos;
	local_pointeur_ClRegion->som_y += (long)ypos;
	local_pointeur_ClRegion->surf += 1;
	
	if(xpos > local_pointeur_ClRegion->box.box_xmax)
	  local_pointeur_ClRegion->box.box_xmax = xpos;
	
	if(ypos > local_pointeur_ClRegion->box.box_ymax)
	  local_pointeur_ClRegion->box.box_ymax = ypos;
	
	*ptcetq = *ptvgetq;
      } /* fin de 2 voisins identiques */
      
      else /* Conflit d'etiquetage */
	/* on affecte au pixel courant l'etiquette minimale des deux */
      {
	
	*ptcetq =
	  ((*ptvgetq) < (*ptvhetq) ?
	   (*ptvgetq) : (*ptvhetq));
	
	/* --- Mise a jour des Classes d'equivalence : 
	   on prend celle d'etiquette la plus faible --- */
	nvetq =
	  ((TabClRegion[*ptvgetq].etiqeq < TabClRegion[*ptvhetq].etiqeq) ?
	   TabClRegion[*ptvgetq].etiqeq : TabClRegion[*ptvhetq].etiqeq);
	
	anetq =
	  ((TabClRegion[*ptvgetq].etiqeq > TabClRegion[*ptvhetq].etiqeq) ?
	   TabClRegion[*ptvgetq].etiqeq : TabClRegion[*ptvhetq].etiqeq);
	
	local_pointeur_ClRegion=&(TabClRegion[*ptcetq]);
	
	/* --- Mise a jour de la structure de donnees --- */
	local_pointeur_ClRegion->som_x += (long)xpos;
	local_pointeur_ClRegion->som_y += (long)ypos;
	local_pointeur_ClRegion->surf += 1;
	
	if(xpos > local_pointeur_ClRegion->box.box_xmax)
	  local_pointeur_ClRegion->box.box_xmax = xpos;
	
	if(ypos > local_pointeur_ClRegion->box.box_ymax)
	  local_pointeur_ClRegion->box.box_ymax = ypos;
	
	/* --- la mise a jour du tableau effectue la fusion des deux
	   classes equivalentes d'etiquettes --- */
	
	MiseAJourTablEquivDF(etq, anetq,nvetq, TabClRegion);
      } /* fin conflit d'etiquettes */
      
      /* --- mise a jour des pointeurs --- */
      ptcsrc++;
      ptcetq++;
      ptvgsrc++;
      ptvhsrc++;
      ptvgetq++;
      ptvhetq++;
    } /* fin boucle interne sur les colonnes */
    /* --- mise a jour des pointeurs --- */
    ptcsrc++;
    ptcetq++;
    ptvgsrc++;
    ptvhsrc++;
    ptvgetq++;
    ptvhetq++;
  } /* fin boucle externe sur les lignes */
  
  /* --- Compactage du Tableau des Etiquettes --- */
  *pnetqinit = etq - 1; // etq est l'etiquette de la prochaine Comp Connexe
  nfetq = CompactageDF (etq, TabClRegion) - 1;
  
  *pnbcc = nfetq;
  
  TabClRegion[0].etiqeq = 0;
  
  /* --- Reetiquetage de l'Image des Etiquettes --- */
  // avec le numero des etiquettes compactees
  ptcetq = pdtetq; /* initialisation au premier point */
  
  for (m = 0; m < nlig; m++) /* boucle externe sur les lignes */
    for (n = 0; n < ncol; n++) /* boucle interne sur les colonnes */
    {
      *ptcetq = TabClRegion[*ptcetq].etiqeq;	/* reetiquetage */
      ptcetq++;                    		/* point suivant */
    } /* fin du reetiquetage */


  return(TRUE);
} /* --- fin de l'operateur de decomposition en composantes connexes --- */
/* ------------------------------------------------------------------- */

/* ----------------------------------------------------------*/
// DecComposanteConnexeSaufZeroDF -----------------------------------
/** 
 * @brief  	Operateur de Decomposition en Composantes Connexes sans 
 * 		Etiqueter le Fond
 *		
 * Même chose que DecComposanteConnexeDF, mais ne considere pas  
 * niveau zero (fond a ne pas etiqueter!). Interet : Rapidite 
 *         
 * @param	imsrc : image issue de la classification 
 * @param	imetq : image des etiquettes
 * @param	pnbcc : pointeur sur le nombre de composantes connexes
 * @param	pnetqinit : pointeur sur le nombre de composantes connexes 
 * initiales, c'est a dire avant la fusion
 * @param	netqmax : nombre de composantes maximale, correspond 
 * a la taille du tableau de composantes allouees
 * @param	TabClRegion : Tableau de Structures
 *
 * @return	entier TRUE si Correct, FALSE si netqmax trop petit
 */
// ------------------------------------------------------------------
// ------------------------------------------------------------------
int DecComposanteConnexeSaufZeroDF (IMAGE *imsrc,
				    IMAGE *imetq, int *pnbcc,
				    int *pnetqinit, 
				    int netqmax,
				    PT_ClREGION TabClRegion)

{
  unsigned char   *pdtsrc = PIOCTET(imsrc);
  int             *pdtetq = PIETIQ(imetq);
  int             nlig = NLIG(imsrc);
  int             ncol = NCOL(imsrc);
  
  unsigned char   *ptcsrc; /* pointeur sur point courant image source */
  int             *ptcetq; /* pointeur sur point courant image des etiquettes */
  
  unsigned char   *ptvhsrc; /* pointeurs sur les points haut et gauche  */
  unsigned char   *ptvgsrc; /* du voisinage de l'image source */
  
  int             *ptvhetq; /* pointeurs sur les etiquettes des points voisins */
  int             *ptvgetq; /* gauche et haut */
  

  int             n=0, m=0;               /* indices pour balayer l'image */
  int             xpos=0, ypos=0;         /* position du point courant */
  int             etq = 1;                /* etiquette courante  */
  int             nfetq=0;                /* nombre d'etiquettes en final */
  int             anetq=0, nvetq=0;       /* etiquettes anciennes et nouvelles */
  
  int     flagh=0;
  int     flagg=0;

  PT_ClREGION local_pointeur_ClRegion;


  /* --- Premier Point : en haut et a gauche  --- */
  ptcsrc = pdtsrc; /* initialisation des points courants en debut */
  ptcetq = pdtetq; /* d'image */

  if(*ptcsrc == 0)
    *ptcetq = 0;
  else
  {
    /* --- initialisation de la structure de donnees --- */
    local_pointeur_ClRegion=&(TabClRegion[etq]);
    
    local_pointeur_ClRegion->etiqeq = etq;
    local_pointeur_ClRegion->niv = *ptcsrc;
    local_pointeur_ClRegion->som_x = 0;
    local_pointeur_ClRegion->som_y = 0;
    local_pointeur_ClRegion->surf = 1;
    local_pointeur_ClRegion->box.box_xmin = 0;
    local_pointeur_ClRegion->box.box_xmax = 0;
    local_pointeur_ClRegion->box.box_ymin = 0;
    local_pointeur_ClRegion->box.box_ymax = 0;

    *ptcetq = etq++;   /* affectation de l'etiquette et incrementation */
  } /* fin de si 'appartient pas au fond */

  /* --- Etiquetage de la Premiere Ligne --- */
  ptvgsrc = ptcsrc;       /* point voisin de gauche image source */
  ptvgetq = ptcetq;       /* idem image des etiquettes */
  ptcsrc++;               /* second point de la premiere ligne */
  ptcetq++;

  for (n = 0; n < ncol - 1; n++)
  {
    xpos = n + 1;
    
    if(*ptcsrc == 0)
      *ptcetq = 0;
    
    else if(*ptcsrc == *ptvgsrc)
    {
      /* --- Mise a jour de la structure de donnees --- */
      TabClRegion[*ptvgetq].som_x += (long)xpos;
      TabClRegion[*ptvgetq].surf += 1;
      if(xpos > TabClRegion[*ptvgetq].box.box_xmax)
	TabClRegion[*ptvgetq].box.box_xmax = xpos;

      *ptcetq = *ptvgetq;
    } /* fin de mm etiquette que le voisin de gauche */

    else
    {
      /* --- initialisation de la structure de donnees --- */
      if(etq > netqmax)
	return(FALSE);

      local_pointeur_ClRegion=&(TabClRegion[etq]);
      local_pointeur_ClRegion->etiqeq = etq;
      local_pointeur_ClRegion->niv = *ptcsrc;
      local_pointeur_ClRegion->som_x = xpos;
      local_pointeur_ClRegion->som_y = 0;
      local_pointeur_ClRegion->surf = 1;
      local_pointeur_ClRegion->box.box_xmin = xpos;
      local_pointeur_ClRegion->box.box_xmax = xpos;
      local_pointeur_ClRegion->box.box_ymin = 0;
      local_pointeur_ClRegion->box.box_ymax = 0;
      *ptcetq = etq++;   /* affectation de l'etiquette et incrementation */
    }
    ptcsrc++;   /* points courants suivants source et etiquette */
    ptcetq++;
    ptvgsrc++;  /* voisin de gauche images source et etiquette */
    ptvgetq++;
  }

  /* --- Etiquetage de la Premiere Colonne  --- */
  ptcsrc = pdtsrc + ncol; /* initialisation des points courants */
  ptcetq = pdtetq + ncol;
  
  ptvhsrc = pdtsrc; /* initialisation des points voisins de dessus */
  ptvhetq = pdtetq;
  
  for (n = 0; n < nlig - 1; n++)
  {
    ypos = n + 1;

    if(*ptcsrc == 0)
      *ptcetq = 0;

    else if(*ptcsrc == *ptvhsrc)
    {
      /* --- Mise a jour de la structure de donnees --- */
      TabClRegion[*ptvhetq].som_y += (long)ypos;
      TabClRegion[*ptvhetq].surf += 1;
      if(ypos > TabClRegion[*ptvhetq].box.box_ymax)
	TabClRegion[*ptvhetq].box.box_ymax = ypos;

      *ptcetq = *ptvhetq;
    } /* fin de mm etiquette que le voisin du dessus */

    else
    {
      /* --- Initialisation de la structure de donnees --- */
      if(etq > netqmax)
	return(FALSE);

      local_pointeur_ClRegion=&(TabClRegion[etq]);
      local_pointeur_ClRegion->etiqeq = etq;
      local_pointeur_ClRegion->niv = *ptcsrc;
      local_pointeur_ClRegion->som_x = 0;
      local_pointeur_ClRegion->som_y = ypos;
      local_pointeur_ClRegion->surf = 1;
      local_pointeur_ClRegion->box.box_xmin = 0;
      local_pointeur_ClRegion->box.box_xmax = 0;
      local_pointeur_ClRegion->box.box_ymin = ypos;
      local_pointeur_ClRegion->box.box_ymax = ypos;
      *ptcetq = etq++;   /* affectation de l'etiquette et incrementation */
    }

    ptcsrc += ncol; /* points courants suivants image source et etiquette */
    ptcetq += ncol;
    ptvhsrc += ncol; /* idem points voisins de dessus */
    ptvhetq += ncol;
  }

  /* --- Etiquetage de l'Interieur de l'Image --- */
  /* --- Initialisation du premier point --- */
  ptcsrc = pdtsrc + ncol + 1; /* points courants */
  ptcetq = pdtetq + ncol + 1;

  ptvhsrc = pdtsrc + 1;        /* points voisins image source */
  ptvgsrc = pdtsrc + ncol;
  
  ptvhetq = pdtetq + 1;        /* idem image des etiquettes */
  ptvgetq = pdtetq + ncol;
  
  for (m = 0; m < nlig - 1; m++) /* boucle externe sur les lignes */
  {
    for (n = 0; n < ncol - 1; n++) /* boucle interne sur les colonnes */
    {
      xpos = n + 1;
      ypos = m + 1;

      if(*ptcsrc == 0)
	*ptcetq = 0;
      
      else
      {
	flagh = FALSE;
	flagg = FALSE;

	if(*ptcsrc == *ptvhsrc)
	  flagh = TRUE;

	if(*ptcsrc == *ptvgsrc)
	  flagg = TRUE;

	if((!flagh) && (!flagg))
	  /* --- pas de voisin identique : nouvelle etiquette --- */
	{
	  /* --- Initialisation de la structure de donnees --- */
	  if(etq > netqmax)
	    return(FALSE);

	  
	  local_pointeur_ClRegion=&(TabClRegion[etq]);
	  local_pointeur_ClRegion->etiqeq = etq;
	  local_pointeur_ClRegion->niv = *ptcsrc;
	  local_pointeur_ClRegion->som_x = (long)xpos;
	  local_pointeur_ClRegion->som_y = (long)ypos;
	  local_pointeur_ClRegion->surf = 1;
	  local_pointeur_ClRegion->box.box_xmin = xpos;
	  local_pointeur_ClRegion->box.box_xmax = xpos;
	  local_pointeur_ClRegion->box.box_ymin = ypos;
	  local_pointeur_ClRegion->box.box_ymax = ypos;

	  /* --- affectation de l'etiquette et incrementation --- */
	  *ptcetq = etq++;
	}

	else if(!flagg)  /* voisin haut identique */
	{
	  
	  local_pointeur_ClRegion=&(TabClRegion[*ptvhetq]);

/* --- Mise a jour de la structure de donnees --- */
	  local_pointeur_ClRegion->som_x += (long)xpos;
	  local_pointeur_ClRegion->som_y += (long)ypos;
	  local_pointeur_ClRegion->surf += 1;

	  if(xpos > local_pointeur_ClRegion->box.box_xmax)
	    local_pointeur_ClRegion->box.box_xmax = xpos;

	  if(ypos > local_pointeur_ClRegion->box.box_ymax)
	    local_pointeur_ClRegion->box.box_ymax = ypos;

	  *ptcetq = *ptvhetq;
	} /* fin voisin du haut identique */

	else if(!flagh)  /* voisin de gauche identique */
	{

	  local_pointeur_ClRegion=&(TabClRegion[*ptvgetq]);
	  /* --- Mise a jour de la structure de donnees --- */
	  local_pointeur_ClRegion->som_x += (long)xpos;
	  local_pointeur_ClRegion->som_y += (long)ypos;
	  local_pointeur_ClRegion->surf += 1;

	  if(xpos > local_pointeur_ClRegion->box.box_xmax)
	    local_pointeur_ClRegion->box.box_xmax = xpos;

	  if(ypos > local_pointeur_ClRegion->box.box_ymax)
	    local_pointeur_ClRegion->box.box_ymax = ypos;

	  *ptcetq = *ptvgetq;
	} /* fin de voisin de gauche identique */

	else if(*ptvgetq == *ptvhetq)
	  /* --- deux voisins identiques, mais de meme etiquette --- */
	{
	  local_pointeur_ClRegion=&(TabClRegion[*ptvgetq]);
	  /* --- Mise a jour de la structure de donnees --- */
	  local_pointeur_ClRegion->som_x += (long)xpos;
	  local_pointeur_ClRegion->som_y += (long)ypos;
	  local_pointeur_ClRegion->surf += 1;

	  if(xpos > local_pointeur_ClRegion->box.box_xmax)
	    local_pointeur_ClRegion->box.box_xmax = xpos;
	  
	  if(ypos > local_pointeur_ClRegion->box.box_ymax)
	    local_pointeur_ClRegion->box.box_ymax = ypos;
	  
	  *ptcetq = *ptvgetq;
	} /* fin de 2 voisins identiques */
	
	else /* Conflit d'etiquetage */
	  /* on affecte au pixel courant l'etiquette minimale des deux */
	{
			      
	  *ptcetq =
	    ((*ptvgetq) < (*ptvhetq) ?
	     (*ptvgetq) : (*ptvhetq));

	  /* --- Mise a jour des Classes d'equivalence : 
	     on prend celle d'etiquette la plus faible --- */
	  nvetq =
	    ((TabClRegion[*ptvgetq].etiqeq < TabClRegion[*ptvhetq].etiqeq) ?
	     TabClRegion[*ptvgetq].etiqeq : TabClRegion[*ptvhetq].etiqeq);

	  anetq =
	    ((TabClRegion[*ptvgetq].etiqeq > TabClRegion[*ptvhetq].etiqeq) ?
	     TabClRegion[*ptvgetq].etiqeq : TabClRegion[*ptvhetq].etiqeq);

	  local_pointeur_ClRegion=&(TabClRegion[*ptcetq]);

	  /* --- Mise a jour de la structure de donnees --- */
	  local_pointeur_ClRegion->som_x += (long)xpos;
	  local_pointeur_ClRegion->som_y += (long)ypos;
	  local_pointeur_ClRegion->surf += 1;

	  if(xpos > local_pointeur_ClRegion->box.box_xmax)
	    local_pointeur_ClRegion->box.box_xmax = xpos;
	  
	  if(ypos > local_pointeur_ClRegion->box.box_ymax)
	    local_pointeur_ClRegion->box.box_ymax = ypos;

	  /* --- la mise a jour du tableau effectue la fusion des deux
	     classes equivalentes d'etiquettes --- */

	  MiseAJourTablEquivDF(etq, anetq,nvetq, TabClRegion);
	} /* fin conflit d'etiquettes */
      } /* fin si le point courant n'est pas du fond */
      
      /* --- mise a jour des pointeurs --- */
      ptcsrc++;
      ptcetq++;
      ptvgsrc++;
      ptvhsrc++;
      ptvgetq++;
      ptvhetq++;
    } /* fin boucle interne sur les colonnes */
    /* --- mise a jour des pointeurs --- */
    ptcsrc++;
    ptcetq++;
    ptvgsrc++;
    ptvhsrc++;
    ptvgetq++;
    ptvhetq++;
  } /* fin boucle externe sur les lignes */

  /* --- Compactage du Tableau des Etiquettes --- */
  *pnetqinit = etq - 1; // etq est l'etiquette de la prochaine Comp Connexe
  nfetq = CompactageDF (etq, TabClRegion) - 1;
  
  *pnbcc = nfetq;

  TabClRegion[0].etiqeq = 0;

  /* --- Reetiquetage de l'Image des Etiquettes --- */
  // avec le numero des etiquettes compactees
  ptcetq = pdtetq; /* initialisation au premier point */
  
  for (m = 0; m < nlig; m++) /* boucle externe sur les lignes */
    for (n = 0; n < ncol; n++) /* boucle interne sur les colonnes */
    {
      *ptcetq = TabClRegion[*ptcetq].etiqeq;	/* reetiquetage */
      ptcetq++;                    		/* point suivant */
    } /* fin du reetiquetage */


  return(TRUE);
} /* --- fin de l'operateur de decomposition en composantes connexes --- */
#endif

/* ------------------------------------------------------------------- */

// FiltrageTailleCompConnexe ------------------------------------
/** 
 * @brief   Elimine les Composantes Connexes de taille
 * 	    inferieure a taillemin, avec une taille minimale de
 * 	    la boite englobante en X et en Y
 *
 * @param	imetq : image des etiquettes
 * @param	TabClRegion : Structure des Composantes Connexes
 * @param	pnbcc : pointeur sur le nombre de regions
 * @param	surfmin : Surface minimale de la composante a garder
 * @param	taillemin : Taille minimale en X et Y de la composante 
 * a garder
 *
 * @return None.
 */
// ------------------------------------------------------------------
void FiltrageTailleCompConnexe(IMAGE *imetq, PT_ClREGION TabClRegion, 
			       int *pnbcc, 
			       long surfmin, int taillemin)

// Comprend un double critere de surface minimale et 
// de taille mini sur la boite englobante,
// Effectue un Reettiquetage sans trou, avec mise a jour 
//de l'image des etiquettes
{
  int 	k=0;    /* indice de la composante connexe */
  int 	kn = 1;	/* indice "nouveau" de la composante connexe */
  int   ncol = NCOL(imetq);
  int   *pdtetq = PIETIQ(imetq);
  
  //      pour la mise a jour image des etiquettes
  int    x = 0, y = 0;
  int	 *ptetqc = NULL;
  long	 depl = 0;


  for ( k = 1; k <= *pnbcc; k++)
  {  // composante de surface minimale et boite englobante >= taillemin
    if((TabClRegion[k].surf >= surfmin)
       && ((TabClRegion[k].box.box_xmax - TabClRegion[k].box.box_xmin) 
	   >= taillemin)
       && ((TabClRegion[k].box.box_ymax - TabClRegion[k].box.box_ymin) 
	   >= taillemin))
    {
      /* sauvegarde de la composante connexe */
      TabClRegion[kn] = TabClRegion[k];	
      
      /* --- une composante de plus --- */
      // Mise a jour de l'image des Etiquettes
      depl = (long)(ncol - (TabClRegion[kn].box.box_xmax
			    - TabClRegion[kn].box.box_xmin + 1));
      /* --- initialisation du point courant --- */
      ptetqc = pdtetq + TabClRegion[kn].box.box_ymin * ncol
	+ TabClRegion[kn].box.box_xmin;

      for (y = TabClRegion[kn].box.box_ymin; 
	   y <= TabClRegion[kn].box.box_ymax; y++)
      {
	for (x = TabClRegion[kn].box.box_xmin; 
	     x <= TabClRegion[kn].box.box_xmax; x++)
	{
	  if ((*ptetqc) == k)
	    *ptetqc = kn;

	  ptetqc++;	/* pixel suivant */
	}	/* fin du balayage colonne */
	ptetqc += depl;
      }	/* fin du balayage ligne */
      kn++;
    }	/* fin de si la taille est suffisante */
    else // effacement de la composante connexe
    {
      depl = (long)(ncol - (TabClRegion[k].box.box_xmax
			    - TabClRegion[k].box.box_xmin + 1));
      
      /* --- initialisation du point courant --- */
      ptetqc = pdtetq + TabClRegion[k].box.box_ymin * ncol
	+ TabClRegion[k].box.box_xmin;
      
      for (y = TabClRegion[k].box.box_ymin; 
	   y <= TabClRegion[k].box.box_ymax; y++)
      {
	for (x = TabClRegion[k].box.box_xmin; 
	     x <= TabClRegion[k].box.box_xmax; x++)
	{
	  if ((*ptetqc) == k)
	    *ptetqc = 0; 	// effacement
	  
	  ptetqc++;	/* pixel suivant */
	}	/* fin du balayage colonne */
	ptetqc += depl;
      }	/* fin du balayage ligne */
    } /* fin de l'effacement */
  } /* fin de la boucle sur les composantes */
  *pnbcc = kn - 1;
} /* fin de la procedure de filtrage */

/* ---------------------------------------------------------- */

// FusionCompConnexe ------------------------------------
/** 
 * @brief   Fusionne deux composantes connexes de meme niveau
 *          dont les boites englobantes sont proches.
 *          Attention la composante obtenue n'est plus connexe
 * 	    
 * @param	imetq : image des etiquettes
 * @param	TabClRegion : Structure des Composantes Connexes
 * @param	pnbcc : pointeur sur le nombre de regions
 * @param	dmax : 
 *
 * @return None.
 */
// ------------------------------------------------------------------

void FusionCompConnexe(IMAGE *imetq, PT_ClREGION TabClRegion, 
		       int *pnbcc, int dmax)
		      
// Reetiquettage sans trou, et Mise a jour de l'image des etiquettes
{
  unsigned char niv=0;            /* niveau de seuillage de la couleur */
  int           k=0, l=0, kn = 1; /* indices de composantes connexes */
  int           nbcc=0; 	  /* nombre de composantes connexes */
  //long          tempx=0, tempy=0; /* variable temporaire */
  int           flag = FALSE;

// Mise a jour image des etiquettes
  int           *pdtetq = PIETIQ(imetq);
  int           x = 0, y = 0;
  int           *ptetqc = NULL;
  long          depl = 0;
  int           ncol = NCOL(imetq);
  
  PT_ClREGION   local_pointeur_reg, local_pointeur_reg_sec;
  
  nbcc = *pnbcc; /* nombre d'etiquettes avant fusion */

  /* --- Initialisation du champ : activ --- */
  for (k = 1; k < *pnbcc; k++)
    TabClRegion[k].activ = 1;

  /* --- Fusions Iteratives --- */

  do
  {
    flag = FALSE;

    for (k = 1; k < *pnbcc; k++)
    { /* passage en revue des composantes connexes */
      local_pointeur_reg = &(TabClRegion[k]);

      if(local_pointeur_reg->activ == 1)
      {  	/* si la composante connexe est toujours active */
	niv = local_pointeur_reg->niv;

	for (l = k + 1; l <= *pnbcc; l++)
	{  	/* passage en revue des composantes suivantes */
	  local_pointeur_reg_sec=&(TabClRegion[l]);

	  if( (local_pointeur_reg_sec->activ == 1)
	      && (local_pointeur_reg_sec->niv == niv))
	  { /* composante suivante active et de meme niveau */
	    if(((local_pointeur_reg->box.box_xmin
		 - local_pointeur_reg_sec->box.box_xmax) <= dmax)
	       && ((local_pointeur_reg_sec->box.box_xmin
		    - local_pointeur_reg->box.box_xmax) <= dmax)
	       && ((local_pointeur_reg->box.box_ymin
		    - local_pointeur_reg_sec->box.box_ymax) <= dmax)
	       && ((local_pointeur_reg_sec->box.box_ymin
		    - local_pointeur_reg->box.box_ymax) <= dmax))
	    { /* fusion des deux composantes connexes */
	      flag = TRUE;

	      /* la composante suivante est desactivee */
	      local_pointeur_reg_sec->activ = 0;

	      // k va fusionner l! Mise a jour de l'image des Etiquettes
	      depl = (long)(ncol - (TabClRegion[l].box.box_xmax
				    - TabClRegion[l].box.box_xmin + 1));
	      /* --- initialisation du point courant --- */
	      ptetqc = pdtetq + TabClRegion[l].box.box_ymin * ncol
		+ TabClRegion[l].box.box_xmin;

	      for (y = TabClRegion[l].box.box_ymin;
		   y <= TabClRegion[l].box.box_ymax; y++)
	      {
		for (x = TabClRegion[l].box.box_xmin;
		     x <= TabClRegion[l].box.box_xmax; x++)
		{
		  if ((*ptetqc) == l)
		    *ptetqc = k;
		  
		  ptetqc++;	/* pixel suivant */
		}	/* fin du balayage colonne */
		ptetqc += depl;
	      }	/* fin du balayage ligne */

	      nbcc--; /* une composante de moins */

/* --- Mise a Jour des Parametres de la composante connexe --- */

	      local_pointeur_reg->box.box_xmin =
		((local_pointeur_reg->box.box_xmin < 
		  local_pointeur_reg_sec->box.box_xmin) ?
		 local_pointeur_reg->box.box_xmin : 
		 local_pointeur_reg_sec->box.box_xmin);

	      local_pointeur_reg->box.box_xmax =
		((local_pointeur_reg->box.box_xmax > 
		  local_pointeur_reg_sec->box.box_xmax) ?
		 local_pointeur_reg->box.box_xmax : 
		 local_pointeur_reg_sec->box.box_xmax);

	      local_pointeur_reg->box.box_ymin =
		((local_pointeur_reg->box.box_ymin < 
		  local_pointeur_reg_sec->box.box_ymin) ?
		 local_pointeur_reg->box.box_ymin : 
		 local_pointeur_reg_sec->box.box_ymin);

	      local_pointeur_reg->box.box_ymax =
		((local_pointeur_reg->box.box_ymax > 
		  local_pointeur_reg_sec->box.box_ymax) ?
		 local_pointeur_reg->box.box_ymax : 
		 local_pointeur_reg_sec->box.box_ymax);

	      /* --- Elements Cumulatifs --- */

	      local_pointeur_reg->som_x += local_pointeur_reg_sec->som_x;
	      local_pointeur_reg->som_y += local_pointeur_reg_sec->som_y;

	      local_pointeur_reg->surf += local_pointeur_reg_sec->surf; 

	      local_pointeur_reg->ptg.x = 
		(int)(local_pointeur_reg->som_x / local_pointeur_reg->surf);

	      local_pointeur_reg->ptg.y = 
		(int)(local_pointeur_reg->som_y / local_pointeur_reg->surf);
	     
	    } /* fin de la fusion des composantes connexes */
	  } /* fin de la composante suivante active et de meme niveau */
	} /* fin de la boucle sur les composantes suivantes */
      } /* fin de si la composante connexe est active */
    } /* fin de la boucle sur les composantes connexes */
  } while (flag); /* propagation des fusions */

/* --- Compactage de la Structure de Composantes Connexes encore Actives --- */
  for ( k = 1; k <= *pnbcc; k++)
  {
    if(TabClRegion[k].activ == 1)
    {
      TabClRegion[kn] = TabClRegion[k];	/* sauvegarde de la composante connexe */
// un second reetiquetage est necessaire
// Mise a jour de l'image des Etiquettes : k devient kn!

      depl = (long)(ncol - (TabClRegion[k].box.box_xmax
			    - TabClRegion[k].box.box_xmin + 1));
/* --- initialisation du point courant --- */
      ptetqc = pdtetq + TabClRegion[k].box.box_ymin * ncol
	+ TabClRegion[k].box.box_xmin;

      for (y = TabClRegion[k].box.box_ymin;
	   y <= TabClRegion[k].box.box_ymax; y++)
      {
	for (x = TabClRegion[k].box.box_xmin;
	     x <= TabClRegion[k].box.box_xmax; x++)
	{
	  if ((*ptetqc) == k)
	    *ptetqc = kn;

	  ptetqc++;	/* pixel suivant */
	}	/* fin du balayage colonne */
	ptetqc += depl;
      }	/* fin du balayage ligne */
      kn++;
    } /* fin si composante active */
  } /* fin de la boucle sur les composantes */
  *pnbcc = nbcc; /* nombre de composantes connexes en final */


} /* fin de la procedure de fusion */


/* ------------------------------------------------------------ */
/* ---                Utilitaires de Test                   --- */
/* ------------------------------------------------------------ */

// EnregistreComposantesConnexes   ----------------------------
/** 
 * @brief  	Enregistrement dans un fichier les composantes et 
 * 			de leurs parametres.
 *		
 * @param	fp : fichier pour l'enregistrement
 * @param	TabClRegion : Structure de Donnees ClREGION
 * @param	nbccf : nombre de ClRegion
 *
 * @return None.
 */
// ------------------------------------------------------------------
void EnregistreComposantesConnexes(FILE *fp,PT_ClREGION TabClRegion, 
				   int nbccf)
{
  int nocc;
  for (nocc = 1; nocc <= nbccf; nocc++)
  {
    fprintf(fp,"\n \tComposante Connexe n° : %d : Surface : %d\n",
	    (int)nocc, (int)TabClRegion[nocc].surf);
    fprintf (fp,"Centre de Gravite : Xg = %d : Yg = %d \n",
	     (int)TabClRegion[nocc].ptg.x, (int)TabClRegion[nocc].ptg.y);
    fprintf (fp,"Xmin = %d : Xmax = %d : Ymin = %d : Ymax = %d\n",
	     (int)TabClRegion[nocc].box.box_xmin,
	     (int)TabClRegion[nocc].box.box_xmax,
	     (int)TabClRegion[nocc].box.box_ymin,
	     (int)TabClRegion[nocc].box.box_ymax);

#if COMPCONNEXE
    fprintf(fp,"Niveau : %d\n",(int)TabClRegion[nocc].niv);
#endif

#if REGCCNB
    fprintf(fp,"Luminance Moyenne : %d\n",(int)TabClRegion[nocc].moy_lum);
#endif
  } /* fin de la boucle sur les composantes connexes */
} /* fin de l'enregistrement des composantes connexes */

/* -------------------------------------------------------------- */
/* Trace du Centre de Gravite et de la Boite englobante           */
/* -------------------------------------------------------------- */
// -------------------------------------------------------------
/** 
 * @brief  	Trace du Centre de Gravite et de la Boite englobante
 * Pour la boite englobante, le trace n'est effectue que sur les
 * pixels de fond . Ceci permet notamment de visualiser les points 
 * de contact de la boite englobante et de la composante connexe.
 * Cette procedure n'est pas realisee en flot de donnee, car elle
 * n'est qu'un utilitaire de debug!
 * 
 * @param	imcetq : image couleur des etiquettes
 * @param	TabClRegion : Structure des Donnees
 * @param	nbcc : nombre de composantes connexes
 *
 * @return None.
 */
// ------------------------------------------------------------------
void TraceComposantesConnexes (IMAGE *imcetq, PT_ClREGION TabClRegion, 
			       int nbcc)
{
  int nocc;
  POINT *point;

  if(crea_POINT(point) == NULL)
  {
    fprintf(stderr,"Erreur d'Allocation Memoire point : \n");
    exit (0);
  }

  for (nocc = 1; nocc <= nbcc; nocc++)
  {

    /* --- Trace de la Boite Englobante --- */
    for (POINT_X(point) = TabClRegion[nocc].box.box_xmin;
	 POINT_X(point) <= TabClRegion[nocc].box.box_xmax; POINT_X(point)++)
    {
      POINT_Y(point) = TabClRegion[nocc].box.box_ymin;
      
      if((POINT_X(point) < 0)|| (POINT_X(point) >= NCOL(imcetq))
	 ||(POINT_Y(point) < 0)|| (POINT_Y(point) >= NLIG(imcetq)))
      {
	fprintf(stderr,"Erreur de Depassement Boite Englobante\n");
	exit(0);
      }

      // Pour visualiser si la Boite englobante touche bien la CC!!! 

      if ((PIXEL_R(imcetq, point) == 0) && (PIXEL_V(imcetq, point) == 0)
	  && (PIXEL_B(imcetq, point) == 0))
      {
	PIXEL_R(imcetq, point) = 255;
	PIXEL_V(imcetq, point) = 255;
	PIXEL_B(imcetq, point) = 255;
      }

      POINT_Y(point) = TabClRegion[nocc].box.box_ymax;

      if((POINT_X(point) < 0)|| (POINT_X(point) >= NCOL(imcetq))
	 ||(POINT_Y(point) < 0)|| (POINT_Y(point) >= NLIG(imcetq)))
      {
	fprintf(stderr,"Erreur de Depassement Boite Englobante \n");
	exit(0);
      }

      // Pour visualiser si la Boite englobante touche bien la CC!!! 

      if ((PIXEL_R(imcetq, point) == 0) && (PIXEL_V(imcetq, point) == 0)
	  && (PIXEL_B(imcetq, point) == 0))
      {
	PIXEL_R(imcetq, point) = 255;
	PIXEL_V(imcetq, point) = 255;
	PIXEL_B(imcetq, point) = 255;
      }
    }

    for (POINT_Y(point) = TabClRegion[nocc].box.box_ymin;
	 POINT_Y(point) <= TabClRegion[nocc].box.box_ymax; POINT_Y(point)++)
    {

      POINT_X(point) = TabClRegion[nocc].box.box_xmin;
      
      if((POINT_X(point) < 0)|| (POINT_X(point) >= NCOL(imcetq))
	 ||(POINT_Y(point) < 0)|| (POINT_Y(point) >= NLIG(imcetq)))
      {
	fprintf(stderr,"Erreur de Depassement Boite Englobante \n");
	exit(0);
      }

      // Pour visualiser si la Boite englobante touche bien la CC!!! 

      if ((PIXEL_R(imcetq, point) == 0) && (PIXEL_V(imcetq, point) == 0)
	  && (PIXEL_B(imcetq, point) == 0))
      {
	PIXEL_R(imcetq, point) = 255;
	PIXEL_V(imcetq, point) = 255;
	PIXEL_B(imcetq, point) = 255;
      }
      
      POINT_X(point) = TabClRegion[nocc].box.box_xmax;
      
      if((POINT_X(point) < 0)|| (POINT_X(point) >= NCOL(imcetq))
	 ||(POINT_Y(point) < 0)|| (POINT_Y(point) >= NLIG(imcetq)))
      {
	fprintf(stderr,"Erreur de Depassement Boite Englobante \n");
	exit(0);
      }

// Pour visualiser si la Boite englobante touche bien la CC!!! 

      if ((PIXEL_R(imcetq, point) == 0) && (PIXEL_V(imcetq, point) == 0)
	  && (PIXEL_B(imcetq, point) == 0))
      {
	PIXEL_R(imcetq, point) = 255;
	PIXEL_V(imcetq, point) = 255;
	PIXEL_B(imcetq, point) = 255;
      }

    }

    /* --- Trace du Centre de Gravite --- */
    POINT_X(point) = TabClRegion[nocc].ptg.x;
    POINT_Y(point) = TabClRegion[nocc].ptg.y;

    if((POINT_X(point) < 0)|| (POINT_X(point) >= NCOL(imcetq))
       ||(POINT_Y(point) < 0)|| (POINT_Y(point) >= NLIG(imcetq)))
    {
      fprintf(stderr,"Erreur de Depassement Centre de Gravite \n");
      exit(0);
    }

    PIXEL_R(imcetq, point) = 255;
    PIXEL_V(imcetq, point) = 255;
    PIXEL_B(imcetq, point) = 255;

    
  } /* fin du balayage du tableau de PCC */

  free((void *)point);
}

/* ------------------------------------------------------------------ */
// Croissance de Region a partir de l'Algorithme de A.Rosenfeld & JL Pflaz
/* ----------------------------------------------------------*/
// RegionNBFromComposanteConnexeDF -----------------------------------
/** 
 * @brief  	Operateur de Croissance de Regions a partir de l'Algorithme
 * de Decomposition en Composantes Connexes de A.Rosenfeld et JL.Pfaltz
 * en imagerie N&B (niveaux de gris)
 * 
 * @attention 	Implante en mode Flot de Donnees et releve toutes les
 *  informations necessaires dans une structure ClRegion		
 * 
 *         
 * @param	imsrc : image en niveau de gris a segmenter en regions
 * @param	imetq : image des etiquettes
 * @param	pnbcc : pointeur sur le nombre de composantes connexes
 * @param	pnetqinit : pointeur sur le nombre de composantes connexes 
 * initiales, c'est a dire avant la fusion
 * @param	netqmax : nombre de composantes maximale, correspond 
 * a la taille du tableau de composantes allouees
 * @param	TabClRegion : Tableau de Structures
 * @param       difmax : valeur maxi de la difference
 * @return	entier TRUE si Correct, FALSE si netqmax trop petit
 */
// ------------------------------------------------------------------
#if REGCCNB
// ------------------------------------------------------------------
int RegionNBFromComposanteConnexeDF (IMAGE *imsrc,
				     IMAGE *imetq, int *pnbcc,
				     int *pnetqinit, 
				     int netqmax,
				     PT_ClREGION TabClRegion,
				     int difmax)

{
  unsigned char   *pdtsrc = PIOCTET(imsrc);
  int             *pdtetq = PIETIQ(imetq);
  int             nlig = NLIG(imsrc);
  int             ncol = NCOL(imsrc);
  
  unsigned char   *ptcsrc; /* pointeur sur point courant image source */
  int             *ptcetq; /* pointeur sur point courant image des etiquettes */
  
  unsigned char   *ptvhsrc; /* pointeurs sur les points haut et gauche  */
  unsigned char   *ptvgsrc; /* du voisinage de l'image source */
  
  int             *ptvhetq; /* pointeurs sur les etiquettes des points voisins */
  int             *ptvgetq; /* gauche et haut */
  

  int             n=0, m=0;               /* indices pour balayer l'image */
  int             xpos=0, ypos=0;         /* position du point courant */
  int             etq = 1;                /* etiquette courante  */
  int             nfetq=0;                /* nombre d'etiquettes en final */
  int             anetq=0, nvetq=0;       /* etiquettes anciennes et nouvelles */
  
  int     flagh=0;
  int     flagg=0;
  int     dif_lum = 0; /* difference de luminance entre pixels voisins */

  PT_ClREGION local_pointeur_ClRegion;


  /* --- Premier Point : en haut et a gauche  --- */
  ptcsrc = pdtsrc; /* initialisation des points courants en debut */
  ptcetq = pdtetq; /* d'image */

  /* --- initialisation de la structure de donnees --- */
  local_pointeur_ClRegion=&(TabClRegion[etq]);
  
  local_pointeur_ClRegion->etiqeq = etq;
  local_pointeur_ClRegion->som_x = 0;
  local_pointeur_ClRegion->som_y = 0;
  local_pointeur_ClRegion->surf = 1;
  local_pointeur_ClRegion->som_lum = (*ptcsrc);
  local_pointeur_ClRegion->box.box_xmin = 0;
  local_pointeur_ClRegion->box.box_xmax = 0;
  local_pointeur_ClRegion->box.box_ymin = 0;
  local_pointeur_ClRegion->box.box_ymax = 0;

  
  *ptcetq = etq++;   /* affectation de l'etiquette et incrementation */

  /* --- Etiquetage de la Premiere Ligne --- */
  ptvgsrc = ptcsrc;       /* point voisin de gauche image source */
  ptvgetq = ptcetq;       /* idem image des etiquettes */
  ptcsrc++;               /* second point de la premiere ligne */
  ptcetq++;

  for (n = 0; n < ncol - 1; n++)
  {
    xpos = n + 1;

    dif_lum = (int)(*ptcsrc) - (int)(*ptvgsrc);
    dif_lum = ((dif_lum >= 0) ? dif_lum : -dif_lum);

    if(dif_lum <= difmax)
    {
      /* --- Mise a jour de la structure de donnees --- */
      TabClRegion[*ptvgetq].som_x += (long)xpos;
      TabClRegion[*ptvgetq].som_lum += (long)(*ptcsrc);

      TabClRegion[*ptvgetq].surf += 1;
      if(xpos > TabClRegion[*ptvgetq].box.box_xmax)
	TabClRegion[*ptvgetq].box.box_xmax = xpos;

      *ptcetq = *ptvgetq;
    } /* fin de mm etiquette que le voisin de gauche */

    else
    {
      /* --- initialisation de la structure de donnees --- */
      if(etq > netqmax)
	return(FALSE);

      local_pointeur_ClRegion=&(TabClRegion[etq]);
      local_pointeur_ClRegion->etiqeq = etq;
      local_pointeur_ClRegion->som_lum = *ptcsrc;
      local_pointeur_ClRegion->som_x = xpos;
      local_pointeur_ClRegion->som_y = 0;
      local_pointeur_ClRegion->surf = 1;
      local_pointeur_ClRegion->box.box_xmin = xpos;
      local_pointeur_ClRegion->box.box_xmax = xpos;
      local_pointeur_ClRegion->box.box_ymin = 0;
      local_pointeur_ClRegion->box.box_ymax = 0;
      *ptcetq = etq++;   /* affectation de l'etiquette et incrementation */
    }
    ptcsrc++;   /* points courants suivants source et etiquette */
    ptcetq++;
    ptvgsrc++;  /* voisin de gauche images source et etiquette */
    ptvgetq++;
  }

  /* --- Etiquetage de la Premiere Colonne  --- */
  ptcsrc = pdtsrc + ncol; /* initialisation des points courants */
  ptcetq = pdtetq + ncol;
  
  ptvhsrc = pdtsrc; /* initialisation des points voisins de dessus */
  ptvhetq = pdtetq;
  
  for (n = 0; n < nlig - 1; n++)
  {
    ypos = n + 1;
    
    dif_lum = (int)(*ptcsrc) - (int)(*ptvhsrc);
    dif_lum = ((dif_lum >= 0) ? dif_lum : -dif_lum);

    if(dif_lum <= difmax)
    {
      /* --- Mise a jour de la structure de donnees --- */
      TabClRegion[*ptvhetq].som_y += (long)ypos;
      TabClRegion[*ptvhetq].som_lum += (long)(*ptcsrc);
      TabClRegion[*ptvhetq].surf += 1;
      if(ypos > TabClRegion[*ptvhetq].box.box_ymax)
	TabClRegion[*ptvhetq].box.box_ymax = ypos;

      *ptcetq = *ptvhetq;
    } /* fin de mm etiquette que le voisin du dessus */

    else
    {
      /* --- Initialisation de la structure de donnees --- */
      if(etq > netqmax)
	return(FALSE);

      local_pointeur_ClRegion=&(TabClRegion[etq]);
      local_pointeur_ClRegion->etiqeq = etq;
      local_pointeur_ClRegion->som_lum = *ptcsrc;
      local_pointeur_ClRegion->som_x = 0;
      local_pointeur_ClRegion->som_y = ypos;
      local_pointeur_ClRegion->surf = 1;
      local_pointeur_ClRegion->box.box_xmin = 0;
      local_pointeur_ClRegion->box.box_xmax = 0;
      local_pointeur_ClRegion->box.box_ymin = ypos;
      local_pointeur_ClRegion->box.box_ymax = ypos;
      *ptcetq = etq++;   /* affectation de l'etiquette et incrementation */
    }

    ptcsrc += ncol; /* points courants suivants image source et etiquette */
    ptcetq += ncol;
    ptvhsrc += ncol; /* idem points voisins de dessus */
    ptvhetq += ncol;
  }

  /* --- Etiquetage de l'Interieur de l'Image --- */
  /* --- Initialisation du premier point --- */
  ptcsrc = pdtsrc + ncol + 1; /* points courants */
  ptcetq = pdtetq + ncol + 1;

  ptvhsrc = pdtsrc + 1;        /* points voisins image source */
  ptvgsrc = pdtsrc + ncol;
  
  ptvhetq = pdtetq + 1;        /* idem image des etiquettes */
  ptvgetq = pdtetq + ncol;
  
  for (m = 0; m < nlig - 1; m++) /* boucle externe sur les lignes */
  {
    for (n = 0; n < ncol - 1; n++) /* boucle interne sur les colonnes */
    {
      xpos = n + 1;
      ypos = m + 1;

      
      flagh = FALSE;
      flagg = FALSE;

      dif_lum = (int)(*ptcsrc) - (int)(*ptvhsrc);
      dif_lum = ((dif_lum >= 0) ? dif_lum : -dif_lum);
      
      if(dif_lum <= difmax)
	flagh = TRUE;
      
      dif_lum = (int)(*ptcsrc) - (int)(*ptvgsrc);
      dif_lum = ((dif_lum >= 0) ? dif_lum : -dif_lum);
      
      if(dif_lum <= difmax)
	flagg = TRUE;

      if((!flagh) && (!flagg))
	/* --- pas de voisin identique : nouvelle etiquette --- */
      {
	/* --- Initialisation de la structure de donnees --- */
	if(etq > netqmax)
	  return(FALSE);

	local_pointeur_ClRegion=&(TabClRegion[etq]);
	local_pointeur_ClRegion->etiqeq = etq;
	local_pointeur_ClRegion->som_lum = *ptcsrc;
	local_pointeur_ClRegion->som_x = (long)xpos;
	local_pointeur_ClRegion->som_y = (long)ypos;
	local_pointeur_ClRegion->surf = 1;
	local_pointeur_ClRegion->box.box_xmin = xpos;
	local_pointeur_ClRegion->box.box_xmax = xpos;
	local_pointeur_ClRegion->box.box_ymin = ypos;
	local_pointeur_ClRegion->box.box_ymax = ypos;

	/* --- affectation de l'etiquette et incrementation --- */
	*ptcetq = etq++;
      }
      
      else if(!flagg)  /* voisin haut identique */
      {
	
	local_pointeur_ClRegion=&(TabClRegion[*ptvhetq]);
	
	/* --- Mise a jour de la structure de donnees --- */
	local_pointeur_ClRegion->som_x += (long)xpos;
	local_pointeur_ClRegion->som_y += (long)ypos;
	local_pointeur_ClRegion->som_lum += (long)(*ptcsrc);
	local_pointeur_ClRegion->surf += 1;
	
	if(xpos > local_pointeur_ClRegion->box.box_xmax)
	  local_pointeur_ClRegion->box.box_xmax = xpos;
	
	if(ypos > local_pointeur_ClRegion->box.box_ymax)
	  local_pointeur_ClRegion->box.box_ymax = ypos;
	
	*ptcetq = *ptvhetq;
      } /* fin voisin du haut identique */
      
      else if(!flagh)  /* voisin de gauche identique */
      {
	
	local_pointeur_ClRegion=&(TabClRegion[*ptvgetq]);
	/* --- Mise a jour de la structure de donnees --- */
	local_pointeur_ClRegion->som_x += (long)xpos;
	local_pointeur_ClRegion->som_y += (long)ypos;
	local_pointeur_ClRegion->som_lum += (long)(*ptcsrc);
	local_pointeur_ClRegion->surf += 1;
	
	if(xpos > local_pointeur_ClRegion->box.box_xmax)
	  local_pointeur_ClRegion->box.box_xmax = xpos;
	
	if(ypos > local_pointeur_ClRegion->box.box_ymax)
	  local_pointeur_ClRegion->box.box_ymax = ypos;
	
	*ptcetq = *ptvgetq;
      } /* fin de voisin de gauche identique */
      
      else if(*ptvgetq == *ptvhetq)
	/* --- deux voisins identiques, mais de meme etiquette --- */
      {
	local_pointeur_ClRegion=&(TabClRegion[*ptvgetq]);
	/* --- Mise a jour de la structure de donnees --- */
	local_pointeur_ClRegion->som_x += (long)xpos;
	local_pointeur_ClRegion->som_y += (long)ypos;
	local_pointeur_ClRegion->som_lum += (long)(*ptcsrc);
	local_pointeur_ClRegion->surf += 1;
	
	if(xpos > local_pointeur_ClRegion->box.box_xmax)
	  local_pointeur_ClRegion->box.box_xmax = xpos;
	
	if(ypos > local_pointeur_ClRegion->box.box_ymax)
	  local_pointeur_ClRegion->box.box_ymax = ypos;
	
	*ptcetq = *ptvgetq;
      } /* fin de 2 voisins identiques */
      
      else /* Conflit d'etiquetage */
	/* on affecte au pixel courant l'etiquette minimale des deux */
      {
	
	*ptcetq =
	  ((*ptvgetq) < (*ptvhetq) ?
	   (*ptvgetq) : (*ptvhetq));
	
	/* --- Mise a jour des Classes d'equivalence : 
	   on prend celle d'etiquette la plus faible --- */
	nvetq =
	  ((TabClRegion[*ptvgetq].etiqeq < TabClRegion[*ptvhetq].etiqeq) ?
	   TabClRegion[*ptvgetq].etiqeq : TabClRegion[*ptvhetq].etiqeq);
	
	anetq =
	  ((TabClRegion[*ptvgetq].etiqeq > TabClRegion[*ptvhetq].etiqeq) ?
	   TabClRegion[*ptvgetq].etiqeq : TabClRegion[*ptvhetq].etiqeq);
	
	local_pointeur_ClRegion=&(TabClRegion[*ptcetq]);
	
	/* --- Mise a jour de la structure de donnees --- */
	local_pointeur_ClRegion->som_x += (long)xpos;
	local_pointeur_ClRegion->som_y += (long)ypos;
	local_pointeur_ClRegion->som_lum += (long)(*ptcsrc);
	local_pointeur_ClRegion->surf += 1;
	
	if(xpos > local_pointeur_ClRegion->box.box_xmax)
	  local_pointeur_ClRegion->box.box_xmax = xpos;
	
	if(ypos > local_pointeur_ClRegion->box.box_ymax)
	  local_pointeur_ClRegion->box.box_ymax = ypos;
	
	/* --- la mise a jour du tableau effectue la fusion des deux
	   classes equivalentes d'etiquettes --- */
	
	MiseAJourTablEquivDF(etq, anetq,nvetq, TabClRegion);
      } /* fin conflit d'etiquettes */
      
      /* --- mise a jour des pointeurs --- */
      ptcsrc++;
      ptcetq++;
      ptvgsrc++;
      ptvhsrc++;
      ptvgetq++;
      ptvhetq++;
    } /* fin boucle interne sur les colonnes */
    /* --- mise a jour des pointeurs --- */
    ptcsrc++;
    ptcetq++;
    ptvgsrc++;
    ptvhsrc++;
    ptvgetq++;
    ptvhetq++;
  } /* fin boucle externe sur les lignes */
  
  /* --- Compactage du Tableau des Etiquettes --- */
  *pnetqinit = etq - 1; // etq est l'etiquette de la prochaine Comp Connexe
  nfetq = CompactageDF (etq, TabClRegion) - 1;
  
  *pnbcc = nfetq;
  
  TabClRegion[0].etiqeq = 0;
  
  /* --- Reetiquetage de l'Image des Etiquettes --- */
  // avec le numero des etiquettes compactees
  ptcetq = pdtetq; /* initialisation au premier point */
  
  for (m = 0; m < nlig; m++) /* boucle externe sur les lignes */
    for (n = 0; n < ncol; n++) /* boucle interne sur les colonnes */
    {
      *ptcetq = TabClRegion[*ptcetq].etiqeq;	/* reetiquetage */
      ptcetq++;                    		/* point suivant */
    } /* fin du reetiquetage */


  return(TRUE);
} /* --- fin de l'operateur Croissance de Regions en N&B --- */
#endif
/* ------------------------------------------------------------------- */

#if REGCCCOUL
/* ----------------------------------------------------------*/
// RegionCoulDEFromComposanteConnexeDF -----------------------------------
/** 
 * @brief  	Operateur de Croissance de Regions a partir de l'Algorithme
 * de Decomposition en Composantes Connexes de A.Rosenfeld et JL.Pfaltz
 * en Imagerie Couleur a donnees entrelacees
 *
 * @attention 	Implante en mode Flot de Donnees et releve toutes les
 *  informations necessaires dans une structure ClRegion		
 * 
 *         
 * @param	imsrc : image couleur a segmenter en regions 
 * @param	imetq : image des etiquettes
 * @param	pnbcc : pointeur sur le nombre de composantes connexes
 * @param	pnetqinit : pointeur sur le nombre de composantes connexes 
 * initiales, c'est a dire avant la fusion
 * @param	netqmax : nombre de composantes maximale, correspond 
 * a la taille du tableau de composantes allouees
 * @param	TabClRegion : Tableau de Structures
 * @param       difmax1 : valeur maxi de la difference pour le plan 1
 * @param       difmax2 : idem plans 2 et 3
 * param        difmax3
 * @return	entier TRUE si Correct, FALSE si netqmax trop petit
 */
// ------------------------------------------------------------------

// ------------------------------------------------------------------
int RegionCoulDEFromComposanteConnexeDF (IMAGE *imsrc,
				     IMAGE *imetq, int *pnbcc,
				     int *pnetqinit, 
				     int netqmax,
				     PT_ClREGION TabClRegion,
				     int difmax1, int difmax2, int difmax3)

{
  unsigned char   *pdtsrc = imsrc->ptdata.poctet;
  int             *pdtetq = PIETIQ(imetq);
  int             nlig = NLIG(imsrc);
  int             ncol = NCOL(imsrc);
  
  unsigned char   *ptcsrc1; /* pointeur sur point courant image source plan1 */
  unsigned char   *ptcsrc2; /* idem plans 2 et 3 */
  unsigned char   *ptcsrc3;

  int             *ptcetq; /* pointeur sur point courant image des etiquettes */
  
  unsigned char   *ptvhsrc1; /* pointeurs sur les points haut et gauche  */
  unsigned char   *ptvgsrc1; /* du voisinage de l'image source plan 1 */
  unsigned char   *ptvhsrc2; /* pointeurs sur les points haut et gauche  */
  unsigned char   *ptvgsrc2; /* du voisinage de l'image source plan 2 */
  unsigned char   *ptvhsrc3; /* pointeurs sur les points haut et gauche  */
  unsigned char   *ptvgsrc3; /* du voisinage de l'image source plan 3 */
  
  int             *ptvhetq; /* pointeurs sur les etiquettes des points voisins */
  int             *ptvgetq; /* gauche et haut */
  

  int             n=0, m=0;               /* indices pour balayer l'image */
  int             xpos=0, ypos=0;         /* position du point courant */
  int             etq = 1;                /* etiquette courante  */
  int             nfetq=0;                /* nombre d'etiquettes en final */
  int             anetq=0, nvetq=0;       /* etiquettes anciennes et nouvelles */
  
  int     flagh=0;
  int     flagg=0;
  int     dif_lum1 = 0; /* difference de luminance entre pixels voisins plan1 */
  int     dif_lum2 = 0; /* idem plans 2 et 3 */
  int     dif_lum3 = 0;

  PT_ClREGION local_pointeur_ClRegion;

  /* --- Premier Point : en haut et a gauche  --- */
  ptcsrc1 = pdtsrc;       /* initialisation des points courants en debut */
  ptcsrc2 = ptcsrc1 + 1;  /* d'image */
  ptcsrc3 = ptcsrc1 + 2;
  
  ptcetq = pdtetq; 

  /* --- initialisation de la structure de donnees --- */
  local_pointeur_ClRegion=&(TabClRegion[etq]);
  
  local_pointeur_ClRegion->etiqeq = etq;
  local_pointeur_ClRegion->som_x = 0;
  local_pointeur_ClRegion->som_y = 0;
  local_pointeur_ClRegion->surf = 1;
  local_pointeur_ClRegion->som_lumPl1 = (*ptcsrc1);
  local_pointeur_ClRegion->som_lumPl2 = (*ptcsrc2);
  local_pointeur_ClRegion->som_lumPl3 = (*ptcsrc3);
  local_pointeur_ClRegion->box.box_xmin = 0;
  local_pointeur_ClRegion->box.box_xmax = 0;
  local_pointeur_ClRegion->box.box_ymin = 0;
  local_pointeur_ClRegion->box.box_ymax = 0;

  
  *ptcetq = etq++;   /* affectation de l'etiquette et incrementation */

  /* --- Etiquetage de la Premiere Ligne --- */
  ptvgsrc1 = ptcsrc1;       /* point voisin de gauche image source */
  ptvgsrc2 = ptcsrc2;       /* idem image des etiquettes */
  ptvgsrc3 = ptcsrc3;       /* second point de la premiere ligne */
  ptvgetq = ptcetq;       
  ptcsrc1 += 3;
  ptcsrc2 += 3;
  ptcsrc3 += 3;
  ptcetq++;

  for (n = 0; n < ncol - 1; n++)
  {
    xpos = n + 1;

    dif_lum1 = (int)(*ptcsrc1) - (int)(*ptvgsrc1);
    dif_lum1 = ((dif_lum1 >= 0) ? dif_lum1 : -dif_lum1);

    dif_lum2 = (int)(*ptcsrc2) - (int)(*ptvgsrc2);
    dif_lum2 = ((dif_lum2 >= 0) ? dif_lum2 : -dif_lum2);

    dif_lum3 = (int)(*ptcsrc3) - (int)(*ptvgsrc3);
    dif_lum3 = ((dif_lum3 >= 0) ? dif_lum3 : -dif_lum3);

    if((dif_lum1 <= difmax1) && (dif_lum2 <= difmax2) 
       && (dif_lum3 <= difmax3))
    {
      /* --- Mise a jour de la structure de donnees --- */
      TabClRegion[*ptvgetq].som_x += (long)xpos;
      TabClRegion[*ptvgetq].som_lumPl1 += (long)(*ptcsrc1);
      TabClRegion[*ptvgetq].som_lumPl2 += (long)(*ptcsrc2);
      TabClRegion[*ptvgetq].som_lumPl3 += (long)(*ptcsrc3);

      TabClRegion[*ptvgetq].surf += 1;
      if(xpos > TabClRegion[*ptvgetq].box.box_xmax)
	TabClRegion[*ptvgetq].box.box_xmax = xpos;

      *ptcetq = *ptvgetq;
    } /* fin de mm etiquette que le voisin de gauche */

    else
    {
      /* --- initialisation de la structure de donnees --- */
      if(etq > netqmax)
	return(FALSE);

      local_pointeur_ClRegion=&(TabClRegion[etq]);
      local_pointeur_ClRegion->etiqeq = etq;
      local_pointeur_ClRegion->som_lumPl1 = *ptcsrc1;
      local_pointeur_ClRegion->som_lumPl2 = *ptcsrc2;
      local_pointeur_ClRegion->som_lumPl3 = *ptcsrc3;
      local_pointeur_ClRegion->som_x = xpos;
      local_pointeur_ClRegion->som_y = 0;
      local_pointeur_ClRegion->surf = 1;
      local_pointeur_ClRegion->box.box_xmin = xpos;
      local_pointeur_ClRegion->box.box_xmax = xpos;
      local_pointeur_ClRegion->box.box_ymin = 0;
      local_pointeur_ClRegion->box.box_ymax = 0;
      *ptcetq = etq++;   /* affectation de l'etiquette et incrementation */
    }
    ptcsrc1 += 3;   /* points courants suivants source et etiquette */
    ptcsrc2 += 3;
    ptcsrc3 += 3;
    ptcetq++;
    ptvgsrc1 += 3;  /* voisin de gauche images source et etiquette */
    ptvgsrc2 += 3;
    ptvgsrc3 += 3;
    ptvgetq++;
  }

  /* --- Etiquetage de la Premiere Colonne  --- */

  ptcsrc1 = pdtsrc + 3 * ncol; /* initialisation des points courants */
  ptcsrc2 = ptcsrc1 + 1;
  ptcsrc3 = ptcsrc1 + 2;
  ptcetq = pdtetq + ncol;
  
  ptvhsrc1 = pdtsrc; /* initialisation des points voisins de dessus */
  ptvhsrc2 = ptvhsrc1 + 1;
  ptvhsrc3 = ptvhsrc1 + 2;
  ptvhetq = pdtetq;
  
  for (n = 0; n < nlig - 1; n++)
  {
    ypos = n + 1;

    dif_lum1 = (int)(*ptcsrc1) - (int)(*ptvhsrc1);
    dif_lum1 = ((dif_lum1 >= 0) ? dif_lum1 : -dif_lum1);

    dif_lum2 = (int)(*ptcsrc2) - (int)(*ptvhsrc2);
    dif_lum2 = ((dif_lum2 >= 0) ? dif_lum2 : -dif_lum2);

    dif_lum3 = (int)(*ptcsrc3) - (int)(*ptvhsrc3);
    dif_lum3 = ((dif_lum3 >= 0) ? dif_lum3 : -dif_lum3);

    if((dif_lum1 <= difmax1) && (dif_lum2 <= difmax2) 
       && (dif_lum3 <= difmax3))
    {
      /* --- Mise a jour de la structure de donnees --- */
      TabClRegion[*ptvhetq].som_y += (long)ypos;
      TabClRegion[*ptvhetq].som_lumPl1 += (long)(*ptcsrc1);
      TabClRegion[*ptvhetq].som_lumPl2 += (long)(*ptcsrc2);
      TabClRegion[*ptvhetq].som_lumPl3 += (long)(*ptcsrc3);
      TabClRegion[*ptvhetq].surf += 1;
      if(ypos > TabClRegion[*ptvhetq].box.box_ymax)
	TabClRegion[*ptvhetq].box.box_ymax = ypos;

      *ptcetq = *ptvhetq;
    } /* fin de mm etiquette que le voisin du dessus */

    else
    {
      /* --- Initialisation de la structure de donnees --- */
      if(etq > netqmax)
	return(FALSE);

      local_pointeur_ClRegion=&(TabClRegion[etq]);
      local_pointeur_ClRegion->etiqeq = etq;
      local_pointeur_ClRegion->som_lumPl1 = *ptcsrc1;
      local_pointeur_ClRegion->som_lumPl2 = *ptcsrc2;
      local_pointeur_ClRegion->som_lumPl3 = *ptcsrc3;
      local_pointeur_ClRegion->som_x = 0;
      local_pointeur_ClRegion->som_y = ypos;
      local_pointeur_ClRegion->surf = 1;
      local_pointeur_ClRegion->box.box_xmin = 0;
      local_pointeur_ClRegion->box.box_xmax = 0;
      local_pointeur_ClRegion->box.box_ymin = ypos;
      local_pointeur_ClRegion->box.box_ymax = ypos;
      *ptcetq = etq++;   /* affectation de l'etiquette et incrementation */
    }

    ptcsrc1 += (3 * ncol); /* points courants suivants image source et etiquette */
    ptcsrc2 += (3 * ncol); 
    ptcsrc3 += (3 * ncol); 
    ptcetq += ncol;
    ptvhsrc1 += (3* ncol); /* idem points voisins de dessus */
    ptvhsrc2 += (3* ncol);
    ptvhsrc3 += (3* ncol);
    ptvhetq += ncol;
  }

  /* --- Etiquetage de l'Interieur de l'Image --- */
  /* --- Initialisation du premier point --- */
  ptcsrc1 = pdtsrc + 3 * (ncol + 1); /* points courants */
  ptcsrc2 = ptcsrc1 + 1;
  ptcsrc3 = ptcsrc1 + 2;

  ptcetq = pdtetq + ncol + 1;

  ptvhsrc1 = pdtsrc + 3;        /* points voisins image source */
  ptvhsrc2 = ptvhsrc1 + 1;
  ptvhsrc3 = ptvhsrc1 + 2;

  ptvgsrc1 = pdtsrc + 3 * ncol;
  ptvgsrc2 = ptvgsrc1 + 1;
  ptvgsrc3 = ptvgsrc1 + 2; 
  
  ptvhetq = pdtetq + 1;        /* idem image des etiquettes */
  ptvgetq = pdtetq + ncol;
  
  for (m = 0; m < nlig - 1; m++) /* boucle externe sur les lignes */
  {
    for (n = 0; n < ncol - 1; n++) /* boucle interne sur les colonnes */
    {
      xpos = n + 1;
      ypos = m + 1;

      
      flagh = FALSE;
      flagg = FALSE;

      dif_lum1 = (int)(*ptcsrc1) - (int)(*ptvhsrc1);
      dif_lum1 = ((dif_lum1 >= 0) ? dif_lum1 : -dif_lum1);

      dif_lum2 = (int)(*ptcsrc2) - (int)(*ptvhsrc2);
      dif_lum2 = ((dif_lum2 >= 0) ? dif_lum2 : -dif_lum2);

      dif_lum3 = (int)(*ptcsrc3) - (int)(*ptvhsrc3);
      dif_lum3 = ((dif_lum3 >= 0) ? dif_lum3 : -dif_lum3);

      if((dif_lum1 <= difmax1) && (dif_lum2 <= difmax2) 
	 && (dif_lum3 <= difmax3))
	flagh = TRUE;

      dif_lum1 = (int)(*ptcsrc1) - (int)(*ptvgsrc1);
      dif_lum1 = ((dif_lum1 >= 0) ? dif_lum1 : -dif_lum1);

      dif_lum2 = (int)(*ptcsrc2) - (int)(*ptvgsrc2);
      dif_lum2 = ((dif_lum2 >= 0) ? dif_lum2 : -dif_lum2);

      dif_lum3 = (int)(*ptcsrc3) - (int)(*ptvgsrc3);
      dif_lum3 = ((dif_lum3 >= 0) ? dif_lum3 : -dif_lum3);

      if((dif_lum1 <= difmax1) && (dif_lum2 <= difmax2) 
	 && (dif_lum3 <= difmax3))
	flagg = TRUE;

      if((!flagh) && (!flagg))
	/* --- pas de voisin identique : nouvelle etiquette --- */
      {
	/* --- Initialisation de la structure de donnees --- */
	if(etq > netqmax)
	  return(FALSE);

	local_pointeur_ClRegion=&(TabClRegion[etq]);
	local_pointeur_ClRegion->etiqeq = etq;
	local_pointeur_ClRegion->som_lumPl1 = *ptcsrc1;
	local_pointeur_ClRegion->som_lumPl2 = *ptcsrc2;
	local_pointeur_ClRegion->som_lumPl3 = *ptcsrc3;
	local_pointeur_ClRegion->som_x = (long)xpos;
	local_pointeur_ClRegion->som_y = (long)ypos;
	local_pointeur_ClRegion->surf = 1;
	local_pointeur_ClRegion->box.box_xmin = xpos;
	local_pointeur_ClRegion->box.box_xmax = xpos;
	local_pointeur_ClRegion->box.box_ymin = ypos;
	local_pointeur_ClRegion->box.box_ymax = ypos;

	/* --- affectation de l'etiquette et incrementation --- */
	*ptcetq = etq++;
      }
      
      else if(!flagg)  /* voisin haut identique */
      {
	
	local_pointeur_ClRegion=&(TabClRegion[*ptvhetq]);
	
	/* --- Mise a jour de la structure de donnees --- */
	local_pointeur_ClRegion->som_x += (long)xpos;
	local_pointeur_ClRegion->som_y += (long)ypos;
	local_pointeur_ClRegion->som_lumPl1 += (long)(*ptcsrc1);
	local_pointeur_ClRegion->som_lumPl2 += (long)(*ptcsrc2);
	local_pointeur_ClRegion->som_lumPl3 += (long)(*ptcsrc3);
	local_pointeur_ClRegion->surf += 1;
	
	if(xpos > local_pointeur_ClRegion->box.box_xmax)
	  local_pointeur_ClRegion->box.box_xmax = xpos;
	
	if(ypos > local_pointeur_ClRegion->box.box_ymax)
	  local_pointeur_ClRegion->box.box_ymax = ypos;
	
	*ptcetq = *ptvhetq;
      } /* fin voisin du haut identique */
      
      else if(!flagh)  /* voisin de gauche identique */
      {
	
	local_pointeur_ClRegion=&(TabClRegion[*ptvgetq]);
	/* --- Mise a jour de la structure de donnees --- */
	local_pointeur_ClRegion->som_x += (long)xpos;
	local_pointeur_ClRegion->som_y += (long)ypos;
	local_pointeur_ClRegion->som_lumPl1 += (long)(*ptcsrc1);
	local_pointeur_ClRegion->som_lumPl2 += (long)(*ptcsrc2);
	local_pointeur_ClRegion->som_lumPl3 += (long)(*ptcsrc3);
	local_pointeur_ClRegion->surf += 1;
	
	if(xpos > local_pointeur_ClRegion->box.box_xmax)
	  local_pointeur_ClRegion->box.box_xmax = xpos;
	
	if(ypos > local_pointeur_ClRegion->box.box_ymax)
	  local_pointeur_ClRegion->box.box_ymax = ypos;
	
	*ptcetq = *ptvgetq;
      } /* fin de voisin de gauche identique */
      
      else if(*ptvgetq == *ptvhetq)
	/* --- deux voisins identiques, mais de meme etiquette --- */
      {
	local_pointeur_ClRegion=&(TabClRegion[*ptvgetq]);
	/* --- Mise a jour de la structure de donnees --- */
	local_pointeur_ClRegion->som_x += (long)xpos;
	local_pointeur_ClRegion->som_y += (long)ypos;
	local_pointeur_ClRegion->som_lumPl1 += (long)(*ptcsrc1);
	local_pointeur_ClRegion->som_lumPl2 += (long)(*ptcsrc2);
	local_pointeur_ClRegion->som_lumPl3 += (long)(*ptcsrc3);
	local_pointeur_ClRegion->surf += 1;
	
	if(xpos > local_pointeur_ClRegion->box.box_xmax)
	  local_pointeur_ClRegion->box.box_xmax = xpos;
	
	if(ypos > local_pointeur_ClRegion->box.box_ymax)
	  local_pointeur_ClRegion->box.box_ymax = ypos;
	
	*ptcetq = *ptvgetq;
      } /* fin de 2 voisins identiques */
      
      else /* Conflit d'etiquetage */
	/* on affecte au pixel courant l'etiquette minimale des deux */
      {
	
	*ptcetq =
	  ((*ptvgetq) < (*ptvhetq) ?
	   (*ptvgetq) : (*ptvhetq));
	
	/* --- Mise a jour des Classes d'equivalence : 
	   on prend celle d'etiquette la plus faible --- */
	nvetq =
	  ((TabClRegion[*ptvgetq].etiqeq < TabClRegion[*ptvhetq].etiqeq) ?
	   TabClRegion[*ptvgetq].etiqeq : TabClRegion[*ptvhetq].etiqeq);
	
	anetq =
	  ((TabClRegion[*ptvgetq].etiqeq > TabClRegion[*ptvhetq].etiqeq) ?
	   TabClRegion[*ptvgetq].etiqeq : TabClRegion[*ptvhetq].etiqeq);
	
	local_pointeur_ClRegion=&(TabClRegion[*ptcetq]);
	
	/* --- Mise a jour de la structure de donnees --- */
	local_pointeur_ClRegion->som_x += (long)xpos;
	local_pointeur_ClRegion->som_y += (long)ypos;
	local_pointeur_ClRegion->som_lumPl1 += (long)(*ptcsrc1);
	local_pointeur_ClRegion->som_lumPl2 += (long)(*ptcsrc2);
	local_pointeur_ClRegion->som_lumPl3 += (long)(*ptcsrc3);
	local_pointeur_ClRegion->surf += 1;
	
	if(xpos > local_pointeur_ClRegion->box.box_xmax)
	  local_pointeur_ClRegion->box.box_xmax = xpos;
	
	if(ypos > local_pointeur_ClRegion->box.box_ymax)
	  local_pointeur_ClRegion->box.box_ymax = ypos;
	
	/* --- la mise a jour du tableau effectue la fusion des deux
	   classes equivalentes d'etiquettes --- */
	
	MiseAJourTablEquivDF(etq, anetq,nvetq, TabClRegion);
      } /* fin conflit d'etiquettes */
      
      /* --- mise a jour des pointeurs --- */
      ptcsrc1 += 3;
      ptcsrc2 += 3;
      ptcsrc3 += 3;
      ptcetq++;
      ptvgsrc1 += 3;
      ptvgsrc2 += 3;
      ptvgsrc3 += 3;
      ptvhsrc1 += 3;
      ptvhsrc2 += 3;
      ptvhsrc3 += 3;
      ptvgetq++;
      ptvhetq++;
    } /* fin boucle interne sur les colonnes */
    /* --- mise a jour des pointeurs --- */
    ptcsrc1 += 3;
    ptcsrc2 += 3;
    ptcsrc3 += 3;
    ptcetq++;
    ptvgsrc1 += 3;
    ptvgsrc2 += 3;
    ptvgsrc3 += 3;
    ptvhsrc1 += 3;
    ptvhsrc2 += 3;
    ptvhsrc3 += 3;
    ptvgetq++;
    ptvhetq++;
  } /* fin boucle externe sur les lignes */
  
  /* --- Compactage du Tableau des Etiquettes --- */
  *pnetqinit = etq - 1; // etq est l'etiquette de la prochaine Comp Connexe
  nfetq = CompactageDF (etq, TabClRegion) - 1;
  
  *pnbcc = nfetq;
  
  TabClRegion[0].etiqeq = 0;
  
  /* --- Reetiquetage de l'Image des Etiquettes --- */
  // avec le numero des etiquettes compactees
  ptcetq = pdtetq; /* initialisation au premier point */
  
  for (m = 0; m < nlig; m++) /* boucle externe sur les lignes */
    for (n = 0; n < ncol; n++) /* boucle interne sur les colonnes */
    {
      *ptcetq = TabClRegion[*ptcetq].etiqeq;	/* reetiquetage */
      ptcetq++;                    		/* point suivant */
    } /* fin du reetiquetage */


  return(TRUE);
} /* --- fin de l'operateur de Croissance de Regions en Couleur --- */

/* ------------------------------------------------------------------  */

/* ----------------------------------------------------------*/
// RegionCoulDSFromComposanteConnexeDF -----------------------------------
/** 
 * @brief  	Operateur de Croissance de Regions a partir de l'Algorithme
 * de Decomposition en Composantes Connexes de A.Rosenfeld et JL.Pfaltz
 * en Imagerie Couleur a donnees separees
 *
 * @attention 	Implante en mode Flot de Donnees et releve toutes les
 *  informations necessaires dans une structure ClRegion		
 * 
 *         
 * @param	imsrc1 : Plan 1 de l'image couleur a segmenter en regions
 * @param       imsrc2 : idem plans 2 et 3
 * @param       imsrc3  
 * @param	imetq : image des etiquettes
 * @param	pnbcc : pointeur sur le nombre de composantes connexes
 * @param	pnetqinit : pointeur sur le nombre de composantes connexes 
 * initiales, c'est a dire avant la fusion
 * @param	netqmax : nombre de composantes maximale, correspond 
 * a la taille du tableau de composantes allouees
 * @param	TabClRegion : Tableau de Structures
 * @param       difmax1 : valeur maxi de la difference pour le plan 1
 * @param       difmax2 : idem plans 2 et 3
 * param        difmax3
 * @return	entier TRUE si Correct, FALSE si netqmax trop petit
 */
// ------------------------------------------------------------------

// ------------------------------------------------------------------
int RegionCoulDSFromComposanteConnexeDF (IMAGE *imsrc1,IMAGE *imsrc2, 
					 IMAGE *imsrc3,IMAGE *imetq, 
					 int *pnbcc,
					 int *pnetqinit, 
					 int netqmax,
					 PT_ClREGION TabClRegion,
					 int difmax1, int difmax2, int difmax3)

{
  unsigned char   *pdtsrc1 = imsrc1->ptdata.poctet;
  unsigned char   *pdtsrc2 = imsrc2->ptdata.poctet;
  unsigned char   *pdtsrc3 = imsrc3->ptdata.poctet;
  
  int             *pdtetq = PIETIQ(imetq);
  int             nlig = NLIG(imsrc1);
  int             ncol = NCOL(imsrc1);
  
  unsigned char   *ptcsrc1; /* pointeur sur point courant image source plan1 */
  unsigned char   *ptcsrc2; /* idem plans 2 et 3 */
  unsigned char   *ptcsrc3;

  int             *ptcetq; /* pointeur sur point courant image des etiquettes */
  
  unsigned char   *ptvhsrc1; /* pointeurs sur les points haut et gauche  */
  unsigned char   *ptvgsrc1; /* du voisinage de l'image source plan 1 */
  unsigned char   *ptvhsrc2; /* pointeurs sur les points haut et gauche  */
  unsigned char   *ptvgsrc2; /* du voisinage de l'image source plan 2 */
  unsigned char   *ptvhsrc3; /* pointeurs sur les points haut et gauche  */
  unsigned char   *ptvgsrc3; /* du voisinage de l'image source plan 3 */
  
  int             *ptvhetq; /* pointeurs sur les etiquettes des points voisins */
  int             *ptvgetq; /* gauche et haut */
  

  int             n=0, m=0;               /* indices pour balayer l'image */
  int             xpos=0, ypos=0;         /* position du point courant */
  int             etq = 1;                /* etiquette courante  */
  int             nfetq=0;                /* nombre d'etiquettes en final */
  int             anetq=0, nvetq=0;       /* etiquettes anciennes et nouvelles */
  
  int     flagh=0;
  int     flagg=0;
  int     dif_lum1 = 0; /* difference de luminance entre pixels voisins plan1 */
  int     dif_lum2 = 0; /* idem plans 2 et 3 */
  int     dif_lum3 = 0;

  PT_ClREGION local_pointeur_ClRegion;


  /* --- Premier Point : en haut et a gauche  --- */
  ptcsrc1 = pdtsrc1;       /* initialisation des points courants en debut */
  ptcsrc2 = pdtsrc2;  /* d'image */
  ptcsrc3 = pdtsrc3;
  
  ptcetq = pdtetq; 

  /* --- initialisation de la structure de donnees --- */
  local_pointeur_ClRegion=&(TabClRegion[etq]);
  
  local_pointeur_ClRegion->etiqeq = etq;
  local_pointeur_ClRegion->som_x = 0;
  local_pointeur_ClRegion->som_y = 0;
  local_pointeur_ClRegion->surf = 1;
  local_pointeur_ClRegion->som_lumPl1 = (*ptcsrc1);
  local_pointeur_ClRegion->som_lumPl2 = (*ptcsrc2);
  local_pointeur_ClRegion->som_lumPl3 = (*ptcsrc3);
  local_pointeur_ClRegion->box.box_xmin = 0;
  local_pointeur_ClRegion->box.box_xmax = 0;
  local_pointeur_ClRegion->box.box_ymin = 0;
  local_pointeur_ClRegion->box.box_ymax = 0;

  
  *ptcetq = etq++;   /* affectation de l'etiquette et incrementation */

  /* --- Etiquetage de la Premiere Ligne --- */
  ptvgsrc1 = ptcsrc1;       /* point voisin de gauche image source */
  ptvgsrc2 = ptcsrc2;       /* idem image des etiquettes */
  ptvgsrc3 = ptcsrc3;       /* second point de la premiere ligne */
  ptvgetq = ptcetq;       
  ptcsrc1++;
  ptcsrc2++;
  ptcsrc3++;
  ptcetq++;

  for (n = 0; n < ncol - 1; n++)
  {
    xpos = n + 1;

    dif_lum1 = (int)(*ptcsrc1) - (int)(*ptvgsrc1);
    dif_lum1 = ((dif_lum1 >= 0) ? dif_lum1 : -dif_lum1);

    dif_lum2 = (int)(*ptcsrc2) - (int)(*ptvgsrc2);
    dif_lum2 = ((dif_lum2 >= 0) ? dif_lum2 : -dif_lum2);

    dif_lum3 = (int)(*ptcsrc3) - (int)(*ptvgsrc3);
    dif_lum3 = ((dif_lum3 >= 0) ? dif_lum3 : -dif_lum3);

    if((dif_lum1 <= difmax1) && (dif_lum2 <= difmax2) 
       && (dif_lum3 <= difmax3))
    {
      /* --- Mise a jour de la structure de donnees --- */
      TabClRegion[*ptvgetq].som_x += (long)xpos;
      TabClRegion[*ptvgetq].som_lumPl1 += (long)(*ptcsrc1);
      TabClRegion[*ptvgetq].som_lumPl2 += (long)(*ptcsrc2);
      TabClRegion[*ptvgetq].som_lumPl3 += (long)(*ptcsrc3);

      TabClRegion[*ptvgetq].surf += 1;
      if(xpos > TabClRegion[*ptvgetq].box.box_xmax)
	TabClRegion[*ptvgetq].box.box_xmax = xpos;

      *ptcetq = *ptvgetq;
    } /* fin de mm etiquette que le voisin de gauche */

    else
    {
      /* --- initialisation de la structure de donnees --- */
      if(etq > netqmax)
	return(FALSE);

      local_pointeur_ClRegion=&(TabClRegion[etq]);
      local_pointeur_ClRegion->etiqeq = etq;
      local_pointeur_ClRegion->som_lumPl1 = *ptcsrc1;
      local_pointeur_ClRegion->som_lumPl2 = *ptcsrc2;
      local_pointeur_ClRegion->som_lumPl3 = *ptcsrc3;
      local_pointeur_ClRegion->som_x = xpos;
      local_pointeur_ClRegion->som_y = 0;
      local_pointeur_ClRegion->surf = 1;
      local_pointeur_ClRegion->box.box_xmin = xpos;
      local_pointeur_ClRegion->box.box_xmax = xpos;
      local_pointeur_ClRegion->box.box_ymin = 0;
      local_pointeur_ClRegion->box.box_ymax = 0;
      *ptcetq = etq++;   /* affectation de l'etiquette et incrementation */
    }
    ptcsrc1++;   /* points courants suivants source et etiquette */
    ptcsrc2++;
    ptcsrc3++;
    ptcetq++;
    ptvgsrc1++;  /* voisin de gauche images source et etiquette */
    ptvgsrc2++;
    ptvgsrc3++;
    ptvgetq++;
  }

  /* --- Etiquetage de la Premiere Colonne  --- */

  ptcsrc1 = pdtsrc1 + ncol; /* initialisation des points courants */
  ptcsrc2 = pdtsrc2 + ncol;
  ptcsrc3 = pdtsrc3 + ncol;
  ptcetq = pdtetq + ncol;
  
  ptvhsrc1 = pdtsrc1; /* initialisation des points voisins de dessus */
  ptvhsrc2 = pdtsrc2;
  ptvhsrc3 = pdtsrc3;
  ptvhetq = pdtetq;
  
  for (n = 0; n < nlig - 1; n++)
  {
    ypos = n + 1;

    dif_lum1 = (int)(*ptcsrc1) - (int)(*ptvhsrc1);
    dif_lum1 = ((dif_lum1 >= 0) ? dif_lum1 : -dif_lum1);

    dif_lum2 = (int)(*ptcsrc2) - (int)(*ptvhsrc2);
    dif_lum2 = ((dif_lum2 >= 0) ? dif_lum2 : -dif_lum2);

    dif_lum3 = (int)(*ptcsrc3) - (int)(*ptvhsrc3);
    dif_lum3 = ((dif_lum3 >= 0) ? dif_lum3 : -dif_lum3);

    if((dif_lum1 <= difmax1) && (dif_lum2 <= difmax2) 
       && (dif_lum3 <= difmax3))
    {
      /* --- Mise a jour de la structure de donnees --- */
      TabClRegion[*ptvhetq].som_y += (long)ypos;
      TabClRegion[*ptvhetq].som_lumPl1 += (long)(*ptcsrc1);
      TabClRegion[*ptvhetq].som_lumPl2 += (long)(*ptcsrc2);
      TabClRegion[*ptvhetq].som_lumPl3 += (long)(*ptcsrc3);
      TabClRegion[*ptvhetq].surf += 1;
      if(ypos > TabClRegion[*ptvhetq].box.box_ymax)
	TabClRegion[*ptvhetq].box.box_ymax = ypos;

      *ptcetq = *ptvhetq;
    } /* fin de mm etiquette que le voisin du dessus */

    else
    {
      /* --- Initialisation de la structure de donnees --- */
      if(etq > netqmax)
	return(FALSE);

      local_pointeur_ClRegion=&(TabClRegion[etq]);
      local_pointeur_ClRegion->etiqeq = etq;
      local_pointeur_ClRegion->som_lumPl1 = *ptcsrc1;
      local_pointeur_ClRegion->som_lumPl2 = *ptcsrc2;
      local_pointeur_ClRegion->som_lumPl3 = *ptcsrc3;
      local_pointeur_ClRegion->som_x = 0;
      local_pointeur_ClRegion->som_y = ypos;
      local_pointeur_ClRegion->surf = 1;
      local_pointeur_ClRegion->box.box_xmin = 0;
      local_pointeur_ClRegion->box.box_xmax = 0;
      local_pointeur_ClRegion->box.box_ymin = ypos;
      local_pointeur_ClRegion->box.box_ymax = ypos;
      *ptcetq = etq++;   /* affectation de l'etiquette et incrementation */
    }

    ptcsrc1 += ncol; /* points courants suivants image source et etiquette */
    ptcsrc2 += ncol; 
    ptcsrc3 += ncol; 
    ptcetq += ncol;
    ptvhsrc1 += ncol; /* idem points voisins de dessus */
    ptvhsrc2 += ncol;
    ptvhsrc3 += ncol;
    ptvhetq += ncol;
  }


  /* --- Etiquetage de l'Interieur de l'Image --- */
  /* --- Initialisation du premier point --- */
  ptcsrc1 = pdtsrc1 + ncol + 1; /* points courants */
  ptcsrc2 = pdtsrc2 + ncol + 1; 
  ptcsrc3 = pdtsrc3 + ncol + 1;

  ptcetq = pdtetq + ncol + 1;

  ptvhsrc1 = pdtsrc1 + 1;        /* points voisins image source */
  ptvhsrc2 = pdtsrc2 + 1;
  ptvhsrc3 = pdtsrc3 + 1;

  ptvgsrc1 = pdtsrc1 + ncol;
  ptvgsrc2 = pdtsrc2 + ncol;
  ptvgsrc3 = pdtsrc3 + ncol; 
  
  ptvhetq = pdtetq + 1;        /* idem image des etiquettes */
  ptvgetq = pdtetq + ncol;
  
  for (m = 0; m < nlig - 1; m++) /* boucle externe sur les lignes */
  {
    for (n = 0; n < ncol - 1; n++) /* boucle interne sur les colonnes */
    {
      xpos = n + 1;
      ypos = m + 1;

      flagh = FALSE;
      flagg = FALSE;

      dif_lum1 = (int)(*ptcsrc1) - (int)(*ptvhsrc1);
      dif_lum1 = ((dif_lum1 >= 0) ? dif_lum1 : -dif_lum1);

      dif_lum2 = (int)(*ptcsrc2) - (int)(*ptvhsrc2);
      dif_lum2 = ((dif_lum2 >= 0) ? dif_lum2 : -dif_lum2);

      dif_lum3 = (int)(*ptcsrc3) - (int)(*ptvhsrc3);
      dif_lum3 = ((dif_lum3 >= 0) ? dif_lum3 : -dif_lum3);

      if((dif_lum1 <= difmax1) && (dif_lum2 <= difmax2) 
	 && (dif_lum3 <= difmax3))
	flagh = TRUE;

      dif_lum1 = (int)(*ptcsrc1) - (int)(*ptvgsrc1);
      dif_lum1 = ((dif_lum1 >= 0) ? dif_lum1 : -dif_lum1);

      dif_lum2 = (int)(*ptcsrc2) - (int)(*ptvgsrc2);
      dif_lum2 = ((dif_lum2 >= 0) ? dif_lum2 : -dif_lum2);

      dif_lum3 = (int)(*ptcsrc3) - (int)(*ptvgsrc3);
      dif_lum3 = ((dif_lum3 >= 0) ? dif_lum3 : -dif_lum3);

      if((dif_lum1 <= difmax1) && (dif_lum2 <= difmax2) 
	 && (dif_lum3 <= difmax3))
	flagg = TRUE;

      if((!flagh) && (!flagg))
	/* --- pas de voisin identique : nouvelle etiquette --- */
      {
	/* --- Initialisation de la structure de donnees --- */
	if(etq > netqmax)
	  return(FALSE);

	local_pointeur_ClRegion=&(TabClRegion[etq]);
	local_pointeur_ClRegion->etiqeq = etq;
	local_pointeur_ClRegion->som_lumPl1 = *ptcsrc1;
	local_pointeur_ClRegion->som_lumPl2 = *ptcsrc2;
	local_pointeur_ClRegion->som_lumPl3 = *ptcsrc3;
	local_pointeur_ClRegion->som_x = (long)xpos;
	local_pointeur_ClRegion->som_y = (long)ypos;
	local_pointeur_ClRegion->surf = 1;
	local_pointeur_ClRegion->box.box_xmin = xpos;
	local_pointeur_ClRegion->box.box_xmax = xpos;
	local_pointeur_ClRegion->box.box_ymin = ypos;
	local_pointeur_ClRegion->box.box_ymax = ypos;

	/* --- affectation de l'etiquette et incrementation --- */
	*ptcetq = etq++;
      }
      
      else if(!flagg)  /* voisin haut identique */
      {
	
	local_pointeur_ClRegion=&(TabClRegion[*ptvhetq]);
	
	/* --- Mise a jour de la structure de donnees --- */
	local_pointeur_ClRegion->som_x += (long)xpos;
	local_pointeur_ClRegion->som_y += (long)ypos;
	local_pointeur_ClRegion->som_lumPl1 += (long)(*ptcsrc1);
	local_pointeur_ClRegion->som_lumPl2 += (long)(*ptcsrc2);
	local_pointeur_ClRegion->som_lumPl3 += (long)(*ptcsrc3);
	local_pointeur_ClRegion->surf += 1;
	
	if(xpos > local_pointeur_ClRegion->box.box_xmax)
	  local_pointeur_ClRegion->box.box_xmax = xpos;
	
	if(ypos > local_pointeur_ClRegion->box.box_ymax)
	  local_pointeur_ClRegion->box.box_ymax = ypos;
	
	*ptcetq = *ptvhetq;
      } /* fin voisin du haut identique */
      
      else if(!flagh)  /* voisin de gauche identique */
      {
	
	local_pointeur_ClRegion=&(TabClRegion[*ptvgetq]);
	/* --- Mise a jour de la structure de donnees --- */
	local_pointeur_ClRegion->som_x += (long)xpos;
	local_pointeur_ClRegion->som_y += (long)ypos;
	local_pointeur_ClRegion->som_lumPl1 += (long)(*ptcsrc1);
	local_pointeur_ClRegion->som_lumPl2 += (long)(*ptcsrc2);
	local_pointeur_ClRegion->som_lumPl3 += (long)(*ptcsrc3);
	local_pointeur_ClRegion->surf += 1;
	
	if(xpos > local_pointeur_ClRegion->box.box_xmax)
	  local_pointeur_ClRegion->box.box_xmax = xpos;
	
	if(ypos > local_pointeur_ClRegion->box.box_ymax)
	  local_pointeur_ClRegion->box.box_ymax = ypos;
	
	*ptcetq = *ptvgetq;
      } /* fin de voisin de gauche identique */
      
      else if(*ptvgetq == *ptvhetq)
	/* --- deux voisins identiques, mais de meme etiquette --- */
      {
	local_pointeur_ClRegion=&(TabClRegion[*ptvgetq]);
	/* --- Mise a jour de la structure de donnees --- */
	local_pointeur_ClRegion->som_x += (long)xpos;
	local_pointeur_ClRegion->som_y += (long)ypos;
	local_pointeur_ClRegion->som_lumPl1 += (long)(*ptcsrc1);
	local_pointeur_ClRegion->som_lumPl2 += (long)(*ptcsrc2);
	local_pointeur_ClRegion->som_lumPl3 += (long)(*ptcsrc3);
	local_pointeur_ClRegion->surf += 1;
	
	if(xpos > local_pointeur_ClRegion->box.box_xmax)
	  local_pointeur_ClRegion->box.box_xmax = xpos;
	
	if(ypos > local_pointeur_ClRegion->box.box_ymax)
	  local_pointeur_ClRegion->box.box_ymax = ypos;
	
	*ptcetq = *ptvgetq;
      } /* fin de 2 voisins identiques */
      
      else /* Conflit d'etiquetage */
	/* on affecte au pixel courant l'etiquette minimale des deux */
      {
	
	*ptcetq =
	  ((*ptvgetq) < (*ptvhetq) ?
	   (*ptvgetq) : (*ptvhetq));
	
	/* --- Mise a jour des Classes d'equivalence : 
	   on prend celle d'etiquette la plus faible --- */
	nvetq =
	  ((TabClRegion[*ptvgetq].etiqeq < TabClRegion[*ptvhetq].etiqeq) ?
	   TabClRegion[*ptvgetq].etiqeq : TabClRegion[*ptvhetq].etiqeq);
	
	anetq =
	  ((TabClRegion[*ptvgetq].etiqeq > TabClRegion[*ptvhetq].etiqeq) ?
	   TabClRegion[*ptvgetq].etiqeq : TabClRegion[*ptvhetq].etiqeq);
	
	local_pointeur_ClRegion=&(TabClRegion[*ptcetq]);
	
	/* --- Mise a jour de la structure de donnees --- */
	local_pointeur_ClRegion->som_x += (long)xpos;
	local_pointeur_ClRegion->som_y += (long)ypos;
	local_pointeur_ClRegion->som_lumPl1 += (long)(*ptcsrc1);
	local_pointeur_ClRegion->som_lumPl2 += (long)(*ptcsrc2);
	local_pointeur_ClRegion->som_lumPl3 += (long)(*ptcsrc3);
	local_pointeur_ClRegion->surf += 1;
	
	if(xpos > local_pointeur_ClRegion->box.box_xmax)
	  local_pointeur_ClRegion->box.box_xmax = xpos;
	
	if(ypos > local_pointeur_ClRegion->box.box_ymax)
	  local_pointeur_ClRegion->box.box_ymax = ypos;
	
	/* --- la mise a jour du tableau effectue la fusion des deux
	   classes equivalentes d'etiquettes --- */
	
	MiseAJourTablEquivDF(etq, anetq,nvetq, TabClRegion);
      } /* fin conflit d'etiquettes */
      
      /* --- mise a jour des pointeurs --- */
      ptcsrc1++;
      ptcsrc2++;
      ptcsrc3++;
      ptcetq++;
      ptvgsrc1++;
      ptvgsrc2++;
      ptvgsrc3++;
      ptvhsrc1++;
      ptvhsrc2++;
      ptvhsrc3++;
      ptvgetq++;
      ptvhetq++;

    } /* fin boucle interne sur les colonnes */
    /* --- mise a jour des pointeurs --- */
    ptcsrc1++;
    ptcsrc2++;
    ptcsrc3++;
    ptcetq++;
    ptvgsrc1++;
    ptvgsrc2++;
    ptvgsrc3++;
    ptvhsrc1++;
    ptvhsrc2++;
    ptvhsrc3++;
    ptvgetq++;
    ptvhetq++;
  } /* fin boucle externe sur les lignes */

 
  /* --- Compactage du Tableau des Etiquettes --- */
  *pnetqinit = etq - 1; // etq est l'etiquette de la prochaine Comp Connexe
  nfetq = CompactageDF (etq, TabClRegion) - 1;
  
  *pnbcc = nfetq;
  
  TabClRegion[0].etiqeq = 0;
  
  /* --- Reetiquetage de l'Image des Etiquettes --- */
  // avec le numero des etiquettes compactees
  ptcetq = pdtetq; /* initialisation au premier point */
  
  for (m = 0; m < nlig; m++) /* boucle externe sur les lignes */
    for (n = 0; n < ncol; n++) /* boucle interne sur les colonnes */
    {
      *ptcetq = TabClRegion[*ptcetq].etiqeq;	/* reetiquetage */
      ptcetq++;                    		/* point suivant */
    } /* fin du reetiquetage */


  return(TRUE);
} /* --- fin de l'operateur de Croissance de Regions en Couleur --- */
#endif

/* ------------------------------------------------------------------- */
