﻿// ------------------------------------------------------------------
/**
 * @file 	ClLibDecCompConnexesDF.h
 *
 * @brief 	Prototypes de Fonctions de Decomposition 
 * en Composantes Connexes,
 * selon l'Algorithme de Rosenfield et Pflatz
 * et Adaptation a une Croissance de Regions Simple
 * en imagerie en niveaux de gris et couleur a donnees entrelacees
 * et separees
 * 
 * @author 	Patrick J. Bonnin
 * @email  	bonnin@iutv.univ-paris13.fr
 * @date 2004.05.28 : creation.
 * @date 2004.05.28 : last modification.
 */
// ------------------------------------------------------------------
/* COPYRIGHT (C)	2004, P. Bonnin <bonnin@iutv.univ-paris13.fr>
 *
 * This  library  is  a free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as  published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This  Library  is  distributed in the hope that it will be useful,
 * but  WITHOUT  ANY  WARRANTY;  without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULIAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You  should  have received a copy of the GNU Lesser General Public 
 * License  along  with  this  library;  if  not,  write  to the Free 
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
// ------------------------------------------------------------------
/* Modifications :
 * 2004.05.28 : creation
 */
// ------------------------------------------------------------------

#define NBClREGIONMAX 5000// parametres a moduler en fonction des images

void MiseAJourTablEquivDF(int etq, int anetq,
        int nvetq, PT_ClREGION TabClRegion);

int CompactageDF (int etq, PT_ClREGION TabClRegion);

int DecComposanteConnexeDF (IMAGE *imsrc,
		  IMAGE *imetq, int *pnbcc,
		  int *pnetqinit, int netqmax,
                  PT_ClREGION TabClRegion);

int DecComposanteConnexeSaufZeroDF (IMAGE *imsrc,
		  IMAGE *imetq, int *pnbcc,
		  int *pnetqinit, int netqmax,
                  PT_ClREGION TabClRegion);

void FiltrageTailleCompConnexe(IMAGE *imetq, PT_ClREGION TabClRegion, 
			       int *pnbcc, 
			       long surfmin, int taillemin);


void FusionCompConnexe(IMAGE *imetq, PT_ClREGION TabClRegion, 
		       int *pnbcc, int dmax);


void EnregistreComposantesConnexes(FILE *fp,PT_ClREGION TabClRegion, 
				   int nbccf);

void TraceComposantesConnexes (IMAGE *imcetq, PT_ClREGION TabClRegion, 
			       int nbcc);

int RegionNBFromComposanteConnexeDF (IMAGE *imsrc,
				     IMAGE *imetq, int *pnbcc,
				     int *pnetqinit, 
				     int netqmax,
				     PT_ClREGION TabClRegion,
				     int difmax);

int RegionCoulDEFromComposanteConnexeDF (IMAGE *imsrcCoul,
					 IMAGE *imetq, int *pnbcc,
					 int *pnetqinit, 
					 int netqmax,
					 PT_ClREGION TabClRegion,
					 int difmax1, int difmax2,
					 int difmax3);

int RegionCoulDSFromComposanteConnexeDF (IMAGE *imsrc1,IMAGE *imsrc2, 
					 IMAGE *imsrc3,IMAGE *imetq, 
					 int *pnbcc,
					 int *pnetqinit, 
					 int netqmax,
					 PT_ClREGION TabClRegion,
					 int difmax1, int difmax2, 
					 int difmax3);


