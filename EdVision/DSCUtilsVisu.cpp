﻿// DSCUtilsVisu.c

#include "EdStructures.h"
#include "CompatibiliteStructures.h"

void Gray_To_Color(EdIMAGE *imBW, EdIMAGE *imRGB)
{
  EdPOINT         *point = NULL;
  
  if(crea_POINT(point) == NULL)
  {
    fprintf(stderr,"Memory Allocation Error : Label_Convertion \n");
    exit(0);
  }
  
  for (POINT_Y(point) = 0; POINT_Y(point) < NLIG(imBW); 
       POINT_Y(point)++)
  for (POINT_X(point) = 0; POINT_X(point) < NCOL(imBW); 
       POINT_X(point)++)
  {                                
    PIXEL_R(imRGB, point) = PIXEL(imBW, point);
    PIXEL_V(imRGB, point) = PIXEL(imBW, point);
    PIXEL_B(imRGB, point) = PIXEL(imBW, point);    
  }
  free((void *)point);
}

int LetterAndFigureDetection(EdIMAGE *imEtq, EdIMAGE *imVisu, 
                             int nbcc, PT_ClREGION TabClRegion)
{
   int ncc, ncc1;
   EdPOINT  *point = NULL, *pointv = NULL;
   EdBOX *box;
   long surfBox;
   int  hight, hight1, delta_h;
   int nb, nbn;
   int flag;
   int i, j;
  
   if(crea_POINT(point) == NULL)
   {
     fprintf(stderr,"Memory Allocation Error : Label_Convertion \n");
     exit(0);
   } 
   if(crea_POINT(pointv) == NULL)
   {
     fprintf(stderr,"Memory Allocation Error : Label_Convertion \n");
     exit(0);
   } 
   if(crea_BOX(box) == NULL)
   {
     fprintf(stderr,"Memory Allocation Error : Label_Convertion \n");
     exit(0);
   } 
#if 0
// juste pour Debug
   fprintf(stderr,"Nombre CC : %d\n\n",nbcc);
   for (ncc = 1; ncc <= nbcc; ncc++)
   {
       fprintf(stderr,"cc : %d :: Niveau : %d ::Xmax %d - Xmin %d | Ymax %d - Ymin %d \n",
         ncc, TabClRegion[ncc].niv,
         TabClRegion[ncc].box.box_xmax, TabClRegion[ncc].box.box_xmin,
         TabClRegion[ncc].box.box_ymax, TabClRegion[ncc].box.box_ymin);     
   } 
// fin
#endif 
   /* --- Search of Figure and Letter Height --- */ 
   for (ncc = 1; ncc <= nbcc; ncc++)
   {
       TabClRegion[ncc].fRec = FALSE;
       flag = FALSE;
       if (TabClRegion[ncc].niv == 0)
       {
          surfBox = TabClRegion[ncc].box.box_ymax 
            - TabClRegion[ncc].box.box_ymin + 1;
          surfBox *= (TabClRegion[ncc].box.box_xmax
            - TabClRegion[ncc].box.box_xmin + 1);
            
          if (TabClRegion[ncc].surf <= (2 * surfBox) / 3)
          {
             hight = TabClRegion[ncc].box.box_ymax 
                       - TabClRegion[ncc].box.box_ymin + 1;
                       
             fprintf(stderr,"CC %d : hight = %d\n", ncc, hight);
             
             nb = 0;
             nbn = 0;
             for (ncc1 = 1; ncc1 <= nbcc; ncc1++)
             {
                 if (TabClRegion[ncc1].niv == 0)
                 {
                   nbn++;
                    hight1 = TabClRegion[ncc1].box.box_ymax 
                       - TabClRegion[ncc1].box.box_ymin + 1;
                    delta_h = hight - hight1;
                    delta_h = (delta_h >= 0 ? delta_h : -delta_h);
                    if (delta_h <= 10)
                      nb++;
                 } 
             }
             fprintf(stderr,"CC %d / nbn %d : hight = %d : nb = %d\n", 
                ncc, nbn, hight, nb);
             if (nb >= (nbn / 2))
             {
               flag = TRUE;
               break;      
             }      
          } /* fin si la surface pas trop importante */                   
       } /* fin test couleur noire */
   } /* fin de la boucle primaire */
   fprintf(stderr,"flag : %d : hauteur : %d, nombre : %d\n",flag, hight, nb);
   
   /* --- Bounding Box --- */
   for (ncc = 1; ncc <= nbcc; ncc++)
   {
       if (TabClRegion[ncc].niv == 0)
       {
          hight1 = TabClRegion[ncc].box.box_ymax 
                       - TabClRegion[ncc].box.box_ymin + 1;
          delta_h = hight - hight1;
          delta_h = (delta_h >= 0 ? delta_h : -delta_h);
          
          if (delta_h > 20)
             continue;
             
          /* --- Recognized Letter or Figure --- */
          TabClRegion[ncc].fRec = TRUE;             
          for (POINT_Y(point) = TabClRegion[ncc].box.box_ymin ; 
               POINT_Y(point) <= TabClRegion[ncc].box.box_ymax ;
               POINT_Y(point)++)
          {
	       POINT_X(point) = TabClRegion[ncc].box.box_xmin ;
               PIXEL_R(imVisu, point) = 255;
               PIXEL_V(imVisu, point) = 0;
               PIXEL_B(imVisu, point) = 0;
               
               POINT_X(point) = TabClRegion[ncc].box.box_xmax ; 
               PIXEL_R(imVisu, point) = 255;
               PIXEL_V(imVisu, point) = 0;
	       PIXEL_B(imVisu, point) = 0;
          }
          for (POINT_X(point) = TabClRegion[ncc].box.box_xmin ; 
               POINT_X(point) <= TabClRegion[ncc].box.box_xmax ;
               POINT_X(point)++)
          {
               POINT_Y(point) = TabClRegion[ncc].box.box_ymin ; 
	       PIXEL_R(imVisu, point) = 255;
               PIXEL_V(imVisu, point) = 0;
               PIXEL_B(imVisu, point) = 0;
               
               POINT_Y(point) = TabClRegion[ncc].box.box_ymax ; 
               PIXEL_R(imVisu, point) = 255;
               PIXEL_V(imVisu, point) = 0;
	       PIXEL_B(imVisu, point) = 0;
          }
          /* --- Find Holes in Figures and Letters --- */
          TabClRegion[ncc].nbHole = 0;
          for (ncc1 = 1; ncc1 <= nbcc; ncc1++)
          {
             if (TabClRegion[ncc1].niv != 255)
             continue;
          
             if ((TabClRegion[ncc].box.box_xmin < TabClRegion[ncc1].box.box_xmin)
                && (TabClRegion[ncc].box.box_xmax > TabClRegion[ncc1].box.box_xmax)
                &&(TabClRegion[ncc].box.box_ymin < TabClRegion[ncc1].box.box_ymin)
                && (TabClRegion[ncc].box.box_ymax > TabClRegion[ncc1].box.box_ymax))
             {
               TabClRegion[ncc].nbHole++;
          
               for (POINT_Y(point) = TabClRegion[ncc1].box.box_ymin ; 
                  POINT_Y(point) <= TabClRegion[ncc1].box.box_ymax ;
                  POINT_Y(point)++)
               {
                  POINT_X(point) = TabClRegion[ncc1].box.box_xmin ; 
                  PIXEL_R(imVisu, point) = 0;
                  PIXEL_V(imVisu, point) = 255;
                  PIXEL_B(imVisu, point) = 0;
               
                  POINT_X(point) = TabClRegion[ncc1].box.box_xmax ; 
                  PIXEL_R(imVisu, point) = 0;
                  PIXEL_V(imVisu, point) = 255;
                  PIXEL_B(imVisu, point) = 0;
               }
               for (POINT_X(point) = TabClRegion[ncc1].box.box_xmin ; 
                  POINT_X(point) <= TabClRegion[ncc1].box.box_xmax ;
                  POINT_X(point)++)
               {
                  POINT_Y(point) = TabClRegion[ncc1].box.box_ymin ; 
                  PIXEL_R(imVisu, point) = 0;
                  PIXEL_V(imVisu, point) = 255;
                  PIXEL_B(imVisu, point) = 0;
               
                  POINT_Y(point) = TabClRegion[ncc1].box.box_ymax ; 
                  PIXEL_R(imVisu, point) = 0;
                  PIXEL_V(imVisu, point) = 255;
                  PIXEL_B(imVisu, point) = 0;
               }                         
             }                                    
          } /* end of loop on White Blob */
        
          /* --- Draw Borders and Compute Perimeter --- */
          BOX_XMIN(box) =  TabClRegion[ncc].box.box_xmin - 1;
          if (BOX_XMIN(box) < 0)
             BOX_XMIN(box)= 0;
          
          BOX_YMIN(box) =  TabClRegion[ncc].box.box_ymin - 1;
          if (BOX_YMIN(box) < 0)
             BOX_YMIN(box)= 0;
          
          BOX_XMAX(box) =  TabClRegion[ncc].box.box_xmax + 1;
          if (BOX_XMAX(box) >= NCOL(imEtq))
             BOX_XMAX(box)= NCOL(imEtq) - 1; 
          
          BOX_YMAX(box) =  TabClRegion[ncc].box.box_ymax + 1;
          if (BOX_YMAX(box) >= NLIG(imEtq))
             BOX_YMAX(box)= NLIG(imEtq) - 1;
          
          TabClRegion[ncc].perim = 0;
           
          for (POINT_Y(point) = BOX_YMIN(box) + 1; 
                  POINT_Y(point) < BOX_YMAX(box) ;
                  POINT_Y(point)++)
          for (POINT_X(point) = BOX_XMIN(box) + 1; 
                  POINT_X(point) < BOX_XMAX(box) ;
                  POINT_X(point)++)
          {
             flag = FALSE;
             if(LABEL(imEtq, point) != ncc)
               continue;
             
             for (j = 0; j < 3; j++)
             for (i = 0; i < 3; i++)
             {
                 POINT_X(pointv) = POINT_X(point) + i - 1;
                 POINT_Y(pointv) = POINT_Y(point) + j - 1;
                 if(LABEL(imEtq, pointv) != ncc)
                    flag = TRUE; 
             }
             if (flag) // Border Point
             {
                TabClRegion[ncc].perim ++;
                PIXEL_R(imVisu, point) = 0;
                PIXEL_V(imVisu, point) = 0;
                PIXEL_B(imVisu, point) = 255;
             }
          } /* end of "erosion" procedure */
          
          /* --- Draw Gravity Center --- */
          POINT_X(point) = TabClRegion[ncc].ptg.x;
          POINT_Y(point) = TabClRegion[ncc].ptg.y;
          
          PIXEL_R(imVisu, point) = 255;
          PIXEL_V(imVisu, point) = 0;
          PIXEL_B(imVisu, point) = 0;
          
          /* --- +/- 2 en Y --- */
          POINT_X(pointv) = POINT_X(point);
          POINT_Y(pointv) = POINT_Y(point) + 1;
          PIXEL_R(imVisu, pointv) = 255;
          PIXEL_V(imVisu, pointv) = 0;
          PIXEL_B(imVisu, pointv) = 0;
          
          POINT_Y(pointv)++;
          PIXEL_R(imVisu, pointv) = 255;
          PIXEL_V(imVisu, pointv) = 0;
          PIXEL_B(imVisu, pointv) = 0;
          
          POINT_Y(pointv) = POINT_Y(point) - 1;
          PIXEL_R(imVisu, pointv) = 255;
          PIXEL_V(imVisu, pointv) = 0;
          PIXEL_B(imVisu, pointv) = 0;
          
          POINT_Y(pointv)--;
          PIXEL_R(imVisu, pointv) = 255;
          PIXEL_V(imVisu, pointv) = 0;
          PIXEL_B(imVisu, pointv) = 0;
          
          /* --- +/- 2 en X --- */
          POINT_X(pointv) = POINT_X(point) - 1;
          POINT_Y(pointv) = POINT_Y(point);
          PIXEL_R(imVisu, pointv) = 255;
          PIXEL_V(imVisu, pointv) = 0;
          PIXEL_B(imVisu, pointv) = 0;
          
          POINT_X(pointv)--;         
          PIXEL_R(imVisu, pointv) = 255;
          PIXEL_V(imVisu, pointv) = 0;
          PIXEL_B(imVisu, pointv) = 0;
          
          POINT_X(pointv) = POINT_X(point) + 1;
          PIXEL_R(imVisu, pointv) = 255;
          PIXEL_V(imVisu, pointv) = 0;
          PIXEL_B(imVisu, pointv) = 0;
          
          POINT_X(pointv)++;         
          PIXEL_R(imVisu, pointv) = 255;
          PIXEL_V(imVisu, pointv) = 0;
          PIXEL_B(imVisu, pointv) = 0;
          
          
           
       } /* fin du Test si Ecriture */
   } /* fin du balayage des comp connexes */

   free((void *)point);
   free((void *)pointv);
   free((void *)box);

   return 1;
}
