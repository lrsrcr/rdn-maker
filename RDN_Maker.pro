#-------------------------------------------------
#
# Project created by QtCreator 2018-03-22T13:23:26
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RDN_Maker
TEMPLATE = app

CONFIG += c++14

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += /usr/local/include/opencv
LIBS += -L/usr/local/lib -lopencv_core -lopencv_imgcodecs -lopencv_imgproc -lopencv_highgui

INCLUDEPATH += /home/julien/workspace/NeuroLib/
LIBS += -L"/home/julien/workspace/build-NeuroLib-Desktop_Qt_5_10_1_GCC_64bit-Debug/" -lNeuroLib

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    test.cpp \
    Donnees/charger.cpp \
    Donnees/conversion.cpp \
    Donnees/traitement.cpp \
    EdVision/EdUtilities.cpp \
	Dialogs/visualiserpoidsdialog.cpp \
    EdVision/detectionplaque.cpp \
    EdVision/ClLibDecCompConnexesDF.cpp \
    EdVision/EdLibThreshold.cpp \
    EdVision/DSCNormExtract.cpp \
    EdVision/DSCUtilsVisu.cpp \
    Dialogs/visualiserplaquesdialog.cpp

HEADERS += \
        mainwindow.h \
    Donnees/charger.h \
    Donnees/conversion.h \
    Donnees/traitement.h \
    EdVision/EdStructures.h \
    EdVision/EdUtilities.h \
	Dialogs/visualiserpoidsdialog.h \
    EdVision/detectionplaque.h \
    EdVision/CompatibiliteStructures.h \
    EdVision/ClLibDecCompConnexesDF.h \
    EdVision/EdLibThreshold.h \
    EdVision/DSCNormExtract.h \
    EdVision/DSCUtilsVisu.h \
    Dialogs/visualiserplaquesdialog.h

FORMS += \
        mainwindow.ui \
   Dialogs/visualiserpoidsdialog.ui \
    Dialogs/visualiserplaquesdialog.ui
