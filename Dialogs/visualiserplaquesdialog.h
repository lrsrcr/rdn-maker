﻿#ifndef VISUALISERPLAQUESDIALOG_H
#define VISUALISERPLAQUESDIALOG_H

// Include GUI
#include <QDialog>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QLabel>

// Include STL
#include <vector>

// Include personnels
#include "Donnees/traitement.h"
#include "Donnees/conversion.h"

using namespace std;

namespace Ui {
  class VisualiserPlaquesDialog;
}

class VisualiserPlaquesDialog : public QDialog
{
  Q_OBJECT

public:
  explicit VisualiserPlaquesDialog(QWidget *parent = 0);
  ~VisualiserPlaquesDialog();

  void AfficherPlaque(QString &fichierPlaque, vector<vector<double>> &caracteresPlaques, vector<size_t> &prediction, vector<string> &labels, int row, int col);

private:
  Ui::VisualiserPlaquesDialog *ui;

  QVBoxLayout *vLayout;
  QGridLayout * gLayout;
  QLabel *plaqueLabel;

  size_t nbElement;

  void RemoveAllWidgets();
};

#endif // VISUALISERPLAQUESDIALOG_H
