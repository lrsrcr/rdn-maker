﻿#include "visualiserplaquesdialog.h"
#include "ui_visualiserplaquesdialog.h"

VisualiserPlaquesDialog::VisualiserPlaquesDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::VisualiserPlaquesDialog)
{
  ui->setupUi(this);
  plaqueLabel = new QLabel("Plaque Label");
  vLayout = new QVBoxLayout(this);
  gLayout = new QGridLayout();

  vLayout->addWidget(plaqueLabel);
  vLayout->addLayout(gLayout);

  nbElement=0;
}

VisualiserPlaquesDialog::~VisualiserPlaquesDialog()
{
  delete ui;
}

void VisualiserPlaquesDialog::AfficherPlaque(QString &fichierPlaque, vector<vector<double> > &caracteresPlaques, vector<size_t> &prediction, vector<string> &labels, int row, int col)
{
  RemoveAllWidgets();
  nbElement = 0;
  vLayout->removeItem(gLayout);
  gLayout = new QGridLayout();

  plaqueLabel->setPixmap(QPixmap(fichierPlaque));
  size_t tailleCarre = ceil(sqrt(caracteresPlaques.size()));

  for(size_t h=0, i=0; i<tailleCarre; i++)
  {
    for(size_t j=0; j<tailleCarre && h<caracteresPlaques.size(); j++, h++)
    {
      vector<double> caractereTmp = caracteresPlaques[h];
      NormaliserVecteur(caractereTmp, 255, 0);
      QPixmap pix=ConversionVectorToPixmap(caractereTmp, row, col, 4.0);

      QString strPrediction;
      int indexePrediction = (int) prediction[h];

      if(labels.size()>0)
      {
          strPrediction=QString("Prédiction: %2 (%1)");
          strPrediction = strPrediction.arg(QString::fromStdString(labels[indexePrediction]))
                .arg(indexePrediction);
      }
      else
      {
        strPrediction=QString("Prédiction: %1");
        strPrediction = strPrediction.arg(indexePrediction);
      }

      QVBoxLayout *tmpLayout=new QVBoxLayout();
      QLabel *labelImage = new QLabel(),
	  *labelLegende=new QLabel(strPrediction);
      labelImage->setPixmap(pix);

      tmpLayout->addWidget(labelImage);
      tmpLayout->addWidget(labelLegende);
      this->gLayout->addLayout(tmpLayout, i, j);
      nbElement++;
    }
  }

  vLayout->addLayout(gLayout);

  this->setVisible(true);
}

void VisualiserPlaquesDialog::RemoveAllWidgets()
{
  if (gLayout != NULL)
  {
    QVBoxLayout* item2;
    for(size_t h=0; h<nbElement; h++)
    {
	if((item2 = (QVBoxLayout*) gLayout->itemAt(h))!=NULL)
	{
//	  QVBoxLayout *item2 = (QVBoxLayout*) &item;
	  delete item2->widget();
	  delete item2->widget();

	  delete item2;
	}
    }
  }
}
