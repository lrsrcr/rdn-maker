﻿#include "visualiserpoidsdialog.h"
#include "ui_visualiserpoidsdialog.h"

VisualiserPoidsDialog::VisualiserPoidsDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::VisualiserPoidsDialog)
{
  ui->setupUi(this);
  this->poidsLayout = new QGridLayout(this);
}

VisualiserPoidsDialog::~VisualiserPoidsDialog()
{
  delete ui;
}

void VisualiserPoidsDialog::AfficherPoids(vector<Neurone> &neurones, size_t row, size_t col)
{
  RemoveAllWidgets();

  size_t tailleCarre = ceil(sqrt(neurones.size()));

  for(size_t h=0, i=0; i<tailleCarre; i++)
  {
    for(size_t j=0; j<tailleCarre && h<neurones.size(); j++, h++)
    {
      vector<double> poidsTmp = neurones[h].poids;
      NormaliserVecteur(poidsTmp, 255, 0);
      QPixmap pix=ConversionVectorToPixmap(poidsTmp, row, col, 5.0);

      QLabel *label = new QLabel();
      label->setPixmap(pix);
      this->poidsLayout->addWidget(label, i, j);
    }
  }

  this->setVisible(true);
}

void VisualiserPoidsDialog::on_pushButton_clicked()
{
  this->hide();
}

void VisualiserPoidsDialog::RemoveAllWidgets()
{
  if (poidsLayout != NULL)
  {
      QLayoutItem* item;
      while ((item = poidsLayout->takeAt( 0 ) ) != NULL)
      {
	  delete item->widget();
	  delete item;
      }
  }
}
