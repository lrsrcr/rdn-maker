﻿#ifndef VISUALISERPOIDSDIALOG_H
#define VISUALISERPOIDSDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QGridLayout>

#include "Neurone/neurone.h"
#include "Donnees/conversion.h"
#include "Donnees/traitement.h"

#define LABELS 50

namespace Ui {
  class VisualiserPoidsDialog;
}

class VisualiserPoidsDialog : public QDialog
{
  Q_OBJECT

public:
  explicit VisualiserPoidsDialog(QWidget *parent = 0);
  ~VisualiserPoidsDialog();

  void AfficherPoids(vector<Neurone> &neurones, size_t row, size_t col);

private slots:
  void on_pushButton_clicked();

private:
  void RemoveAllWidgets();

  Ui::VisualiserPoidsDialog *ui;

  QLabel *poids[LABELS];
  QWidget *baseArea;
  QGridLayout *poidsLayout=NULL;
};

#endif // VISUALISERPOIDSDIALOG_H
